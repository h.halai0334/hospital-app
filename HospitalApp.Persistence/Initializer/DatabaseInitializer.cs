using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using CsvHelper;
using HospitalApp.Common.Constants;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Persistence.Initializer
{
    public class DatabaseInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            initializer.SeedEverything(context);
        }

        private void SeedEverything(ApplicationDbContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
            SeedDiseases(context);
        }

        private void SeedDiseases(ApplicationDbContext context)
        {
            if (context.Diseases.Any())
            {
                return;
            }

            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var currentDirectory = Path.GetDirectoryName(assembly.Location);
                using (var reader = new StreamReader(currentDirectory + @"\Data\Diseases.csv"))
                using (var csv = new CsvParser(reader,CultureInfo.InvariantCulture))
                {
                    // var data = reader.ReadToEnd();
                    List<List<string>> rows = new List<List<string>>();
                    while (true)
                    {
                        var line = csv.Read();
                        if (line == null || !line.Any())
                        {
                            break;
                        }
                        rows.Add(line.ToList());
                    }
                    var headers = rows.First();
                    List<SymptomType> listOfSymptomTypes = new List<SymptomType>();
                    List<Symptom> listOfSymptoms = new List<Symptom>();
                    List<DiseaseSymptom> listOfDiseasesSymptoms = new List<DiseaseSymptom>();
                    List<Disease> listOfDiseases = new List<Disease>();
                    for (int i = 0; i < headers.Count - 3; i++)
                    {
                        listOfSymptomTypes.Add(new SymptomType()
                        {
                            Name = headers[i]
                        });
                    }

                    context.SymptomTypes.AddRange(listOfSymptomTypes);
                    context.SaveChanges();
                    for(int i = 1; i < rows.Count;i++)
                    {
                        var symptoms = rows[i];
                        for (int j = 0; j < symptoms.Count - 3; j++)
                        {
                            var symptom = symptoms[j];
                            if (!string.IsNullOrWhiteSpace(symptom))
                            {
                                var type = listOfSymptomTypes[j];
                                if (listOfSymptoms.Any(p => p.Name == symptom))
                                {
                                    continue;
                                }
                                listOfSymptoms.Add(new Symptom()
                                {
                                    Name = symptom,
                                    SymptomTypeId = type.Id
                                });
                            }
                        }
                    }

                    context.AddRange(listOfSymptoms);
                    context.SaveChanges();
                    for(int i = 1; i < rows.Count;i++)
                    {
                        var line = rows[i];
                        var disease = line[^2];
                        if (!string.IsNullOrWhiteSpace(disease))
                        {
                            if (listOfDiseases.Any(p => p.Name == disease))
                            {
                                continue;
                            }
                            listOfDiseases.Add(new Disease()
                            {
                                Name = disease
                            });
                        }
                    }

                    context.AddRange(listOfDiseases);
                    context.SaveChanges();
                    for(int i = 1; i < rows.Count;i++)
                    {
                        List<string> line = rows[i];
                        var symptoms = line.Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                        var disease = symptoms.LastOrDefault();
                        symptoms = symptoms.SkipLast(1).ToList();
                        List<Symptom> symptomList = new List<Symptom>();
                        if (symptoms.Any())
                        {
                            symptomList = listOfSymptoms.Where(p => symptoms.Contains(p.Name)).ToList();
                        }
                        Disease diseaseObj = null;
                        if (!string.IsNullOrWhiteSpace(disease))
                        {
                            diseaseObj = listOfDiseases.FirstOrDefault(p => p.Name == disease);
                            if (diseaseObj == null)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        foreach (var symptom in symptomList)
                        {
                            listOfDiseasesSymptoms.Add(new DiseaseSymptom()
                            {
                                DiseaseId = diseaseObj.Id,
                                SymptomTypeId = symptom.SymptomTypeId,
                                SymptomId = symptom.Id
                            });
                        }
                        
                    }
                    context.AddRange(listOfDiseasesSymptoms);
                    context.SaveChanges();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                context.Diseases.DeleteAll();
                context.SymptomTypes.DeleteAll();
                context.SaveChanges();
            }
        }

        private void SeedStoredProcedures(ApplicationDbContext context)
        {
            var dropGetRecentUsersSql = @"DROP PROCEDURE IF EXISTS GetRecentUsers;";
            var getRecentUsersSql = @"CREATE PROCEDURE GetRecentUsers(
                                    IN userId VARCHAR(255),
		                                IN page INTEGER,
		                                IN skip INTEGER
                                    )
                                    SELECT
		                                u.Id,
		                                u.FullName,
		                                u.Image,
		                                u.PhoneNumber 
	                                FROM
		                                AspNetUsers AS u 
	                                WHERE
	                                Id IN (SELECT ReceiverUserId FROM Transactions WHERE SenderUserId = userId AND ReceiverUserId IS NOT NULL GROUP BY ReceiverUserId ORDER BY MAX(CreatedDate) DESC) 
                                    LIMIT page OFFSET skip";

            var dropUpdateWalletTrigger = "DROP TRIGGER If EXISTS updateBalance;";
            var createUpdateWalletTrigger = @"CREATE TRIGGER updateBalance AFTER INSERT ON Wallets FOR EACH ROW
                                            INSERT INTO UserBalances ( UserId, TotalBalance )
	                                            VALUES( New.UserId, New.Amount ) 
		                                    ON DUPLICATE KEY UPDATE TotalBalance = TotalBalance + Values(TotalBalance)";
            context.Database.ExecuteSqlRaw(dropGetRecentUsersSql);
            context.Database.ExecuteSqlRaw(dropUpdateWalletTrigger);
            context.Database.ExecuteSqlRaw(getRecentUsersSql);
            context.Database.ExecuteSqlRaw(createUpdateWalletTrigger);
        }

        private void SeedRoles(ApplicationDbContext context)
        {
            if (context.Roles.Any())
            {
                return;
            }

            var roles = new[] {RoleNames.Admin, RoleNames.Doctor, RoleNames.CareProvider, RoleNames.DiagnosticProvider};
            foreach (var role in roles)
            {
                context.Roles.Add(new Role()
                {
                    Name = role,
                    NormalizedName = role.ToUpper()
                });
            }

            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            if (context.Users.Any())
            {
                return;
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            string adminRoleId = context.Roles.First(p => p.Name == RoleNames.Admin).Id;
            var admin = new User()
            {
                PhoneNumber = Constant.AdminPhone,
                Email = Constant.AdminEmail,
                IsEnabled = true,
                NormalizedEmail = Constant.AdminEmail.ToUpper(),
                Image = Constant.DefaultImageUrl,
                FullName = "Administrator",
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };
            admin.UserRoles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = adminRoleId,
                    UserId = admin.Id
                }
            };
            admin.PasswordHash = passwordHasher.HashPassword(admin, Constant.AdminPassword);
            context.Users.Add(admin);
            context.SaveChanges();
        }
    }
}