﻿using HospitalApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalApp.Persistence.Configurations
{

    public class DiseaseSymptomTypeConfiguration : IEntityTypeConfiguration<DiseaseSymptom>
    {
        public void Configure(EntityTypeBuilder<DiseaseSymptom> builder)
        {
            builder.HasKey(p => new {p.DiseaseId, p.SymptomId, p.SymptomTypeId});
            builder.HasOne(p => p.Disease)
                .WithMany(p => p.DiseaseSymptomTypes)
                .HasForeignKey(p => p.DiseaseId);
            builder.HasOne(p => p.SymptomType)
                .WithMany(p => p.DiseaseSymptomTypes)
                .HasForeignKey(p => p.SymptomTypeId);
            builder.HasOne(p => p.Symptom)
                .WithMany(p => p.DiseaseSymptomTypes)
                .HasForeignKey(p => p.SymptomId);


        }
    }

}