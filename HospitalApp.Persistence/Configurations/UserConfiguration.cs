using HospitalApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalApp.Persistence.Configurations
{

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(p => p.PhoneNumber).IsRequired();
            builder.Property(p => p.PasswordHash).IsRequired();
        }
    }

}