using HospitalApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalApp.Persistence.Configurations
{

    public class AppSettingConfiguration : IEntityTypeConfiguration<AppSetting>
    {
        public void Configure(EntityTypeBuilder<AppSetting> builder)
        {
            builder.HasKey(p => p.Key);
            builder.Property(p => p.Key).IsRequired();
            builder.Property(p => p.Value).IsRequired();
        }
    }

}