using HospitalApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalApp.Persistence.Configurations
{

    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {

        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.Property(p => p.Message).IsRequired();
            builder.Property(p => p.UserId).IsRequired();
            builder.HasOne(p => p.User)
                .WithMany(b => b.Notifications)
                .HasForeignKey(p => p.UserId);
        }
    }

}