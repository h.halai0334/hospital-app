﻿using HospitalApp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HospitalApp.Persistence.Configurations
{

    public class SymptomTypeConfiguration : IEntityTypeConfiguration<SymptomType>
    {
        public void Configure(EntityTypeBuilder<SymptomType> builder)
        {
            builder.Property(p => p.Name).IsRequired();
        }
    }

}