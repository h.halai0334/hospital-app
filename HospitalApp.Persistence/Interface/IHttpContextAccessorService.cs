namespace HospitalApp.Persistence.Interface
{
    public interface IHttpContextAccessorService
    {
        string GetUserId();
    }
}