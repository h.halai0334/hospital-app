namespace HospitalApp.Persistence.Option
{
    public class MongoDbOption
    {
        public string DefaultDatabase { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}