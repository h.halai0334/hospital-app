using System.Linq;
using FluentValidation.Results;
using HospitalApp.Application.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HospitalApp.Web.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid == false)
            {
                var errors = context.ModelState.Where(p => p.Value.ValidationState == ModelValidationState.Invalid).Select(p => p.Key).ToList();
                if (errors.Count == 1 && errors.Any( p => p.Contains("Id")))
                {
                    return;
                }

                throw new ValidationException(context.ModelState
                    .Where(p => p.Value.ValidationState == ModelValidationState.Invalid)
                    .Select(p => new ValidationFailure(p.Key, p.Value.Errors.First().ErrorMessage)).ToList());
            }
        }
    }
}