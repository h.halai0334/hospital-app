namespace HospitalApp.Web.Options
{
    public class JwtAppOption
    {
        public string JwtSecret { get; set; }
        public string JwtIssuer { get; set; }
        public string JwtAudience { get; set; }
        public string JwtAuthority { get; set; }
        public int JwtExpiry { get; set; }
    }
}