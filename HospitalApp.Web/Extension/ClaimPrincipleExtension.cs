using System.Security.Claims;
using HospitalApp.Common.Constants;

namespace HospitalApp.Web.Extension
{
    public static class ClaimPrincipleExtension
    {
        public static string GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(CustomClaimTypes.UserId);
        }
        
    }
}