using System.Security.Claims;
using System.Threading.Tasks;
using HospitalApp.Application.Users.Commands.ChangePassword;
using HospitalApp.Application.Users.Commands.ForgetPassword;
using HospitalApp.Application.Users.Commands.ResetPassword;
using HospitalApp.Application.Users.Commands.UpdateFcmId;
using HospitalApp.Application.Users.Commands.UpdateImage;
using HospitalApp.Application.Users.Commands.UpdateProfile;
using HospitalApp.Application.Users.Commands.UpdateProfileAddress;
using HospitalApp.Application.Users.Queries.GetProfileAddress;
using HospitalApp.Application.Users.Queries.GetUserByPhoneAndRole;
using HospitalApp.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{
    public class UserController : BaseController
    {
        [HttpPost("forgetPassword")]
        public async Task<IActionResult> ForgetPassword(ForgetPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost("verifyOtpPin")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        
        [Authorize]
        [HttpPost("fcm")]
        public async Task<IActionResult> UpdateFcm(UpdateFcmIdRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [Authorize]
        [HttpPost("profile")]
        public async Task<IActionResult> UpdateProfile(UpdateProfileRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("profile/address")]
        public async Task<IActionResult> UpdateProfileAddress(UpdateProfileAddressRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpGet("profile/address")]
        public async Task<IActionResult> GrtProfileAddress()
        {
            GetProfileAddressRequestModel model = new GetProfileAddressRequestModel
            {
                UserId = User.FindFirstValue(CustomClaimTypes.UserId)
            };
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [Authorize]
        [HttpPost("image")]
        public async Task<IActionResult> UpdateImage(UpdateImageRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("getByRoleAndPhone")]
        public async Task<IActionResult> GetByRoleAndPhone(GetUserByPhoneAndRoleRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}