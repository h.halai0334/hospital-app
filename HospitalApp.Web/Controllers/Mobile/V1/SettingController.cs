using System.Threading.Tasks;
using HospitalApp.Application.AppSettings.Commands.SaveSetting;
using HospitalApp.Application.AppSettings.Queries.GetSettings;
using HospitalApp.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{
    public class SettingController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetSettings([FromQuery] GetSettingsRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> SaveSettings(SaveSettingRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}