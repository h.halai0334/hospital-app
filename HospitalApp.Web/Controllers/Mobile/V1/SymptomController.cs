﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HospitalApp.Application.Symptoms.Commands.CreateSymptom;
using HospitalApp.Application.Symptoms.Commands.DeleteSymptom;
using HospitalApp.Application.Symptoms.Commands.UpdateSymptom;
using HospitalApp.Application.Symptoms.Queries.GetSymptoms;
using HospitalApp.Application.Symptoms.Queries.GetSymptomsDropDown;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{

    public class SymptomController : BaseController
    {

        [HttpGet("dropDown")]
        [Authorize]
        public async Task<List<GetSymptomsDropDownResponseModel>> GetSymptom([FromQuery] GetSymptomsDropDownRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }
        [HttpGet]
        [Authorize]
        public async Task<GetSymptomsResponseModel> GetSymptom([FromQuery] GetSymptomsRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPost]
        [Authorize]
        public async Task<CreateSymptomResponseModel> CreateSymptom(CreateSymptomRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPut("{Id}")]
        [Authorize]
        public async Task<UpdateSymptomResponseModel> UpdateSymptom(int id, UpdateSymptomRequestModel model)
        {
            model.Id = id;

            var response = await Mediator.Send(model);
            return response;
        }


        [HttpDelete("{Id}/symptomType/{symptomTypeId}")]
        [Authorize]
        public async Task<DeleteSymptomResponseModel> DeleteSymptom(int id, int symptomTypeId)
        {
            DeleteSymptomRequestModel model = new DeleteSymptomRequestModel();
            model.SymptomTypeId = symptomTypeId;
            model.Id = id;

            var response = await Mediator.Send(model);
            return response;
        }
    }

}