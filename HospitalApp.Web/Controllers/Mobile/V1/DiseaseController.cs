﻿using System.Threading.Tasks;
using HospitalApp.Application.Diseases.Commands.CreateDisease;
using HospitalApp.Application.Diseases.Commands.DeleteDisease;
using HospitalApp.Application.Diseases.Commands.UpdateDisease;
using HospitalApp.Application.Diseases.Queries.GetDiseases;
using HospitalApp.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{

    public class DiseaseController : BaseController
    {

        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<GetDiseasesResponseModel> GetDisease([FromQuery] GetDiseasesRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPost]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<CreateDiseaseResponseModel> CreateDisease(CreateDiseaseRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPut("{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<UpdateDiseaseResponseModel> UpdateDisease(int id, UpdateDiseaseRequestModel model)
        {
            model.Id = id;

            var response = await Mediator.Send(model);
            return response;
        }


        [HttpDelete("{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<DeleteDiseaseResponseModel> DeleteDisease(int id)
        {
            DeleteDiseaseRequestModel model = new DeleteDiseaseRequestModel();
            model.Id = id;
            var response = await Mediator.Send(model);
            return response;
        }
    }

}