﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HospitalApp.Application.Symptoms.Queries.GetSymptomsDropDown;
using HospitalApp.Application.SymptomTypes.Commands.CreateSymptomType;
using HospitalApp.Application.SymptomTypes.Commands.DeleteSymptomType;
using HospitalApp.Application.SymptomTypes.Commands.UpdateSymptomType;
using HospitalApp.Application.SymptomTypes.Queries.GetSymptomTypes;
using HospitalApp.Application.SymptomTypes.Queries.GetSymptomTypesDropDown;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{

    public class SymptomTypeController : BaseController
    {

        [HttpGet("dropDown")]
        [Authorize]
        public async Task<List<GetSymptomTypesDropDownResponseModel>> GetSymptom([FromQuery] GetSymptomTypesDropDownRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }
        [HttpGet]
        [Authorize]
        public async Task<GetSymptomTypesResponseModel> GetSymptomType([FromQuery] GetSymptomTypesRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPost]
        [Authorize]
        public async Task<CreateSymptomTypeResponseModel> CreateSymptomType(CreateSymptomTypeRequestModel model)
        {

            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPut("{Id}")]
        [Authorize]
        public async Task<UpdateSymptomTypeResponseModel> UpdateSymptomType(int id, UpdateSymptomTypeRequestModel model)
        {
            model.Id = id;

            var response = await Mediator.Send(model);
            return response;
        }


        [HttpDelete("{Id}")]
        [Authorize]
        public async Task<DeleteSymptomTypeResponseModel> DeleteSymptomType(int id)
        {
            DeleteSymptomTypeRequestModel model = new DeleteSymptomTypeRequestModel {Id = id};

            var response = await Mediator.Send(model);
            return response;
        }
    }

}