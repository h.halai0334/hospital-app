using System.Security.Claims;
using System.Threading.Tasks;
using HospitalApp.Application.Dashboards.Queries.GetAdminDashboard;
using HospitalApp.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{
    
    public class DashboardController : BaseController
    {
        [HttpGet("admin-dashboard")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetAdminDashboard()
        {
            var model = new GetAdminDashboardRequestModel {UserId = User.FindFirstValue(CustomClaimTypes.UserId)};
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}