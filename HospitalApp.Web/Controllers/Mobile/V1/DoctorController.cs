﻿using System.Threading.Tasks;
using HospitalApp.Application.Doctors.Commands.BlockDoctor;
using HospitalApp.Application.Doctors.Commands.CreateDoctor;
using HospitalApp.Application.Doctors.Commands.DeleteDoctor;
using HospitalApp.Application.Doctors.Commands.UnBlockDoctor;
using HospitalApp.Application.Doctors.Commands.UpdateDoctor;
using HospitalApp.Application.Doctors.Queries.GetDoctors;
using HospitalApp.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{

    public class DoctorController : BaseController
    {

        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<GetDoctorsResponseModel> GetDoctor([FromQuery] GetDoctorsRequestModel model)
        {
            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPost]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<CreateDoctorResponseModel> CreateDoctor(CreateDoctorRequestModel model)
        {
            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPut("{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<UpdateDoctorResponseModel> UpdateDoctor([FromRoute]string id, UpdateDoctorRequestModel model)
        {
            model.Id = id;
            var response = await Mediator.Send(model);
            return response;
        }


        [HttpDelete("{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<DeleteDoctorResponseModel> DeleteDoctor([FromRoute]string id)
        {
            DeleteDoctorRequestModel model = new DeleteDoctorRequestModel {Id = id};
            var response = await Mediator.Send(model);
            return response;
        }
        
        [HttpPut("block/{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<BlockDoctorResponseModel> BlockDoctor([FromRoute]string id)
        {
            var model = new BlockDoctorRequestModel() {Id = id};
            var response = await Mediator.Send(model);
            return response;
        }
        
        [HttpPut("unblock/{Id}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<UnBlockDoctorResponseModel> UnBlock([FromRoute]string id)
        {
            var model = new UnBlockDoctorRequestModel() {Id = id};
            var response = await Mediator.Send(model);
            return response;
        }
    }

}