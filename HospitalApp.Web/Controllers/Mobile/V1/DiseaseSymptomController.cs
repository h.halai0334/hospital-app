﻿using System.Threading.Tasks;
using HospitalApp.Application.DiseaseSymptoms.Commands.CreateDiseaseSymptom;
using HospitalApp.Application.DiseaseSymptoms.Commands.DeleteDiseaseSymptom;
using HospitalApp.Application.DiseaseSymptoms.Queries.GetDiseaseSymptomByDiseaseId;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HospitalApp.Web.Controllers.Mobile.V1
{
    public class DiseaseSymptomController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<GetDiseaseSymptomByDiseaseIdResponseModel> GetDiseaseSymptom(
            [FromQuery] GetDiseaseSymptomByDiseaseIdRequestModel model)
        {
            var response = await Mediator.Send(model);
            return response;
        }

        [HttpPost]
        [Authorize]
        public async Task<CreateDiseaseSymptomResponseModel> CreateDiseaseSymptom(
            CreateDiseaseSymptomRequestModel model)
        {
            var response = await Mediator.Send(model);
            return response;
        }


        [HttpDelete("{diseaseId}/symptom/{symptomId}/symptomType/{symptomTypeId}")]
        [Authorize]
        public async Task<DeleteDiseaseSymptomResponseModel> DeleteDiseaseSymptom(int diseaseId, int symptomId,
            int symptomTypeId)
        {
            DeleteDiseaseSymptomRequestModel model = new DeleteDiseaseSymptomRequestModel();
            model.DiseaseId = diseaseId;
            model.SymptomTypeId = symptomTypeId;
            model.SymptomId = symptomId;
            var response = await Mediator.Send(model);
            return response;
        }
    }
}