import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private authService: AuthService,
              private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const url: string = state.url;
    return this.checkLogin(url, route);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }


  checkLogin(url: string, route: ActivatedRouteSnapshot): boolean {
    try {
      if (this.authService.isLoggedIn) {
        if (route.data.role == null) {
          return true;
        }
        const authRole = route.data.role as string[];
        return this.authService.isRole(authRole);
      }
    } catch (e) {
      console.log(e);
    }
    this.router.navigate(['/login']);
    return false;
  }
}
