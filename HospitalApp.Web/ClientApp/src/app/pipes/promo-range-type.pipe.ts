import { Pipe, PipeTransform } from '@angular/core';
import {PromoRangeType} from '../enums/promo-range-type.enum';

@Pipe({
  name: 'promoRangeType'
})
export class PromoRangeTypePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case PromoRangeType.DateRange:
        return 'Date';
      case PromoRangeType.PriceRange:
        return 'Price';
      case PromoRangeType.DayOfWeek:
        return 'Week';
      case PromoRangeType.TimeRange:
        return 'Time';
      case PromoRangeType.NoRange:
        return 'N/A';
    }
  }

}
