import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'boolToVerificationLabel'
})
export class BoolToVerificationLabelPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value === true) {
      return '<span class="badge badge-success">Verified</span>';
    }
    return '<span class="badge badge-danger">Not Verified</span>';
  }

}
