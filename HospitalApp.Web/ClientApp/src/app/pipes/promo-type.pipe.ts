import {Pipe, PipeTransform} from '@angular/core';
import {PromoType} from '../enums/promo-type.enum';

@Pipe({
  name: 'promoType'
})
export class PromoTypePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case PromoType.Percent:
        return 'Percent';
      case PromoType.Cash:
        return 'Cash';
    }
  }

}
