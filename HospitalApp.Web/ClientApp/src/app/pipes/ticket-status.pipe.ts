import { Pipe, PipeTransform } from '@angular/core';
import {PromoRangeType} from '../enums/promo-range-type.enum';
import {TicketStatus} from '../enums/ticket-status.enum';

@Pipe({
  name: 'ticketStatus'
})
export class TicketStatusPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case TicketStatus.Pending:
        return '<span class="badge badge-danger">Pending</span>';
      case TicketStatus.Processing:
        return '<span class="badge badge-warning">Processing</span>';
      case TicketStatus.Resolved:
        return '<span class="badge badge-success">Resolved</span>';
    }
  }

}
