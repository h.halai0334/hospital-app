import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoolToLabelPipe } from './bool-to-label.pipe';
import { BoolToVerificationLabelPipe } from './bool-to-verification-label.pipe';
import { PromoTypePipe } from './promo-type.pipe';
import { PromoRangeTypePipe } from './promo-range-type.pipe';
import { TicketStatusPipe } from './ticket-status.pipe';

@NgModule({
  declarations: [BoolToLabelPipe, BoolToVerificationLabelPipe, PromoTypePipe, PromoRangeTypePipe, TicketStatusPipe],
  exports: [BoolToLabelPipe, BoolToVerificationLabelPipe, PromoTypePipe, PromoRangeTypePipe, TicketStatusPipe],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
