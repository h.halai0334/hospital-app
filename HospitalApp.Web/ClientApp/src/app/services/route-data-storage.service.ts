import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RouteDataStorageService {
  public data: any;
}
