export class Message {
  id: string;
  text: string;
  createdDate: string;
  senderId: string;
  senderName: string;
  receiverId: string;
  receiverName: string;

  sampleData(): Message {
    this.senderId = 'ABC';
    this.senderName = 'Company Name';
    this.receiverId = 'ABC';
    this.receiverName = 'Admin';
    this.text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n';
    this.createdDate = '10/10/10 6:45 PM';
    return this;
  }
}
