
export class UserDetail {
  name: string;
  email: string;
  phone: string;
  wallet: string;
  cnic: string;
  image: string;
  date: string;
  totalSent: number;
  totalReceived: number;
  isEnabled: boolean;
  isVerified: boolean;
}

