export class Card {
  id: number;
  customer: string;
  name: string;
  cardType: number;
  createdDate: string;
  expiryDate: string;
  lastDigits: string;
}
