import {VehicleRange} from './vehicle-range';
import {VehicleExtraService} from './vehicle-extra-service';

export class Vehicle {
  id: number;
  name: string;
  image: string;
  isActive: boolean;
  parcelLimit: number;
  weightInKgLimit: number;
  dimensionInCmLimit: number;
  durationInMinLimit: number;
  topUpCharges: number;
  topUpDurationInMin: number;
  deliveryType: number;
  createdDate: string;
  ranges: VehicleRange[];
  bookingCount: number;
  driverCount: number;
  extraServices: VehicleExtraService[];
  extraServiceCount: number;
}
