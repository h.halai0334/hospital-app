export class SymptomType {
  id: number;
  name: string;
  createdDate: string;
  symptomCount: number;
}
