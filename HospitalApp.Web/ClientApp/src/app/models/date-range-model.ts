export class DateRangeModel {
  startDate: string;
  endDate: string;
}
