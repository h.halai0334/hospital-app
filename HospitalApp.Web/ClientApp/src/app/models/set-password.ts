export class SetPassword {
  password: string;
  confirmPassword: string;
  userId: string;
  token: string;
}
