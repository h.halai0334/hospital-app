export class VehicleExtraService {
  id: number;
  extraServiceId: number;
  vehicleTypeId: number;
  vehicle: string;
  extraService: string;
  createdDate: string;
  isActive: boolean;
  isImmediate: boolean;
  isSchedule: boolean;
  isOther: boolean;
  charge: number;
}
