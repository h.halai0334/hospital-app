export class Area {
  id: number;
  name: string;
  zipCodes: string[];
  createdDate: string;
}
