export class Doctor {
  id: string;
  name: string;
  createdDate: string;
  email: string;
  image: string;
  isEnabled: boolean;
  password: string;
}
