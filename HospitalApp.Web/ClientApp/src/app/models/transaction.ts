export class Transaction {
  sender: string;
  receiver: string;
  createdDate: string;
  transactionCode: string;
  loadType: number;
  amount: number;
  tax: number;
}
