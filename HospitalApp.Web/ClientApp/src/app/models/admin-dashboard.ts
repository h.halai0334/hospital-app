import {Company} from './company';

export class AdminDashboard {
  public totalCompanies: number;
  public totalFreeAccounts: number;
  public totalJobs: number;
  public totalEmployees: number;
  public companies: Company[];
  public totalManagers: number;
  public totalStaffs: number;
  public totalWorkers: number;
}
