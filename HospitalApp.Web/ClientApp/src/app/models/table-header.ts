export class TableHeader {
  title: string;
  sort: string;
  sortable = true;
  toolTip: string;

  constructor(title: string, sort: string, sortable: boolean, tooltip: string = '') {
    this.title = title;
    this.sort = sort;
    this.sortable = sortable;
    this.toolTip = tooltip;
  }

  sortIcon = 'fas fa fa-sort';
  revertSorting() {
    this.sortIcon = 'fas fa fa-sort';
    console.log(this.sortIcon);
  }
  set sortingDirection(value: boolean) {
    if (value) {
      this.sortIcon = 'fas fa fa-sort-up';
    } else {
      this.sortIcon = 'fas fa fa-sort-down';
    }
  }
}
