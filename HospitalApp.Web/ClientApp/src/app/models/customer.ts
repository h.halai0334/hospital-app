export class Customer {
  id: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  isEnabled: boolean;
  isVerified: boolean;
}
