import {Company} from './company';
import {Message} from './message';

export class Ticket {
  id: number;
  ticketNo: string;
  subject: string;
  status: number;
  detail: string;
  companyId: string;
  companyName: string;
  companyEmail: string;
  companyImage: string;
  companyPhoneNumber: string;
  createdDate: string;
  workDate: string;
  resolvedDate: string;
  messages: Message[];

  sampleData() {
    this.id = 4;
    this.ticketNo = '123AVD';
    this.subject = 'Help Us!!';
    this.detail = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n';
    this.companyEmail = 'test@gmail.com';
    this.companyId = 'test@gmail.com';
    this.companyPhoneNumber = '+12345301531';
    this.companyName = 'Super Nova';
    this.status = 0;
    this.createdDate = '10/10/10 1:45 PM';
    this.workDate = '11/10/10 1:45 PM';
    this.resolvedDate = '12/10/10 1:45 PM';
    this.companyImage = '/assets/img/default-user.png';
    let m = new Message();
    m = m.sampleData();
    this.messages = [m, m, m, m];
    return this;
  }
}
