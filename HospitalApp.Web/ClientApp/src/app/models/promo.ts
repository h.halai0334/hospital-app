import {TimeRangeModel} from './time-range-model';
import {DateRangeModel} from './date-range-model';
import {PriceRangeModel} from './price-range-model';
import {DayOfWeekModel} from './day-of-week-model';

export class Promo {
  id: number;
  createdDate: string;
  type: number;
  rangeType: number;
  json: string;
  title: string;
  code: string;
  description: string;
  discount: number;
  isActive: boolean;
  jsonModel: TimeRangeModel | DateRangeModel | PriceRangeModel | DayOfWeekModel;
}
