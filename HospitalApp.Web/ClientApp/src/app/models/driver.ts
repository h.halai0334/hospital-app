export class Driver {
  id: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  isEnabled: boolean;
  isVerified: boolean;
  isLocked: boolean;
  licenseImage: boolean;
  identityCardImage: string;
  vehicleRegistrationImage: string;
  vehiclePlateNo: string;
}
