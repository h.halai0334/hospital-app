export class Company {
  constructor() {
    this.deviceImeiNos = ['123456789012345'];
  }

  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  image: string;
  address: string;
  createdDate: string;
  serviceType: string;
  noOfLicenses: number;
  floor: string;
  unit: string;
  flat: string;
  remarks: string;
  country: string;
  startDate: string;
  endDate: string;
  usedLicenses: number;
  deviceImeiNos: string[];
  isEnabled: boolean;
  jobsCompleted: number;

  sampleData(): Company {
    this.id = 'abcd';
    this.deviceImeiNos = ['123456789012345'];
    this.startDate = '10/10/2000';
    this.endDate = '10/10/2004';
    this.email = 'test@demo.com';
    this.isEnabled = true;
    this.phoneNumber = '+923336041031';
    this.name = 'Test Company';
    this.remarks = 'No Remarks';
    this.unit = '7';
    this.floor = '8';
    this.flat = '9';
    this.country = 'Pakistan';
    this.address = 'Sample Address';
    this.usedLicenses = 3;
    this.noOfLicenses = 10;
    this.jobsCompleted = 10;
    return this;
  }
}
