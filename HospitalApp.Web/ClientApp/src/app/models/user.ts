export class User {
  roles: any;
  email: string;
  fullName: string;
  image: string;
  id: string;
  isEnabled: boolean;
  phoneNumber: string;
  address: string;

  sampleData(): User {
    this.image = '/assets/img/default-user.png';
    this.fullName = 'Kelvin Lek';
    this.email = 'admin@tam.com';
    this.phoneNumber = '+951234567890';
    this.address = 'Some Unknown Street';
    return this;
  }
}

