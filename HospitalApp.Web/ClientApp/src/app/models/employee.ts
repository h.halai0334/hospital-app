export class Employee {
  id: string;
  fullName: string;
  totalTransactions: number;
  points: number;
  totalSpent: number;
  createdDate: string;
}
