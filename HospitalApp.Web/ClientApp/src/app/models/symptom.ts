export class Symptom {
  id: number;
  name: string;
  createdDate: string;
  symptomTypeId: number;
}
