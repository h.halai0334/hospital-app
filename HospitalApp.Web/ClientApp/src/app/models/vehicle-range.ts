export class VehicleRange {
  id: number;
  fromKm: number;
  toKm: string;
  vehicleTypeId: number;
  vehicleType: string;
  baseCharge: number;
  createdDate: string;
  hasNoLimit: boolean;
}
