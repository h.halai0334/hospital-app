export class Restaurant {
  id: number;
  image: string;
  restaurantName: string;
  address: string;
  fullName: string;
  email: string;
  password: string;
  confirmPassword: string;
  userId: string;
  createdDate: string;
}
