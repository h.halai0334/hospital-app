export class Language {
  code: string;
  name: string;
  createdDate: string;
}
