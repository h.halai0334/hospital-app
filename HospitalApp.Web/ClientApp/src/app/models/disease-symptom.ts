export class DiseaseSymptom {
  diseaseId: number;
  disease: string;
  symptomTypeId: number;
  symptomType: string;
  symptom: string;
  symptomId: number;
  createdDate: string;
}
