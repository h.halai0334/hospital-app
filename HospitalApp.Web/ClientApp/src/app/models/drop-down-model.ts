export class DropDownModel {
  value: number | string;
  text: string;
}
