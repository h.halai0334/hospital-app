export class Disease {
  id: number;
  name: string;
  symptomCount: number;
  createdDate: string;
}
