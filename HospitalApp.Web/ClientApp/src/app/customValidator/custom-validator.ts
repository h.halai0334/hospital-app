import {AbstractControl, ValidatorFn, Validators} from '@angular/forms';
import {AppRegex} from '../constants/app-regex';
import {DataType} from '../enums/data-type.enum';

export class CustomValidator {
  private static regex = new RegExp(AppRegex.PasswordRegex);

  static emailValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return {'email': true};
      }
      return null;
    };
  }

  static swiftCodeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return {'swiftCode': true};
      }
      if ((control.value.length != min && control.value.length != max)) {
        return {'swiftCode': true};
      }
      return null;
    };
  }

  static PasswordMatch(passwordControl: AbstractControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return {'password': true};
      }
      if ((control.value !== passwordControl.value)) {
        return {'password': true};
      }
      return null;
    };
  }

  static Password(): ValidatorFn {
    // return (control: AbstractControl): { [key: string]: any } => {
    //   if (!control.value) {
    //     // if control is empty return no error
    //     return null;
    //   }
    //   // test the value of the control against the regexp supplied
    //   const valid = this.regex.test(control.value);
    //   // if true, return no error (no error), else return error passed in the second parameter
    //   return !valid ? null : { 'invalid': true };
    // };
    return Validators.pattern(AppRegex.PasswordRegex);
  }

  static GreaterThan(minControl: AbstractControl, type: DataType = DataType.Number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (type === DataType.Number) {
        if ((+control.value <= +minControl.value)) {
          return {'greater': true, 'value': control.value};
        }
      }
      if (type === DataType.Date) {
        if ((control.value <= minControl.value)) {
          return {'greater': true, 'value': control.value};
        }
      }

      return null;
    };
  }

  static NoUnderScore(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return {'underscore': true};
      }
      if ((control.value.toString().includes('_'))) {
        return {'underscore': true};
      }
      return null;
    };
  }

  static Phone(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return {'phone': true};
      }
      const value = control.value.toString();
      if (!(value.startsWith('+') && value.length >= 12 && value.length <= 13)) {
        return {'phone': true};
      }
      return null;
    };
  }
}
