import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SetPasswordFormComponent} from './components/set-password-form/set-password-form.component';

const routes: Routes = [
  {
    path: '',
    component: SetPasswordFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetPasswordRoutingModule { }
