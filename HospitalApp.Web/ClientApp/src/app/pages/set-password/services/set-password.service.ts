import {Injectable} from '@angular/core';
import {SetPassword} from '../../../models/set-password';
import {ResponseModel} from '../../../models/response.model';
import {SetPasswordEndpointService} from './set-password-endpoint.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SetPasswordService {
  constructor(private passwordService: SetPasswordEndpointService) {

  }

  setPassword(model: SetPassword) {
    return this.passwordService.setPasswordEndpoint(model)
      .pipe(map((response) => <ResponseModel>response));
  }


}
