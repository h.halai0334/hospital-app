import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../services/endpoint-factory.service';
import {SetPassword} from '../../../models/set-password';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SetPasswordEndpointService extends EndpointFactoryService {

  private readonly _createPasswordUrl: string = '/api/v1/registration/setPasswordByToken';

  get PasswordsUrl() { return this.configurations.baseUrl + this._createPasswordUrl; }
  setPasswordEndpoint(model: SetPassword): Observable<any> {
    return this.http.put(this.PasswordsUrl, model, { headers: this.getRequestHeaders().headers });
  }
}
