import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetPasswordFormComponent } from './components/set-password-form/set-password-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SetPasswordRoutingModule} from './set-password-routing.module';

@NgModule({
  declarations: [SetPasswordFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SetPasswordRoutingModule,
  ]
})
export class SetPasswordModule { }
