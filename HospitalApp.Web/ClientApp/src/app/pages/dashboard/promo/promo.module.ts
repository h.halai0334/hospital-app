import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoManagementComponent } from './components/promo-management/promo-management.component';
import { PromoInfoDialogComponent } from './components/promo-info-dialog/promo-info-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PromoRoutingModule} from './promo-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
  declarations: [PromoManagementComponent, PromoInfoDialogComponent],
  entryComponents: [PromoInfoDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    PromoRoutingModule,
    SharedModule,
    PipesModule,
  ]
})
export class PromoModule { }
