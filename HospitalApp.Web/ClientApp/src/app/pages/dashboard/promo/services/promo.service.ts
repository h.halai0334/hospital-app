import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {Promo} from '../../../../models/promo';

@Injectable({
  providedIn: 'root'
})
export class PromoService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/promo';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }



  public cacheRows: Promo[] = [];
  cachePage: number;
  cacheTotalSize: number;


  getPromos(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }


  updatePromo(promo: Promo): Observable<ResponseModel> {
    return this.http.put(this.baseUrl + '/' + promo.id, promo, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  createPromo(promo: Promo): Observable<ResponseModel> {
    return this.http.post(this.baseUrl, promo, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  deletePromo(id: number): Observable<ResponseModel> {
    return this.http.delete(this.baseUrl + '/' + id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));

  }

}
