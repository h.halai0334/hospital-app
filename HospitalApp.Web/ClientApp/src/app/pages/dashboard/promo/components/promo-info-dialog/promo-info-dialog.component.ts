import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Promo} from '../../../../../models/promo';
import {AlertService} from '../../../../../services/alert.service';
import {PromoService} from '../../services/promo.service';
import {DropDownModel} from '../../../../../models/drop-down-model';
import {PromoType} from '../../../../../enums/promo-type.enum';
import {PromoRangeType} from '../../../../../enums/promo-range-type.enum';
import {DayOfWeek} from '../../../../../enums/day-of-week.enum';
import {CustomValidator} from '../../../../../customValidator/custom-validator';
import {TimeRangeModel} from '../../../../../models/time-range-model';
import {DayOfWeekModel} from '../../../../../models/day-of-week-model';
import {PriceRangeModel} from '../../../../../models/price-range-model';
import {DateRangeModel} from '../../../../../models/date-range-model';
import {DateUtil} from '../../../../../util/date-util';

@Component({
  selector: 'app-promo-info-dialog',
  templateUrl: './promo-info-dialog.component.html',
  styleUrls: ['./promo-info-dialog.component.scss']
})
export class PromoInfoDialogComponent implements OnInit {


  cancelEvent: () => void;
  saveEvent: (promo: Promo) => void;

  isEdit = true;
  promo: Promo = new Promo();
  promoGroup: FormGroup;
  typeControl: FormControl;
  rangeControl: FormControl;
  titleControl: FormControl;
  codeControl: FormControl;
  descriptionControl: FormControl;
  discountControl: FormControl;
  isActiveControl: FormControl;

  dateRangeGroup: FormGroup;
  fromDateControl: FormControl;
  toDateControl: FormControl;
  daysControl: FormControl;
  minPriceControl: FormControl;
  maxPriceControl: FormControl;
  priceRangeGroup: FormGroup;

  timeRangeGroup: FormGroup;
  toTimeControl: FormControl;
  fromTimeControl: FormControl;

  typeDropDown: DropDownModel[] = [
    {text: PromoType[PromoType.Cash], value: +PromoType.Cash},
    {text: PromoType[PromoType.Percent], value: +PromoType.Percent},
  ];

  daysDropDown: DropDownModel[] = [
    {text: DayOfWeek[DayOfWeek.Sunday], value: +DayOfWeek.Sunday},
    {text: DayOfWeek[DayOfWeek.Monday], value: +DayOfWeek.Monday},
    {text: DayOfWeek[DayOfWeek.Tuesday], value: +DayOfWeek.Tuesday},
    {text: DayOfWeek[DayOfWeek.Wednesday], value: +DayOfWeek.Wednesday},
    {text: DayOfWeek[DayOfWeek.Thursday], value: +DayOfWeek.Thursday},
    {text: DayOfWeek[DayOfWeek.Friday], value: +DayOfWeek.Friday},
    {text: DayOfWeek[DayOfWeek.Saturday], value: +DayOfWeek.Saturday},
  ];
  rangeDropDown: DropDownModel[] = [
    {text: 'None', value: +PromoRangeType.NoRange},
    {text: 'Date Range', value: +PromoRangeType.DateRange},
    {text: 'Day Of Weeks', value: +PromoRangeType.DayOfWeek},
    {text: 'Price Range', value: +PromoRangeType.PriceRange},
    // {text: 'Time Range', value: +PromoRangeType.TimeRange},
  ];

  constructor(private  alertService: AlertService, private promoService: PromoService, private cd: ChangeDetectorRef) {
  }


  isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  promoRange(value: number): PromoRangeType {
    return value as PromoRangeType;
  }

  initForm() {
    this.typeControl = new FormControl('0', [Validators.required]);
    this.codeControl = new FormControl('', [Validators.required]);
    this.rangeControl = new FormControl('0', [Validators.required]);
    this.discountControl = new FormControl(0, [Validators.required]);
    this.titleControl = new FormControl('', [Validators.required]);
    this.descriptionControl = new FormControl('', [Validators.required]);
    this.isActiveControl = new FormControl(true, [Validators.required]);
    this.promoGroup = new FormGroup({
      type: this.typeControl,
      range: this.rangeControl,
      discount: this.discountControl,
      title: this.titleControl,
      description: this.descriptionControl,
      isActive: this.isActiveControl
    });
    this.fromDateControl = new FormControl(new Date(), [Validators.required]);
    this.toDateControl = new FormControl(new Date(), [Validators.required]);
    this.dateRangeGroup = new FormGroup({
      from: this.fromDateControl,
      to: this.toDateControl,
    });
    this.daysControl = new FormControl('', [Validators.required]);
    this.minPriceControl = new FormControl(1, [Validators.required]);
    this.maxPriceControl = new FormControl(1, [Validators.required, CustomValidator.GreaterThan(this.minPriceControl)]);
    this.priceRangeGroup = new FormGroup({
      min: this.minPriceControl,
      max: this.maxPriceControl,
    });

    this.fromTimeControl = new FormControl(new Date(), [Validators.required]);
    this.toTimeControl = new FormControl(new Date(), [Validators.required]);
    this.timeRangeGroup = new FormGroup({
      from: this.fromTimeControl,
      to: this.toTimeControl,
    });
  }

  populateForm() {
    this.typeControl.setValue(this.promo.type);
    this.titleControl.setValue(this.promo.title);
    this.discountControl.setValue(this.promo.discount);
    this.isActiveControl.setValue(this.promo.isActive);
    this.descriptionControl.setValue(this.promo.description);
    this.rangeControl.setValue(this.promo.rangeType);
    this.codeControl.setValue(this.promo.code);
    switch (this.promo.rangeType) {
      case PromoRangeType.TimeRange:
        break;
      case PromoRangeType.DateRange:
        this.promo.jsonModel = JSON.parse(this.promo.json) as DateRangeModel;
        const fromDate = (new Date(this.promo.jsonModel.startDate));
        const toDate = new Date(this.promo.jsonModel.endDate);
        this.fromDateControl.setValue({
          year: fromDate.getFullYear(),
          month: fromDate.getMonth(),
          day: fromDate.getDate()
        });
        this.toDateControl.setValue({
          year: toDate.getFullYear(),
          month: toDate.getMonth(),
          day: toDate.getDate()
        });
        break;
    }
  }

  ngOnInit() {
  }

  setPromo(promo: Promo) {
    this.initForm();
    if (promo == null) {
      this.isEdit = false;
      promo = new Promo();
      promo.type = 0;
      promo.rangeType = 0;
      promo.isActive = true;
    }
    Object.assign(this.promo, promo);
    this.populateForm();
  }

  validateAndPopulate(): boolean {
    if (this.promoGroup.valid) {
      let isValid = false;
      this.promo.rangeType = +this.rangeControl.value;
      this.promo.description = this.descriptionControl.value;
      this.promo.title = this.titleControl.value;
      this.promo.type = +this.typeControl.value;
      this.promo.discount = this.discountControl.value;
      this.promo.isActive = this.isActiveControl.value;
      this.promo.code = this.codeControl.value;
      switch (this.promoRange(+this.rangeControl.value)) {
        case PromoRangeType.NoRange:
          isValid = true;
          this.promo.jsonModel = new DayOfWeekModel();
          break;
        case PromoRangeType.TimeRange:
          isValid = this.timeRangeGroup.valid;
          const timeRangeModel = new TimeRangeModel();
          timeRangeModel.endTime = this.toTimeControl.value;
          timeRangeModel.startTime = this.fromTimeControl.value;
          this.promo.jsonModel = timeRangeModel;
          break;
        case PromoRangeType.DayOfWeek:
          isValid = this.daysControl.valid;
          const dayOfWeekModel = new DayOfWeekModel();
          dayOfWeekModel.days = this.daysControl.value;
          this.promo.jsonModel = dayOfWeekModel;
          break;
        case PromoRangeType.PriceRange:
          isValid = this.priceRangeGroup.valid;
          const priceRangeModel = new PriceRangeModel();
          priceRangeModel.max = +this.maxPriceControl.value;
          priceRangeModel.min = +this.minPriceControl.value;
          this.promo.jsonModel = priceRangeModel;
          break;
        case PromoRangeType.DateRange:
          isValid = this.dateRangeGroup.valid;
          const dateRangeModel = new DateRangeModel();
          const endDate = new Date(+this.toDateControl.value.year, this.toDateControl.value.month - 1, +this.toDateControl.value.day);
          const startDate = new Date(+this.fromDateControl.value.year, this.fromDateControl.value.month - 1, +this.fromDateControl.value.day);
          dateRangeModel.startDate = DateUtil.formatDate(startDate);
          dateRangeModel.endDate = DateUtil.formatDate(endDate);
          this.promo.jsonModel = dateRangeModel;
          break;
      }
      if (isValid) {
        this.promo.json = JSON.stringify(this.promo.jsonModel);
        return true;
      }
    }
    this.alertService.showErrorMessage('Please fill all the required values');
    return false;
  }

  save() {
    if (this.validateAndPopulate()) {
      if (this.isEdit) {
        this.update();
        return;
      }
      this.alertService.startLoadingMessage('Saving...');
      this.promoService.createPromo(this.promo).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showSuccessMessage('Promo Created');
        if (this.saveEvent) {
          this.saveEvent(result.data as Promo);
        }
      });
    }
  }

  update() {
    this.alertService.startLoadingMessage('Updating...');
    this.promoService.updatePromo(this.promo).subscribe(result => {
      this.alertService.stopLoadingMessage();
      this.alertService.showSuccessMessage('Promo Updated');
      if (this.saveEvent) {
        this.saveEvent(result.data as Promo);
      }
    });
  }

  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

  dayChanged(value: string) {
    let days: string[] = [];
    if (this.daysControl.value) {
      days = this.daysControl.value;
    }
    if (days.some(p => p === value)) {
      const index = days.indexOf(value, 0);
      if (index > -1) {
        days.splice(index, 1);
      }
    } else {
      days.push(value);
    }
    this.daysControl.setValue(days);
  }
}
