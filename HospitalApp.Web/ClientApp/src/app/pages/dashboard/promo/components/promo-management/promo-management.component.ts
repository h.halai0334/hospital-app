import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PromoService} from '../../services/promo.service';
import {Promo} from '../../../../../models/promo';
import {PromoInfoDialogComponent} from '../promo-info-dialog/promo-info-dialog.component';
import {PromoType} from '../../../../../enums/promo-type.enum';

@Component({
  selector: 'app-promo-management',
  templateUrl: './promo-management.component.html',
  styleUrls: ['./promo-management.component.scss']
})
export class PromoManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Code', 'code', true),
      new TableHeader('Title', 'title', true),
      new TableHeader('Type', 'type', true),
      new TableHeader('Range', 'rangeType', true),
      new TableHeader('Discount', 'discount', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private promoService: PromoService, private modalService: NgbModal) {

  }

  get rows(): Promo[] {
    return this.promoService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }


  cashType(): number {
    return +PromoType.Cash;
  }
  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.promoService.cachePage;
      this.totalPage = this.promoService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.promoService.getPromos(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.promoService.cacheRows = result.data as Promo[];
      this.promoService.cachePage = this.page;
      this.totalPage = result.count;
      this.promoService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editPromo(row: Promo) {
    const modal = this.modalService.open(PromoInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as PromoInfoDialogComponent;
    instance.setPromo(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (promo: Promo) => {
      modal.close();
      Object.assign(row, promo);
    };
  }


  deletePromo(row: Promo) {
    this.alertService.showDialog('Do you want to delete ' + row.title + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.promoService.deletePromo(row.id).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(PromoInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as PromoInfoDialogComponent;
    instance.setPromo(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (promo: Promo) => {
      modal.close();
      this.getData();
    };
  }

}
