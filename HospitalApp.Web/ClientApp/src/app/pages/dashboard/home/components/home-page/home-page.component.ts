import {Component, OnInit} from '@angular/core';
import {AdminDashboard} from '../../../../../models/admin-dashboard';
import {Company} from '../../../../../models/company';
import {RoleNames} from '../../../../../constants/role-names';
import {TableHeader} from '../../../../../models/table-header';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  adminDashboard: AdminDashboard;
  data: any[] = [];
  barData: any[] = [];

  headers: TableHeader[] =
    [
      new TableHeader('Name', 'name', false),
      new TableHeader('Email', 'email', false),
      new TableHeader('Phone Number', 'phoneNumber', false),
      new TableHeader('Jobs Completed', 'jobs', false),
    ];
  constructor() {
  }

  ngOnInit() {
    this.addSampleData();
  }

  addSampleData() {
    this.adminDashboard = new AdminDashboard();
    this.adminDashboard.totalCompanies = 10;
    this.adminDashboard.totalEmployees = 15;
    this.adminDashboard.totalStaffs = 16;
    this.adminDashboard.totalManagers = 5;
    this.adminDashboard.totalFreeAccounts = 20;
    this.adminDashboard.totalJobs = 352;
    this.adminDashboard.totalWorkers = 20;
    const company = new Company();
    const c = company.sampleData();
    this.adminDashboard.companies = [c, c, c, c, c, c, c, c, c, c];
    this.drawPieChart();
    this.drawBarChart();
  }

  getRandomInt(max): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  drawBarChart() {
    this.barData.push({name: 'Sun', value: this.getRandomInt(50)});
    this.barData.push({name: 'Mon', value: this.getRandomInt(50)});
    this.barData.push({name: 'Tue', value: this.getRandomInt(50)});
    this.barData.push({name: 'Wed', value: this.getRandomInt(50)});
    this.barData.push({name: 'Thurs', value: this.getRandomInt(50)});
    this.barData.push({name: 'Fri', value: this.getRandomInt(50)});
    this.barData.push({name: 'Sat', value: this.getRandomInt(50)});
  }
  drawPieChart() {
    this.data.push({name: RoleNames.Manager, value: this.adminDashboard.totalManagers});
    this.data.push({name: RoleNames.Staff, value: this.adminDashboard.totalStaffs});
    this.data.push({name: RoleNames.Worker, value: this.adminDashboard.totalWorkers});
    this.data.push({name: 'Remaining', value: this.adminDashboard.totalFreeAccounts});
  }
}
