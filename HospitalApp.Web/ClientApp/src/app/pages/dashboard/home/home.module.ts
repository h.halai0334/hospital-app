import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './components/home-page/home-page.component';
import {HomeRoutingModule} from './home-routing.module';
import {BarChartModule, PieChartModule} from '@swimlane/ngx-charts';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    PieChartModule,
    BarChartModule,
    NgbTooltipModule,
  ]
})
export class HomeModule { }
