import {Injectable} from '@angular/core';
import {Doctor} from '../../../../models/doctor';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorPageService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/doctor';
  private readonly _blockUrl: string = this._baseUrl + '/block';
  private readonly _unblockUrl: string = this._baseUrl + '/unblock';
  cachePage: number;
  cacheTotalSize: number;

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get blockUrl() {
    return this.configurations.baseUrl + this._blockUrl;
  }

  get unBlockUrl() {
    return this.configurations.baseUrl + this._unblockUrl;
  }


  public cacheRows: Doctor[] = [];

  getDoctors(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  createDoctor(doctor: Doctor) {
    return this.http.post(this.baseUrl, doctor, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Doctor>response));
  }

  updateDoctor(doctor: Doctor) {
    return this.http.put(this.baseUrl + '/' + doctor.id, doctor, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Doctor>response));
  }

  deleteDoctor(doctor: Doctor) {
    return this.http.delete(this.baseUrl + '/' + doctor.id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Doctor>response));
  }


  blockDoctor(doctor: Doctor) {
    return this.http.put(this.blockUrl + '/' + doctor.id, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Doctor>response));

  }

  unBlockDoctor(doctor: Doctor) {
    return this.http.put(this.unBlockUrl + '/' + doctor.id, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Doctor>response));

  }
}
