import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorPageComponent } from './components/doctor-page/doctor-page.component';
import { DoctorInfoDialogComponent } from './components/doctor-info-dialog/doctor-info-dialog.component';
import {DoctorPageRoutingModule} from './doctor-page-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';



@NgModule({
  declarations: [DoctorPageComponent, DoctorInfoDialogComponent],
    imports: [
        CommonModule,
        DoctorPageRoutingModule,
        NgbModule,
        ReactiveFormsModule,
        SharedModule,
        PipesModule
    ]
})
export class DoctorPageModule { }
