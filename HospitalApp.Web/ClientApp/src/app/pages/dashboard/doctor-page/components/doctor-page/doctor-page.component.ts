import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DoctorPageService} from '../../services/doctor-page.service';
import {Doctor} from '../../../../../models/doctor';
import {DoctorInfoDialogComponent} from '../doctor-info-dialog/doctor-info-dialog.component';
import {ViewImageDialogComponent} from '../../../../../shared/view-image-dialog/view-image-dialog.component';

@Component({
  selector: 'app-doctor-page',
  templateUrl: './doctor-page.component.html',
  styleUrls: ['./doctor-page.component.scss']
})
export class DoctorPageComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Image', 'fullName', false),
      new TableHeader('Name', 'fullName', true),
      new TableHeader('Email', 'email', true),
      new TableHeader('Status', 'isEnabled', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private doctorService: DoctorPageService, private modalService: NgbModal) {

  }

  get rows(): Doctor[] {
    return this.doctorService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.doctorService.cachePage;
      this.totalPage = this.doctorService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.doctorService.getDoctors(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.doctorService.cacheRows = result.data as Doctor[];
      this.doctorService.cachePage = this.page;
      this.totalPage = result.count;
      this.doctorService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editDoctor(row: Doctor) {
    const modal = this.modalService.open(DoctorInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as DoctorInfoDialogComponent;
    instance.setDoctor(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (doctor: Doctor) => {
      modal.close();
      Object.assign(row, doctor);
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteDoctor(row: Doctor) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.doctorService.deleteDoctor(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  blockDoctor(row: Doctor) {
    this.alertService.showDialog('Do you want to block ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.doctorService.blockDoctor(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was blocked', MessageSeverity.success);
        row.isEnabled = false;
      });
    });
  }

  unblockDoctor(row: Doctor) {
    this.alertService.showDialog('Do you want to unblock ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('UnBlocking...');
      this.alertService.closeDialog();
      this.doctorService.unBlockDoctor(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The user was unblocked', MessageSeverity.success);
        row.isEnabled = true;
      });
    });
  }

  create() {
    const modal = this.modalService.open(DoctorInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as DoctorInfoDialogComponent;
    instance.setDoctor(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (doctor: Doctor) => {
      modal.close();
      this.alertService.showMessage('Success', doctor.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

  viewImage(row: Doctor) {
    const modal = this.modalService.open(ViewImageDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as ViewImageDialogComponent;
    instance.image = row.image;
  }
}
