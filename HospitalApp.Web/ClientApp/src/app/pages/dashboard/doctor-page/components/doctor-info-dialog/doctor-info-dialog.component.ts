import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Doctor} from '../../../../../models/doctor';
import {DoctorPageService} from '../../services/doctor-page.service';
import {CustomValidator} from '../../../../../customValidator/custom-validator';
import {ImageFile} from '../../../../../constants/image-file';

@Component({
  selector: 'app-doctor-info-dialog',
  templateUrl: './doctor-info-dialog.component.html',
  styleUrls: ['./doctor-info-dialog.component.scss']
})
export class DoctorInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (doctor: Doctor) => void;
  isEdit = false;

  doctor: Doctor = new Doctor();
  nameControl: FormControl;
  imageControl: FormControl;
  emailControl: FormControl;
  passwordControl: FormControl;
  doctorGroup: FormGroup;

  constructor(private doctorService: DoctorPageService,
              private cd: ChangeDetectorRef,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.doctor.name);
    this.emailControl.setValue(this.doctor.email);
    this.imageControl.setValue(this.doctor.image);
  }

  initializeForm() {
    this.imageControl = new FormControl(ImageFile.DefaultUserImage, [Validators.required]);
    this.nameControl = new FormControl('', [Validators.required]);
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);
    this.passwordControl = new FormControl('', [Validators.required, CustomValidator.Password()]);
    this.doctorGroup = new FormGroup({
      image: this.imageControl,
      name: this.nameControl,
      email: this.emailControl,
      password: this.passwordControl,
    });
  }

  changeImage() {
    document.getElementById('fileToUpload2').click();
  }

  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.doctor.image = e.currentTarget.result;
        this.imageControl.setValue(this.doctor.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  setDoctor(row: Doctor = null) {
    this.initializeForm();
    if (row == null) {
      row = new Doctor();
    } else {
      this.isEdit = true;
      this.emailControl.disable();
      this.doctorGroup.removeControl('password');
    }
    Object.assign(this.doctor, row);
    this.populateForm();
  }

  update() {
    if (this.doctorGroup.valid) {
      this.alertService.startLoadingMessage('Updating...');
      this.doctorService.updateDoctor(this.doctor).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result as Doctor;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.doctorGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    this.doctor.name = this.nameControl.value;
    this.doctor.email = this.emailControl.value;
    this.doctor.password = this.passwordControl.value;
    if (this.isEdit) {
      this.update();
      return;
    }
    this.alertService.startLoadingMessage('Saving...');
    this.doctorService.createDoctor(this.doctor).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result as Doctor;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
