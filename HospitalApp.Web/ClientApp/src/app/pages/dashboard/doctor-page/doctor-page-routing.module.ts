import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DoctorPageComponent} from './components/doctor-page/doctor-page.component';



const routes: Routes = [
  {path: '', component: DoctorPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorPageRoutingModule { }
