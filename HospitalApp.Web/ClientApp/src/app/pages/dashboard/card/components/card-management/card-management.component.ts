import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {CardService} from '../../services/card.service';
import {Card} from '../../../../../models/card';

@Component({
  selector: 'app-card-management',
  templateUrl: './card-management.component.html',
  styleUrls: ['./card-management.component.scss']
})
export class CardManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Customer', 'fullName', true),
      new TableHeader('Name', 'name', true),
      new TableHeader('Card Type', 'cardType', true),
      new TableHeader('Expiry Date', 'expiryDate', true),
      new TableHeader('Last Digits', 'lastDigits', true),
      new TableHeader('Created Date', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private cardService: CardService) {

  }

  get rows(): Card[] {
    return this.cardService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.cardService.cachePage;
      this.totalPage = this.cardService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.cardService.getCards(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.cardService.cacheRows = result.data as Card[];
      this.cardService.cachePage = this.page;
      this.totalPage = result.count;
      this.cardService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }


  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }


}
