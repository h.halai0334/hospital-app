import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {Card} from '../../../../models/card';

@Injectable({
  providedIn: 'root'
})
export class CardService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/userCard';
  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }
  public cacheRows: Card[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getCards(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

}
