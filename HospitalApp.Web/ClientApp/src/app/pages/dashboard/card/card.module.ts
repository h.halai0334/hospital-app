import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardManagementComponent } from './components/card-management/card-management.component';
import {CardRoutingModule} from './card-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CardManagementComponent],
  imports: [
    CommonModule,
    SharedModule,
    PipesModule,
    NgbModule,
    CardRoutingModule,
  ]
})
export class CardModule { }
