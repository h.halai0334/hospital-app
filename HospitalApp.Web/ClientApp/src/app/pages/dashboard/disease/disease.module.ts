import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiseaseManagementComponent } from './components/disease-management/disease-management.component';
import { DiseaseInfoDialogComponent } from './components/disease-info-dialog/disease-info-dialog.component';
import {DiseaseRoutingModule} from './disease-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [DiseaseManagementComponent, DiseaseInfoDialogComponent],
  imports: [
    CommonModule,
    DiseaseRoutingModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class DiseaseModule { }
