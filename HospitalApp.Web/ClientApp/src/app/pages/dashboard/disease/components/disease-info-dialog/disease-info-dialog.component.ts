import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {DiseaseService} from '../../services/disease.service';
import {Disease} from '../../../../../models/disease';

@Component({
  selector: 'app-disease-info-dialog',
  templateUrl: './disease-info-dialog.component.html',
  styleUrls: ['./disease-info-dialog.component.scss']
})
export class DiseaseInfoDialogComponent implements OnInit {


  cancelEvent: () => void;
  saveEvent: (disease: Disease) => void;
  isEdit = false;

  disease: Disease = new Disease();
  nameControl: FormControl;
  diseaseGroup: FormGroup;

  constructor(private diseaseService: DiseaseService,
              private cd: ChangeDetectorRef,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.disease.name);
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.required]);
    this.diseaseGroup = new FormGroup({
      name: this.nameControl,
    });
  }

  setDisease(row: Disease = null) {
    this.initializeForm();
    if (row == null) {
      row = new Disease();
    } else {
      this.isEdit = true;
      this.diseaseGroup.removeControl('password');
    }
    Object.assign(this.disease, row);
    this.populateForm();
  }

  update() {
    if (this.diseaseGroup.valid) {
      this.alertService.startLoadingMessage('Updating...');
      this.diseaseService.updateDisease(this.disease).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result as Disease;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.diseaseGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    this.disease.name = this.nameControl.value;
    if (this.isEdit) {
      this.update();
      return;
    }
    this.alertService.startLoadingMessage('Saving...');
    this.diseaseService.createDisease(this.disease).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result as Disease;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
