import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DiseaseService} from '../../services/disease.service';
import {Disease} from '../../../../../models/disease';
import {DiseaseInfoDialogComponent} from '../disease-info-dialog/disease-info-dialog.component';

@Component({
  selector: 'app-disease-management',
  templateUrl: './disease-management.component.html',
  styleUrls: ['./disease-management.component.scss']
})
export class DiseaseManagementComponent implements OnInit {


  headers: TableHeader[] =
    [
      new TableHeader('Name', 'fullName', true),
      new TableHeader('Symptoms', 'fullName', false),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService,
              private diseaseService: DiseaseService,
              private router: Router,
              private modalService: NgbModal) {

  }

  get rows(): Disease[] {
    return this.diseaseService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.diseaseService.cachePage;
      this.totalPage = this.diseaseService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.diseaseService.getDiseases(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.diseaseService.cacheRows = result.data as Disease[];
      this.diseaseService.cachePage = this.page;
      this.totalPage = result.count;
      this.diseaseService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editDisease(row: Disease) {
    const modal = this.modalService.open(DiseaseInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as DiseaseInfoDialogComponent;
    instance.setDisease(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (disease: Disease) => {
      modal.close();
      row.name = disease.name;
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteDisease(row: Disease) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.diseaseService.deleteDisease(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(DiseaseInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as DiseaseInfoDialogComponent;
    instance.setDisease(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (disease: Disease) => {
      modal.close();
      this.alertService.showMessage('Success', disease.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

  viewDisease(row: Disease) {
    this.router.navigateByUrl('/dashboard/disease-symptoms/' + row.id);
  }
}
