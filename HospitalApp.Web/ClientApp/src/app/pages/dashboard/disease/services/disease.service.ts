import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {Disease} from '../../../../models/disease';

@Injectable({
  providedIn: 'root'
})
export class DiseaseService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/disease';
  cachePage: number;
  cacheTotalSize: number;

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  public cacheRows: Disease[] = [];

  getDiseases(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  createDisease(disease: Disease) {
    return this.http.post(this.baseUrl, disease, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Disease>response));
  }

  updateDisease(disease: Disease) {
    return this.http.put(this.baseUrl + '/' + disease.id, disease, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Disease>response));
  }

  deleteDisease(disease: Disease) {
    return this.http.delete(this.baseUrl + '/' + disease.id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Disease>response));
  }

}
