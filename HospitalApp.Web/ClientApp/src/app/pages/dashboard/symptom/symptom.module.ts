import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SymptomManagementComponent } from './components/symptom-management/symptom-management.component';
import { SymptomInfoDialogComponent } from './components/symptom-info-dialog/symptom-info-dialog.component';
import {SymptomRoutingModule} from './symptom-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [SymptomManagementComponent, SymptomInfoDialogComponent],
  imports: [
    CommonModule,
    SymptomRoutingModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class SymptomModule { }
