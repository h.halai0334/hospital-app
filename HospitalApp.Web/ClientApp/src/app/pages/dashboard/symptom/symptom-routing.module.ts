import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SymptomManagementComponent} from './components/symptom-management/symptom-management.component';



const routes: Routes = [
  {path: ':id', component: SymptomManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SymptomRoutingModule { }
