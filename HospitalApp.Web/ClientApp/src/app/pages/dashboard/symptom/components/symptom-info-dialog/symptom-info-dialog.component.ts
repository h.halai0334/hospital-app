import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Symptom} from '../../../../../models/symptom';
import {SymptomService} from '../../services/symptom.service';

@Component({
  selector: 'app-symptom-info-dialog',
  templateUrl: './symptom-info-dialog.component.html',
  styleUrls: ['./symptom-info-dialog.component.scss']
})
export class SymptomInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (symptom: Symptom) => void;
  isEdit = false;

  symptom: Symptom = new Symptom();
  nameControl: FormControl;
  symptomGroup: FormGroup;

  constructor(private symptomService: SymptomService,
              private cd: ChangeDetectorRef,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.symptom.name);
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.required]);
    this.symptomGroup = new FormGroup({
      name: this.nameControl,
    });
  }

  setSymptom(symptomTypeId: number, row: Symptom = null) {
    this.initializeForm();
    if (row == null) {
      row = new Symptom();
      row.symptomTypeId = symptomTypeId;
    } else {
      this.isEdit = true;
      this.symptomGroup.removeControl('password');
    }
    Object.assign(this.symptom, row);
    this.populateForm();
  }

  update() {
    if (this.symptomGroup.valid) {
      this.alertService.startLoadingMessage('Updating...');
      this.symptomService.updateSymptom(this.symptom).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result as Symptom;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.symptomGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    this.symptom.name = this.nameControl.value;
    if (this.isEdit) {
      this.update();
      return;
    }
    this.alertService.startLoadingMessage('Saving...');
    this.symptomService.createSymptom(this.symptom).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result as Symptom;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
