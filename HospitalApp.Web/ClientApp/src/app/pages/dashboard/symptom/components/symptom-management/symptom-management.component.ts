import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SymptomService} from '../../services/symptom.service';
import {Symptom} from '../../../../../models/symptom';
import {SymptomInfoDialogComponent} from '../symptom-info-dialog/symptom-info-dialog.component';
import {SymptomType} from '../../../../../models/symptom-type';

@Component({
  selector: 'app-symptom-management',
  templateUrl: './symptom-management.component.html',
  styleUrls: ['./symptom-management.component.scss']
})
export class SymptomManagementComponent implements OnInit {

  symptomTypeId = 0;
  symptomType: SymptomType = new SymptomType();
  headers: TableHeader[] =
    [
      new TableHeader('Name', 'fullName', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService,
              private symptomService: SymptomService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal) {

  }

  get rows(): Symptom[] {
    return this.symptomService.cacheRows;
  }


  ngOnInit() {
    this.activatedRoute.params.subscribe(result => {
      this.symptomTypeId = +result['id'];
      this.initialize();
      this.initializeDataTable();
    });
  }

  initialize() {
    this.getData(true);
  }

  getData(isInit = false) {
    this.alertService.startLoadingMessage();
    this.symptomService.getSymptoms(this.symptomTypeId, isInit, this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      if (isInit) {
        this.symptomType = result.parent as SymptomType;
      }
      this.symptomService.cacheRows = result.data as Symptom[];
      this.symptomService.cachePage = this.page;
      this.totalPage = result.count;
      this.symptomService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editSymptom(row: Symptom) {
    const modal = this.modalService.open(SymptomInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as SymptomInfoDialogComponent;
    instance.setSymptom(this.symptomTypeId, row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (symptom: Symptom) => {
      modal.close();
      Object.assign(row, symptom);
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteSymptom(row: Symptom) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.symptomService.deleteSymptom(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(SymptomInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as SymptomInfoDialogComponent;
    instance.setSymptom(this.symptomTypeId, null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (symptom: Symptom) => {
      modal.close();
      this.alertService.showMessage('Success', symptom.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

  goBack() {
    this.router.navigateByUrl('/dashboard/symptomTypes');
  }
}
