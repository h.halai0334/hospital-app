import {Injectable} from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {Symptom} from '../../../../models/symptom';
import {HttpParams} from '@angular/common/http';
import {SymptomType} from '../../../../models/symptom-type';

@Injectable({
  providedIn: 'root'
})
export class SymptomService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/symptom';
  private readonly _dropDownUrl: string = this._baseUrl + '/dropDown';
  cachePage: number;
  cacheTotalSize: number;

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get dropDownUrl() {
    return this.configurations.baseUrl + this._dropDownUrl;
  }
  public cacheRows: Symptom[] = [];

  getSymptoms(symptomTypeId: number, isInit: boolean, page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    const params = new HttpParams().set('isGetParent', isInit + '').set('symptomTypeId', symptomTypeId + '').set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false')
      .set('orderBy', orderBy + '');
    return this.http.get(this.baseUrl, {
      headers: this.getRequestHeaders().headers,
      params: params
    }).pipe(map((response) => <any>response));

  }

  createSymptom(symptom: Symptom) {
    return this.http.post(this.baseUrl, symptom, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Symptom>response));
  }

  updateSymptom(symptom: Symptom) {
    return this.http.put(this.baseUrl + '/' + symptom.id, symptom, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Symptom>response));
  }

  deleteSymptom(symptom: Symptom) {
    return this.http.delete(this.baseUrl + '/' + symptom.id + '/symptomType/' + symptom.symptomTypeId, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <Symptom>response));
  }

  getSymptomsDropDown(symptomTypeId: number) {
    const params = new HttpParams().set('symptomTypeId', symptomTypeId + '');
    return this.http.get(this.dropDownUrl, {
      headers: this.getRequestHeaders().headers,
      params: params
    }).pipe(map((response) => <Symptom[]>response));

  }
}
