import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingManagementComponent } from './components/setting-management/setting-management.component';
import {SettingRoutingModule} from './setting-routing.module';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [SettingManagementComponent],
  imports: [
    CommonModule,
    SettingRoutingModule,
    NgbTooltipModule
  ]
})
export class SettingModule { }
