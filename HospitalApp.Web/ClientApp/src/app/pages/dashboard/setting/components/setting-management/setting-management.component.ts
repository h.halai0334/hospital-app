import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-setting-management',
  templateUrl: './setting-management.component.html',
  styleUrls: ['./setting-management.component.scss']
})
export class SettingManagementComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPassword() {
    this.router.navigateByUrl('/dashboard/password');
  }

  goToProfile() {
    this.router.navigateByUrl('/dashboard/profile');
  }

  goToNotification() {
    this.router.navigateByUrl('/dashboard/notifications');
  }

  goToLocalization() {
    this.router.navigateByUrl('/dashboard/localizations');
  }

  goToCopyrights() {
    this.router.navigateByUrl('/dashboard/copyrights');
  }

  goToPrices() {
    this.router.navigateByUrl('/dashboard/prices');
  }
}
