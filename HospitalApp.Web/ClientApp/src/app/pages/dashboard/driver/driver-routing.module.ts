import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DriverManagementComponent} from './components/driver-management/driver-management.component';

const routes: Routes = [
  {path: '', component: DriverManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DriverRoutingModule {
}
