import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DriverManagementComponent} from './components/driver-management/driver-management.component';
import {DriverRoutingModule} from './driver-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {VerifyDriverDialogComponent} from './components/verify-driver-dialog/verify-driver-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RejectDriverDialogComponent} from './components/reject-driver-dialog/reject-driver-dialog.component';

@NgModule({
  declarations: [DriverManagementComponent, VerifyDriverDialogComponent, RejectDriverDialogComponent],
  entryComponents: [VerifyDriverDialogComponent, RejectDriverDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    PipesModule,
    NgbModule,
    DriverRoutingModule,
    ReactiveFormsModule
  ]
})
export class DriverModule {
}
