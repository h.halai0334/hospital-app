import { Component, OnInit } from '@angular/core';
import {Driver} from '../../../../../models/driver';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DriverService} from '../../services/driver.service';
import {AlertService} from '../../../../../services/alert.service';

@Component({
  selector: 'app-reject-driver-dialog',
  templateUrl: './reject-driver-dialog.component.html',
  styleUrls: ['./reject-driver-dialog.component.scss']
})
export class RejectDriverDialogComponent implements OnInit {

  driver: Driver;
  saveEvent: () => void;
  cancelEvent: () => void;
  messageFormControl: FormControl;
  rejectDriverGroup: FormGroup;

  constructor(private driverService: DriverService, private alertService: AlertService) {
  }


  ngOnInit() {
  }
  isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  setDriver(driver: Driver) {
    this.driver = driver;
    this.initializeForm();
  }

  initializeForm() {
    this.messageFormControl = new FormControl([Validators.required]);
    this.rejectDriverGroup = new FormGroup({
      message: this.messageFormControl,
    });
  }

  sendEmail() {

  }


}
