import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DriverService} from '../../services/driver.service';
import {AlertService} from '../../../../../services/alert.service';
import {Driver} from '../../../../../models/driver';

@Component({
  selector: 'app-verify-driver-dialog',
  templateUrl: './verify-driver-dialog.component.html',
  styleUrls: ['./verify-driver-dialog.component.scss']
})
export class VerifyDriverDialogComponent implements OnInit {

  driver: Driver;
  saveEvent: (isVerified: boolean, isLocked: boolean) => void;
  cancelEvent: () => void;
  rejectEvent: () => void;
  licenseImageControl: FormControl;
  identityCardImageControl: FormControl;
  vehicleRegistrationImageControl: FormControl;
  vehiclePlateNoControl: FormControl;
  verifyDriverGroup: FormGroup;

  constructor(private driverService: DriverService, private alertService: AlertService) {
  }

  ngOnInit() {
  }

  setDriver(driver: Driver) {
    this.driver = driver;
    this.initializeForm();
    this.populateForm();
  }

  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.licenseImageControl.setValue(this.driver.licenseImage);
    this.identityCardImageControl.setValue(this.driver.identityCardImage);
    this.vehicleRegistrationImageControl.setValue(this.driver.vehicleRegistrationImage);
    this.licenseImageControl.setValue(this.driver.licenseImage);
    this.vehiclePlateNoControl.setValue(this.driver.vehiclePlateNo);
  }

  initializeForm() {
    this.licenseImageControl = new FormControl('',[Validators.required]);
    this.identityCardImageControl = new FormControl('',[Validators.required]);
    this.vehicleRegistrationImageControl = new FormControl('',[Validators.required]);
    this.vehiclePlateNoControl = new FormControl('',[Validators.required]);
    this.verifyDriverGroup = new FormGroup({
      licenseImage: this.licenseImageControl,
      identityCardImage: this.identityCardImageControl,
      vehicleRegistrationImage: this.vehicleRegistrationImageControl,
      plateNo: this.vehiclePlateNoControl,
    });
  }

  verify() {
    if (this.verifyDriverGroup.valid) {
      this.alertService.startLoadingMessage();
      this.driverService.verifyDriver(this.driver.id).subscribe(result => {
        this.alertService.stopLoadingMessage();
        if (this.saveEvent) {
          this.saveEvent(true, false);
        }
      });
    }
  }

  verifyAndLock() {
    if (this.verifyDriverGroup.valid) {
      this.alertService.startLoadingMessage();
      this.driverService.verifyDriver(this.driver.id, true).subscribe(result => {
        this.alertService.stopLoadingMessage();
        if (this.saveEvent) {
          this.saveEvent(true, true);
        }
      });
    }
  }

  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

  reject() {
    if (this.rejectEvent) {
      this.rejectEvent();
    }
  }
}
