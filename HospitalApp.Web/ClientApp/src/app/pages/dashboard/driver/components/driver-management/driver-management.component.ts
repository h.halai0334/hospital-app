import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {DriverService} from '../../services/driver.service';
import {Driver} from '../../../../../models/driver';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {VerifyDriverDialogComponent} from '../verify-driver-dialog/verify-driver-dialog.component';
import {RejectDriverDialogComponent} from '../reject-driver-dialog/reject-driver-dialog.component';

@Component({
  selector: 'app-driver-management',
  templateUrl: './driver-management.component.html',
  styleUrls: ['./driver-management.component.scss']
})
export class DriverManagementComponent implements OnInit {


  headers: TableHeader[] =
    [
      new TableHeader('Full Name', 'fullName', true),
      new TableHeader('Email', 'email', true),
      new TableHeader('Phone Number', 'phoneNumber', true),
      new TableHeader('Verification', 'IsVerified', true),
      new TableHeader('Status', 'isEnabled', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private driverService: DriverService, private modalService: NgbModal) {

  }

  get rows(): Driver[] {
    return this.driverService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.driverService.cachePage;
      this.totalPage = this.driverService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.driverService.getDrivers(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.driverService.cacheRows = result.data as Driver[];
      this.driverService.cachePage = this.page;
      this.totalPage = result.count;
      this.driverService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  viewDetail(row: Driver) {

  }

  blockDriver(row: Driver) {
    this.alertService.showDialog('Do you want to block ' + row.fullName + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Blocking...');
      this.alertService.closeDialog();
      this.driverService.blockDriver(row.id).subscribe(result => {
        row.isEnabled = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The user is blocked successfully', MessageSeverity.success);
      });
    });
  }

  unBlockDriver(row: Driver) {
    this.alertService.showDialog('Do you want to unblock ' + row.fullName + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('UnBlocking...');
      this.alertService.closeDialog();
      this.driverService.unBlockDriver(row.id).subscribe(result => {
        row.isEnabled = true;
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The user is unblocked successfully', MessageSeverity.success);
      });
    });
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  verifyDriver(row: Driver) {
    const modal = this.modalService.open(VerifyDriverDialogComponent);
    const instance = modal.componentInstance as VerifyDriverDialogComponent;
    instance.setDriver(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (isVerified, isLocked) => {
      modal.close();
      row.isVerified = isVerified;
      row.isLocked = isLocked;
      if (row.isLocked) {
        this.alertService.showMessage('Success', row.fullName + ' was verified and cannot change details', MessageSeverity.success);
      } else {
        this.alertService.showMessage('Success', row.fullName + ' was verified', MessageSeverity.success);
      }
    };
    instance.rejectEvent = () => {
      this.rejectDriver(row);
    };
  }

  rejectDriver(row: Driver) {
    const modal = this.modalService.open(RejectDriverDialogComponent, {
      backdrop: false
    });
    const instance = modal.componentInstance as RejectDriverDialogComponent;
    instance.setDriver(row);
    instance.saveEvent = () => {
      modal.close();
    };
    instance.cancelEvent = () => {
      modal.close();
    };
  }

}
