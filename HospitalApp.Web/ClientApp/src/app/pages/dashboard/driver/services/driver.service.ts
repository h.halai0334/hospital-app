import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Customer} from '../../../../models/customer';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Driver} from '../../../../models/driver';

@Injectable({
  providedIn: 'root'
})
export class DriverService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/driver';
  private readonly _blockUrl: string = this._baseUrl + '/block/';
  private readonly _unBlockUrl: string = this._baseUrl + '/unblock/';
  private readonly _verifyUrl: string = this._baseUrl + '/verify/';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get blockUrl() {
    return this.configurations.baseUrl + this._blockUrl;
  }

  get unblockUrl() {
    return this.configurations.baseUrl + this._unBlockUrl;
  }

  get verifyUrl() {
    return this.configurations.baseUrl + this._verifyUrl;
  }

  public cacheRows: Driver[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getDrivers(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  blockDriver(userId: string): Observable<ResponseModel> {
    return this.http.put(this.blockUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  verifyDriver(userId: string, isLockDetail = false): Observable<ResponseModel> {
    return this.http.put(this.verifyUrl + userId + '/' + isLockDetail, {
      isLockDetail: isLockDetail
    }, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  unBlockDriver(userId: string): Observable<ResponseModel> {
    return this.http.put(this.unblockUrl + userId, {
      isLockDetail: true
    }, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
