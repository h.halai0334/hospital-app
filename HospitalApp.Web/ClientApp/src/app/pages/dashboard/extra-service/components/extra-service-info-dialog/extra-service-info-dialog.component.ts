import {Component, OnInit} from '@angular/core';
import {ExtraService} from '../../../../../models/extra-service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExtraServiceService} from '../../services/extra-service.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';

@Component({
  selector: 'app-extra-service-info-dialog',
  templateUrl: './extra-service-info-dialog.component.html',
  styleUrls: ['./extra-service-info-dialog.component.scss']
})
export class ExtraServiceInfoDialogComponent implements OnInit {
  cancelEvent: () => void;
  saveEvent: (extraService: ExtraService) => void;
  isEdit = false;

  extraService: ExtraService;
  nameControl: FormControl;
  isActiveControl: FormControl;
  extraServiceGroup: FormGroup;

  constructor(private extraServiceService: ExtraServiceService, private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.extraService.name);
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.required]);
    this.isActiveControl = new FormControl('');
    this.extraServiceGroup = new FormGroup({
      name: this.nameControl,
    });
  }


  setExtraService(row: ExtraService = null) {
    this.initializeForm();
    if (row == null) {
      row = new ExtraService();
    } else {
      this.isEdit = true;
    }
    this.extraService = row;
    this.populateForm();
  }

  update() {
    if (this.extraServiceGroup.valid) {
      this.extraService.name = this.nameControl.value;
      this.alertService.startLoadingMessage('Updating...');
      this.extraServiceService.updateExtraService(this.extraService).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result.data as ExtraService;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.extraServiceGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    if (this.isEdit) {
      this.update();
      return;
    }
    this.extraService.name = this.nameControl.value;
    this.alertService.startLoadingMessage('Saving...');
    this.extraServiceService.createExtraService(this.extraService).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result.data as ExtraService;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
