import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ExtraServiceService} from '../../services/extra-service.service';
import {ExtraService} from '../../../../../models/extra-service';
import {ExtraServiceInfoDialogComponent} from '../extra-service-info-dialog/extra-service-info-dialog.component';

@Component({
  selector: 'app-extra-service-management',
  templateUrl: './extra-service-management.component.html',
  styleUrls: ['./extra-service-management.component.scss']
})
export class ExtraServiceManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Name', 'fullName', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private extraServiceService: ExtraServiceService, private modalService: NgbModal) {

  }

  get rows(): ExtraService[] {
    return this.extraServiceService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.extraServiceService.cachePage;
      this.totalPage = this.extraServiceService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.extraServiceService.getExtraServices(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.extraServiceService.cacheRows = result.data as ExtraService[];
      this.extraServiceService.cachePage = this.page;
      this.totalPage = result.count;
      this.extraServiceService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editExtraService(row: ExtraService) {
    const modal = this.modalService.open(ExtraServiceInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as ExtraServiceInfoDialogComponent;
    instance.setExtraService(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (extraService: ExtraService) => {
      modal.close();
      Object.assign(row, extraService);
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteExtraService(row: ExtraService) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.extraServiceService.deleteExtraService(row.id).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(ExtraServiceInfoDialogComponent,{
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as ExtraServiceInfoDialogComponent;
    instance.setExtraService(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (extraService: ExtraService) => {
      modal.close();
      this.alertService.showMessage('Success', extraService.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }
}
