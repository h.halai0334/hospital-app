import {Injectable} from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {ExtraService} from '../../../../models/extra-service';

@Injectable({
  providedIn: 'root'
})
export class ExtraServiceService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/extraService';
  private readonly _disableUrl: string = this._baseUrl + '/disable/';
  private readonly _enableUrl: string = this._baseUrl + '/enable/';
  private readonly _dropDownUrl: string = this._baseUrl + '/dropdown/';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get disableUrl() {
    return this.configurations.baseUrl + this._disableUrl;
  }

  get enableUrl() {
    return this.configurations.baseUrl + this._enableUrl;
  }

  get dropDownUrl() {
    return this.configurations.baseUrl + this._dropDownUrl;
  }


  public cacheRows: ExtraService[] = [];
  cachePage: number;
  cacheTotalSize: number;


  getExtraServicesForDropDown(): Observable<ResponseModel> {
    return this.http.get(this.dropDownUrl, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  getExtraServices(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }


  updateExtraService(extraService: ExtraService): Observable<ResponseModel> {
    return this.http.put(this.baseUrl + '/' + extraService.id, extraService, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  createExtraService(extraService: ExtraService): Observable<ResponseModel> {
    return this.http.post(this.baseUrl, extraService, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  deleteExtraService(id: number): Observable<ResponseModel> {
    return this.http.delete(this.baseUrl + '/' + id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));

  }

  disableExtraService(id: number): Observable<ResponseModel> {
    return this.http.put(this.disableUrl + id, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  enableExtraService(id: number): Observable<ResponseModel> {
    return this.http.put(this.enableUrl + id, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
