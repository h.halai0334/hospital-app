import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExtraServiceRoutingModule} from './extra-service-routing.module';
import { ExtraServiceManagementComponent } from './components/extra-service-management/extra-service-management.component';
import { ExtraServiceInfoDialogComponent } from './components/extra-service-info-dialog/extra-service-info-dialog.component';
import {PipesModule} from '../../../pipes/pipes.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [ExtraServiceManagementComponent, ExtraServiceInfoDialogComponent],
  entryComponents: [ExtraServiceInfoDialogComponent],
  imports: [
    CommonModule,
    ExtraServiceRoutingModule,
    PipesModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class ExtraServiceModule { }
