import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExtraServiceManagementComponent} from './components/extra-service-management/extra-service-management.component';

const routes: Routes = [
  {path: '', component: ExtraServiceManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExtraServiceRoutingModule {
}
