import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService} from '../../../../../services/alert.service';
import {TransactionService} from '../../services/transaction.service';
import {Transaction} from '../../../../../models/transaction';

@Component({
  selector: 'app-transaction-management',
  templateUrl: './transaction-management.component.html',
  styleUrls: ['./transaction-management.component.scss']
})
export class TransactionManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Ref#', 'id', true),
      new TableHeader('Sender', 'SenderUser.FullName', true),
      new TableHeader('Receiver', 'ReceiverUser.FullName', true),
      new TableHeader('Type', 'loadType', true),
      new TableHeader('Amount', 'amount', true),
      new TableHeader('Tax', 'tax', true),
      new TableHeader('Created Date', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private transactionService: TransactionService) {

  }

  get rows(): Transaction[] {
    return this.transactionService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.transactionService.cachePage;
      this.totalPage = this.transactionService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.transactionService.getTransactions(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.transactionService.cacheRows = result.data as Transaction[];
      this.transactionService.cachePage = this.page;
      this.totalPage = result.count;
      this.transactionService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }


  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

}
