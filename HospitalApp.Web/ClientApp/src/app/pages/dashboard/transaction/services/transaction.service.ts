import { Injectable } from '@angular/core';
import {Transaction} from '../../../../models/transaction';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/userTransaction';
  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }
  public cacheRows: Transaction[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getTransactions(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

}
