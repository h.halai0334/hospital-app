import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionManagementComponent } from './components/transaction-management/transaction-management.component';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PipesModule} from '../../../pipes/pipes.module';
import {TransactionRoutingModule} from './transaction-routing.module';

@NgModule({
  declarations: [TransactionManagementComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    PipesModule,
    TransactionRoutingModule
  ]
})
export class TransactionModule { }
