import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from '../../../../../models/user';
import {Form, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../../../../customValidator/custom-validator';
import {AlertService} from '../../../../../services/alert.service';
import {ProfileService} from '../../services/profile.service';

@Component({
  selector: 'app-profile-management',
  templateUrl: './profile-management.component.html',
  styleUrls: ['./profile-management.component.scss']
})
export class ProfileManagementComponent implements OnInit {

  nameControl: FormControl;
  phoneControl: FormControl;
  addressControl: FormControl;
  imageControl: FormControl;
  profileGroup: FormGroup;
  user: User = new User();

  constructor(private  alertService: AlertService, private profileService: ProfileService, private cd: ChangeDetectorRef) {
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.maxLength(50), Validators.required]);
    this.imageControl = new FormControl('', [Validators.required]);
    this.phoneControl = new FormControl('', [CustomValidator.Phone(), Validators.required]);
    this.addressControl = new FormControl('', [Validators.maxLength(255), Validators.required]);
    this.profileGroup = new FormGroup({
      name: this.nameControl,
      address: this.addressControl,
      phone: this.phoneControl,
      image: this.imageControl,
    });
  }

  ngOnInit() {
    this.addSampleData();
    this.initializeForm();
    this.populateForm();
  }

  populateForm() {
    this.addressControl.setValue(this.user.address);
    this.nameControl.setValue(this.user.fullName);
    this.phoneControl.setValue(this.user.phoneNumber);
    this.imageControl.setValue(this.user.image);
  }

  addSampleData() {
    this.user = this.user.sampleData();
  }

  save() {
    if (this.profileGroup.valid) {
      this.user.fullName = this.nameControl.value;
      this.user.phoneNumber = this.phoneControl.value;
      this.user.address = this.addressControl.value;
      this.alertService.startLoadingMessage('Saving...');
      this.profileService.updateProfile(this.user).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showSuccessMessage('Profile Updated Successfully');
      });
    } else {
      this.alertService.showErrorMessage('Please fill all the fields.');
    }
  }

  changeImage() {
    document.getElementById('fileToUpload2').click();
  }

  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.user.image = e.currentTarget.result;
        this.imageControl.setValue(this.user.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
}
