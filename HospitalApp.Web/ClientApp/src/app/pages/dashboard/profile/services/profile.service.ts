import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Company} from '../../../../models/company';
import {map} from 'rxjs/operators';
import {ResponseModel} from '../../../../models/response.model';
import {User} from '../../../../models/user';

@Injectable({
  providedIn: 'root'
})
export class ProfileService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/user';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  updateProfile(user: User) {
    return this.http.post(this.baseUrl, user, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
