import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {DiseaseSymptom} from '../../../../models/disease-symptom';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class DiseaseSymptomService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/diseaseSymptom';
  cachePage: number;
  cacheTotalSize: number;

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  public cacheRows: DiseaseSymptom[] = [];

  getDiseaseSymptoms(diseaseId: number, isInit: boolean, page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    const params = new HttpParams().set('isGetParent', isInit + '').set('diseaseId', diseaseId + '').set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false')
      .set('orderBy', orderBy + '');
    return this.http.get(this.baseUrl, {
      headers: this.getRequestHeaders().headers,
      params: params
    }).pipe(map((response) => <any>response));

  }

  createDiseaseSymptom(diseaseSymptom: DiseaseSymptom) {
    return this.http.post(this.baseUrl, diseaseSymptom, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <DiseaseSymptom>response));
  }


  deleteDiseaseSymptom(diseaseSymptom: DiseaseSymptom) {
    return this.http.delete(this.baseUrl + '/' + diseaseSymptom.diseaseId + '/symptom/' + diseaseSymptom.symptomId + '/symptomType/' + diseaseSymptom.symptomTypeId, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <DiseaseSymptom>response));
  }
}
