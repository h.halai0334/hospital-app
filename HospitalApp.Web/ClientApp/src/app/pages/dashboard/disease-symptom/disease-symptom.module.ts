import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiseaseSymptomManagementComponent } from './components/disease-symptom-management/disease-symptom-management.component';
import { DiseaseSymptomInfoDialogComponent } from './components/disease-symptom-info-dialog/disease-symptom-info-dialog.component';
import {DiseaseSymptomRoutingModule} from './disease-symptom-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [DiseaseSymptomManagementComponent, DiseaseSymptomInfoDialogComponent],
  imports: [
    CommonModule,
    DiseaseSymptomRoutingModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class DiseaseSymptomModule { }
