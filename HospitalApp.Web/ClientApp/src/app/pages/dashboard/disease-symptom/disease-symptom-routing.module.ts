import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DiseaseSymptomManagementComponent} from './components/disease-symptom-management/disease-symptom-management.component';



const routes: Routes = [
  {path: ':id', component: DiseaseSymptomManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiseaseSymptomRoutingModule { }
