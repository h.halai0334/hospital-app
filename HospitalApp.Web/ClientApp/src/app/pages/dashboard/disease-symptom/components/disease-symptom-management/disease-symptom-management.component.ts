import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {RouteDataStorageService} from '../../../../../services/route-data-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DiseaseSymptom} from '../../../../../models/disease-symptom';
import {DiseaseSymptomService} from '../../services/disease-symptom.service';
import {Disease} from '../../../../../models/disease';
import {DiseaseSymptomInfoDialogComponent} from '../disease-symptom-info-dialog/disease-symptom-info-dialog.component';

@Component({
  selector: 'app-disease-symptom-management',
  templateUrl: './disease-symptom-management.component.html',
  styleUrls: ['./disease-symptom-management.component.scss']
})
export class DiseaseSymptomManagementComponent implements OnInit {

  diseaseId: number;
  disease = new Disease();
  headers: TableHeader[] =
    [
      new TableHeader('Type', 'fullName', false),
      new TableHeader('Symptom', 'fullName', false),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService,
              private activatedRoute: ActivatedRoute,
              private diseaseSymptomService: DiseaseSymptomService,
              private router: Router,
              private modalService: NgbModal) {

  }

  get rows(): DiseaseSymptom[] {
    return this.diseaseSymptomService.cacheRows;
  }


  ngOnInit() {
    this.activatedRoute.params.subscribe(result => {
      this.diseaseId = +result['id'];
      this.initialize();
      this.initializeDataTable();
    });
  }

  initialize() {
    this.getData(true);
  }

  getData(isInit = false) {
    this.alertService.startLoadingMessage();
    this.diseaseSymptomService.getDiseaseSymptoms(this.diseaseId, isInit, this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      if (isInit) {
        this.disease = result.parent as Disease;
      }
      this.diseaseSymptomService.cacheRows = result.data as DiseaseSymptom[];
      this.diseaseSymptomService.cachePage = this.page;
      this.totalPage = result.count;
      this.diseaseSymptomService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }


  deleteDiseaseSymptom(row: DiseaseSymptom) {
    this.alertService.showDialog('Do you want to delete this symptom?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.diseaseSymptomService.deleteDiseaseSymptom(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(DiseaseSymptomInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as DiseaseSymptomInfoDialogComponent;
    instance.setDiseaseSymptom(this.diseaseId);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (diseaseSymptom: DiseaseSymptom) => {
      modal.close();
      this.alertService.showMessage('Success', 'Symptom was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

}
