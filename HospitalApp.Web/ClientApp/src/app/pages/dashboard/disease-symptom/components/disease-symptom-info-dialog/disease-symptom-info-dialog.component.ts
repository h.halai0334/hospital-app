import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {DiseaseSymptom} from '../../../../../models/disease-symptom';
import {DiseaseSymptomService} from '../../services/disease-symptom.service';
import {SymptomType} from '../../../../../models/symptom-type';
import {Symptom} from '../../../../../models/symptom';
import {SymptomTypeService} from '../../../symptom-type/services/symptom-type.service';
import {SymptomService} from '../../../symptom/services/symptom.service';

@Component({
  selector: 'app-disease-symptom-info-dialog',
  templateUrl: './disease-symptom-info-dialog.component.html',
  styleUrls: ['./disease-symptom-info-dialog.component.scss']
})
export class DiseaseSymptomInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (diseaseSymptom: DiseaseSymptom) => void;

  diseaseSymptom: DiseaseSymptom = new DiseaseSymptom();
  symptomControl: FormControl;
  symptomTypeControl: FormControl;
  diseaseSymptomGroup: FormGroup;

  symptomTypes: SymptomType[] = [];
  symptoms: Symptom[] = [];

  constructor(private diseaseSymptomService: DiseaseSymptomService,
              private cd: ChangeDetectorRef,
              private symptomService: SymptomService,
              private symptomTypeService: SymptomTypeService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }


  initializeForm() {
    this.symptomControl = new FormControl('0', [Validators.required]);
    this.symptomTypeControl = new FormControl('0', [Validators.required]);
    this.diseaseSymptomGroup = new FormGroup({
      symptom: this.symptomControl,
      symptomType: this.symptomTypeControl
    });
  }

  getData() {
    this.alertService.startLoadingMessage('Loading...');
    this.symptomTypeService.getSymptomTypesDropDown().subscribe(result => {
      this.alertService.stopLoadingMessage();
      this.symptomTypes = result;
    });
  }

  getSymptoms() {
    this.alertService.startLoadingMessage('Loading...');
    this.symptomService.getSymptomsDropDown(+this.symptomTypeControl.value).subscribe(result => {
      this.alertService.stopLoadingMessage();
      this.symptoms = result;
      this.symptomControl.setValue(0);
    });
  }

  setDiseaseSymptom(diseaseId: number) {
    this.initializeForm();
    this.diseaseSymptom.diseaseId = diseaseId;
    this.getData();
  }

  save() {
    if (this.diseaseSymptomGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    this.diseaseSymptom.symptomId = +this.symptomControl.value;
    this.diseaseSymptom.symptomTypeId = +this.symptomTypeControl.value;
    console.log(this.diseaseSymptom);
    this.alertService.startLoadingMessage('Saving...');
    this.diseaseSymptomService.createDiseaseSymptom(this.diseaseSymptom).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result as DiseaseSymptom;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
