import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {VehicleExtraService} from '../../../../models/vehicle-extra-service';

@Injectable({
  providedIn: 'root'
})
export class VehicleExtraServiceService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/vehicleExtraService';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  getVehicleExtraService(vehicleId: number, page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false')
      .set('orderBy', orderBy + '').set('vehicleId', vehicleId + '');
    return this.http.get(this.baseUrl, {
      headers: this.getRequestHeaders().headers,
      params: params
    }).pipe(map((response) => <ResponseModel>response));
  }

  updateVehicleService(vehicle: VehicleExtraService): Observable<ResponseModel> {
    return this.http.put(this.baseUrl + '/' + vehicle.id, vehicle, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  createVehicleService(vehicle: VehicleExtraService): Observable<ResponseModel> {
    return this.http.post(this.baseUrl, vehicle, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  deleteVehicleService(id: number): Observable<ResponseModel> {
    return this.http.delete(this.baseUrl + '/' + id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
