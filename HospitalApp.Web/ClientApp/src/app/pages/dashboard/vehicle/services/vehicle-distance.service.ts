import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Vehicle} from '../../../../models/vehicle';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {VehicleRange} from '../../../../models/vehicle-range';

@Injectable({
  providedIn: 'root'
})
export class VehicleDistanceService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/vehicleRangeType';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  updateVehicleDistance(vehicleTypeId: number, vehicle: VehicleRange[]): Observable<ResponseModel> {
    return this.http.put(this.baseUrl + '/' + vehicleTypeId, {
      vehicleTypeId,
      distanceModels: vehicle
    }, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

}
