import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Vehicle} from '../../../../models/vehicle';

@Injectable({
  providedIn: 'root'
})
export class VehicleService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/vehicleType';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }



  public cacheRows: Vehicle[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getVehicles(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  getVehicleDetail(id: number): Observable<ResponseModel> {
    return this.http.get(this.baseUrl + '/' + id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  updateVehicle(vehicle: Vehicle): Observable<ResponseModel> {
    return this.http.put(this.baseUrl + '/' + vehicle.id, vehicle, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  createVehicle(vehicle: Vehicle): Observable<ResponseModel> {
    return this.http.post(this.baseUrl, vehicle, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  deleteVehicle(id: number): Observable<ResponseModel> {
    return this.http.delete(this.baseUrl + '/' + id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
