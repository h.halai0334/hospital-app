import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VehicleManagementComponent} from './components/vehicle-management/vehicle-management.component';
import {VehicleDetailComponent} from './components/vehicle-detail/vehicle-detail.component';

const routes: Routes = [
  {path: '', component: VehicleManagementComponent},
  {path: 'detail/:id', component: VehicleDetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VehicleRoutingModule {
}
