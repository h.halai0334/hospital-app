import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VehicleExtraService} from '../../../../../models/vehicle-extra-service';
import {AlertService} from '../../../../../services/alert.service';
import {Vehicle} from '../../../../../models/vehicle';
import {ExtraService} from '../../../../../models/extra-service';
import {ExtraServiceService} from '../../../extra-service/services/extra-service.service';
import {VehicleExtraServiceService} from '../../services/vehicle-extra-service.service';

@Component({
  selector: 'app-vehicle-service-info-dialog',
  templateUrl: './vehicle-service-info-dialog.component.html',
  styleUrls: ['./vehicle-service-info-dialog.component.scss']
})
export class VehicleServiceInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (vehicleExtraService: VehicleExtraService) => void;
  isEdit = true;
  extraServiceControl: FormControl;
  isActiveControl: FormControl;
  chargeControl: FormControl;
  isImmediateControl: FormControl;
  isScheduledControl: FormControl;
  isOtherControl: FormControl;
  serviceGroup: FormGroup;
  vehicleExtraService: VehicleExtraService;
  extraServices: ExtraService[];
  isDropDownLoading = false;
  vehicleId: number;

  constructor(private alertService: AlertService, private  extraServiceService: ExtraServiceService, private vehicleExtraServiceService: VehicleExtraServiceService) {
  }

  ngOnInit() {
  }


  loadData() {
    this.isDropDownLoading = true;
    this.extraServiceService.getExtraServicesForDropDown().subscribe(result => {
      this.isDropDownLoading = false;
      this.extraServices = result.data as ExtraService[];
    }, error => {
      this.isDropDownLoading = false;
    });
  }

  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  initForm() {
    this.isOtherControl = new FormControl(false, [Validators.required]);
    this.isActiveControl = new FormControl(true, [Validators.required]);
    this.isImmediateControl = new FormControl(true, [Validators.required]);
    this.isScheduledControl = new FormControl(true, [Validators.required]);
    this.chargeControl = new FormControl(0, [Validators.required, Validators.min(1)]);
    this.extraServiceControl = new FormControl('', [Validators.required]);
    this.serviceGroup = new FormGroup({
      isActive: this.isActiveControl,
      isImmediate: this.isImmediateControl,
      isSchedule: this.isScheduledControl,
      charge: this.chargeControl,
      extraService: this.extraServiceControl,
      isOther: this.isOtherControl
    });
  }

  populateForm() {
    this.isActiveControl.setValue(this.vehicleExtraService.isActive);
    this.chargeControl.setValue(this.vehicleExtraService.charge);
    this.extraServiceControl.setValue(this.vehicleExtraService.extraServiceId);
    this.isImmediateControl.setValue(this.vehicleExtraService.isImmediate);
    this.isScheduledControl.setValue(this.vehicleExtraService.isSchedule);
    this.isOtherControl.setValue(this.vehicleExtraService.isOther);
  }

  setExtraService(vehicleId: number, vehicleExtraService: VehicleExtraService) {
    if (vehicleExtraService == null) {
      this.isEdit = false;
      vehicleExtraService = new VehicleExtraService();
      vehicleExtraService.isActive = true;
      vehicleExtraService.isImmediate = true;
      vehicleExtraService.isSchedule = true;
      vehicleExtraService.isOther = false;
      vehicleExtraService.charge = 0;
    }
    this.vehicleId = vehicleId;
    this.vehicleExtraService = vehicleExtraService;
    this.initForm();
    this.populateForm();
    this.loadData();
  }

  save() {
    if (this.serviceGroup.valid) {
      this.vehicleExtraService.extraServiceId = +this.extraServiceControl.value;
      this.vehicleExtraService.isActive = this.isActiveControl.value;
      this.vehicleExtraService.charge = this.chargeControl.value;
      this.vehicleExtraService.vehicleTypeId = this.vehicleId;
      this.vehicleExtraService.isOther = this.isOtherControl.value;
      this.vehicleExtraService.isImmediate = this.isImmediateControl.value;
      this.vehicleExtraService.isSchedule = this.isScheduledControl.value;
      if (this.isEdit) {
        this.update();
        return;
      }
      this.alertService.startLoadingMessage('Creating...');
      this.vehicleExtraServiceService.createVehicleService(this.vehicleExtraService).subscribe(result => {
        this.alertService.stopLoadingMessage();
        if (this.saveEvent) {
          this.saveEvent(result.data as VehicleExtraService);
        }
      });
    } else {
      this.alertService.showErrorMessage('Please fill the required fields');
    }
  }

  update() {
    this.alertService.startLoadingMessage('Updating...');
    this.vehicleExtraServiceService.updateVehicleService(this.vehicleExtraService).subscribe(result => {
      this.alertService.stopLoadingMessage();
      if (this.saveEvent) {
        this.saveEvent(result.data as VehicleExtraService);
      }
    });
  }

  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }
}
