import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {VehicleService} from '../../services/vehicle.service';
import {Vehicle} from '../../../../../models/vehicle';
import {VehicleInfoDialogComponent} from '../vehicle-info-dialog/vehicle-info-dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vehicle-management',
  templateUrl: './vehicle-management.component.html',
  styleUrls: ['./vehicle-management.component.scss']
})
export class VehicleManagementComponent implements OnInit {


  headers: TableHeader[] =
    [
      new TableHeader('#', 'name', false),
      new TableHeader('Name', 'name', true),
      new TableHeader('Status', 'isActive', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private vehicleService: VehicleService, private modalService: NgbModal, private router: Router) {

  }

  get rows(): Vehicle[] {
    return this.vehicleService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.vehicleService.cachePage;
      this.totalPage = this.vehicleService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.vehicleService.getVehicles(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.vehicleService.cacheRows = result.data as Vehicle[];
      this.vehicleService.cachePage = this.page;
      this.totalPage = result.count;
      this.vehicleService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editVehicle(row: Vehicle) {
    const modal = this.modalService.open(VehicleInfoDialogComponent, {
      backdrop: false
    });
    const instance = modal.componentInstance as VehicleInfoDialogComponent;
    instance.setVehicle(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (vehicle: Vehicle) => {
      modal.close();
      Object.assign(row, vehicle);
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteVehicle(row: Vehicle) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.vehicleService.deleteVehicle(row.id).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(VehicleInfoDialogComponent, {
      backdrop: false
    });
    const instance = modal.componentInstance as VehicleInfoDialogComponent;
    instance.setVehicle(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (vehicle: Vehicle) => {
      modal.close();
      this.alertService.showMessage('Success', vehicle.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

  viewVehicle(row: Vehicle) {
    this.router.navigateByUrl('/dashboard/vehicles/detail/' + row.id).catch(err => {
      this.alertService.showErrorMessage('Unable to navigate');
    });
  }
}
