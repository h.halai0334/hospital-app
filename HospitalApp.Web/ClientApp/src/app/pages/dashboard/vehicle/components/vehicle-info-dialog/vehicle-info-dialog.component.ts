import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {VehicleService} from '../../services/vehicle.service';
import {Vehicle} from '../../../../../models/vehicle';
import {DeliveryType} from '../../../../../enums/delivery-type.enum';

@Component({
  selector: 'app-vehicle-info-dialog',
  templateUrl: './vehicle-info-dialog.component.html',
  styleUrls: ['./vehicle-info-dialog.component.scss']
})
export class VehicleInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (vehicle: Vehicle) => void;
  isEdit = false;


  vehicle: Vehicle = new Vehicle();
  deliveryTypes = [
    { value: DeliveryType.DoorToDoor.toString(), text: 'Door To Door'},
    { value: DeliveryType.PointToPoint.toString(), text: 'Point To Point'},
  ];
  nameControl: FormControl;
  parcelLimitControl: FormControl;
  weightInKgLimitControl: FormControl;
  dimensionInCmLimitControl: FormControl;
  durationInMinLimitControl: FormControl;
  topUpChargesControl: FormControl;
  topUpDurationInMinControl: FormControl;
  deliveryTypeControl: FormControl;
  imageControl: FormControl;
  isActiveControl: FormControl;
  vehicleGroup: FormGroup;

  constructor(private vehicleService: VehicleService, private alertService: AlertService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.vehicle.name);
    this.imageControl.setValue(this.vehicle.image);
    this.parcelLimitControl.setValue(this.vehicle.parcelLimit);
    this.weightInKgLimitControl.setValue(this.vehicle.weightInKgLimit);
    this.dimensionInCmLimitControl.setValue(this.vehicle.dimensionInCmLimit);
    this.durationInMinLimitControl.setValue(this.vehicle.durationInMinLimit);
    this.topUpDurationInMinControl.setValue(this.vehicle.topUpDurationInMin);
    this.deliveryTypeControl.setValue(this.vehicle.deliveryType);
    this.topUpChargesControl.setValue(this.vehicle.topUpCharges);
    this.imageControl.setValue(this.vehicle.image);
    this.isActiveControl.setValue(this.vehicle.isActive ? 'true' : 'false');
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.required]);
    this.parcelLimitControl = new FormControl('0', [Validators.min(0)]);
    this.weightInKgLimitControl = new FormControl('0', [Validators.min(0)]);
    this.dimensionInCmLimitControl = new FormControl('0', [Validators.min(0)]);
    this.durationInMinLimitControl = new FormControl('0', [Validators.min(0)]);
    this.topUpChargesControl = new FormControl('0', [Validators.min(0)]);
    this.topUpDurationInMinControl = new FormControl('0', [Validators.min(0)]);
    this.deliveryTypeControl = new FormControl('0', [Validators.required]);
    this.imageControl = new FormControl('', [Validators.required]);
    this.isActiveControl = new FormControl('');
    this.vehicleGroup = new FormGroup({
      name: this.nameControl,
      isActive: this.isActiveControl,
      image: this.imageControl,
      parcelLimit: this.parcelLimitControl,
      weightInKgLimit: this.weightInKgLimitControl,
      dimensionInCmLimit: this.dimensionInCmLimitControl,
      durationInMinLimit: this.durationInMinLimitControl,
      topUpCharge: this.topUpChargesControl,
      topUpDuration: this.topUpDurationInMinControl,
      deliveryType: this.deliveryTypeControl
    });
  }


  setVehicle(row: Vehicle = null) {
    this.initializeForm();
    if (row == null) {
      row = new Vehicle();
    } else {
      this.isEdit = true;
    }
    Object.assign(this.vehicle, row);
    this.populateForm();
  }

  update() {
    if (this.vehicleGroup.valid) {
      this.alertService.startLoadingMessage('Updating...');
      this.vehicleService.updateVehicle(this.vehicle).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result.data as Vehicle;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.vehicleGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill all the required fields before submitting', MessageSeverity.error);
      return;
    }
    this.vehicle.name = this.nameControl.value;
    this.vehicle.parcelLimit = this.parcelLimitControl.value;
    this.vehicle.deliveryType = +this.deliveryTypeControl.value;
    this.vehicle.topUpCharges = this.topUpChargesControl.value;
    this.vehicle.topUpDurationInMin = this.topUpDurationInMinControl.value;
    this.vehicle.durationInMinLimit = this.durationInMinLimitControl.value;
    this.vehicle.dimensionInCmLimit = this.dimensionInCmLimitControl.value;
    this.vehicle.weightInKgLimit = this.weightInKgLimitControl.value;
    this.vehicle.isActive = this.isActiveControl.value;
    console.log(this.vehicle);
    console.log(this.isActiveControl.value);
    if (this.isEdit) {
      this.update();
      return;
    }
    this.alertService.startLoadingMessage('Saving...');
    this.vehicleService.createVehicle(this.vehicle).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result.data as Vehicle;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }


  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.vehicle.image = e.currentTarget.result;
        this.imageControl.setValue(this.vehicle.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
}
