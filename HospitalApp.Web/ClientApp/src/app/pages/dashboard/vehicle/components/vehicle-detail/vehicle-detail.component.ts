import {Component, OnInit} from '@angular/core';
import {Vehicle} from '../../../../../models/vehicle';
import {ActivatedRoute} from '@angular/router';
import {VehicleService} from '../../services/vehicle.service';
import {AlertService, DialogType} from '../../../../../services/alert.service';
import {TableHeader} from '../../../../../models/table-header';
import {VehicleExtraService} from '../../../../../models/vehicle-extra-service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {VehicleServiceInfoDialogComponent} from '../vehicle-service-info-dialog/vehicle-service-info-dialog.component';
import {VehicleExtraServiceService} from '../../services/vehicle-extra-service.service';
import {unwrapFirst} from 'codelyzer/util/function';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {VehicleRange} from '../../../../../models/vehicle-range';
import {VehicleDistanceService} from '../../services/vehicle-distance.service';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.scss']
})
export class VehicleDetailComponent implements OnInit {

  vehicleId: number;
  vehicle: Vehicle;
  distanceFormGroup: FormGroup;
  allControls: FormArray = new FormArray([]);
  serviceHeaders: TableHeader[] =
    [
      new TableHeader('Name', 'name', true),
      new TableHeader('Charge', 'charge', true),
      new TableHeader('Status', 'isActive', true),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 20;
  totalPage = 0;

  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }

  get serviceRows(): VehicleExtraService[] {
    if (this.vehicle !== undefined && this.vehicle !== null) {
      return this.vehicle.extraServices;
    }
    return [];
  }

  addService(vehicleExtraService: VehicleExtraService) {
    this.vehicle.extraServices.push(vehicleExtraService);
  }

  constructor(private activatedRoute: ActivatedRoute,
              private vehicleDistanceService: VehicleDistanceService,
              private vehicleExtraServiceService: VehicleExtraServiceService,
              private vehicleService: VehicleService,
              private alertService: AlertService,
              private modalService: NgbModal) {
  }

  get AllControls() {
    return this.allControls;
  }


  initForm() {
    this.distanceFormGroup = new FormGroup({
      allControl: this.allControls
    });
  }

  addControls() {
    for (let i = 0; i < this.vehicle.ranges.length; i++) {
      this.allControls.push(new FormControl(this.vehicle.ranges[i].baseCharge, [Validators.required]));
    }
  }

  ngOnInit() {
    this.initForm();
    this.activatedRoute.params.subscribe(p => {
      this.vehicleId = +p['id'];
      this.alertService.startLoadingMessage();
      this.vehicleService.getVehicleDetail(this.vehicleId).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.vehicle = result.data as Vehicle;
        this.addControls();
      });
    });
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  createService() {
    const modal = this.modalService.open(VehicleServiceInfoDialogComponent, {
      centered: true,
      backdrop: 'static'
    });
    const instance = modal.componentInstance as VehicleServiceInfoDialogComponent;
    instance.setExtraService(this.vehicleId, null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (vehicleExtraService) => {
      modal.close();
      this.addService(vehicleExtraService);
    };

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  private getData() {
    this.alertService.startLoadingMessage();
    this.vehicleExtraServiceService.getVehicleExtraService(this.vehicleId, this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.vehicle.extraServices = result.data as VehicleExtraService[];
      this.totalPage = result.count;
      this.alertService.stopLoadingMessage();
    });
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  editService(row: VehicleExtraService) {
    const modal = this.modalService.open(VehicleServiceInfoDialogComponent, {
      centered: true,
      backdrop: 'static'
    });
    const instance = modal.componentInstance as VehicleServiceInfoDialogComponent;
    instance.setExtraService(this.vehicleId, row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (vehicleExtraService) => {
      modal.close();
      Object.assign(row, vehicleExtraService);
    };
  }

  deleteService(row: VehicleExtraService) {
    this.alertService.showDialog('Do you want to delete this service?', DialogType.confirm, () => {
      this.vehicleExtraServiceService.deleteVehicleService(row.id).subscribe(result => {
        this.alertService.closeDialog();
        this.getData();
      });
    });
  }

  saveDistanceSetting() {
    if (this.distanceFormGroup.valid) {
      for (let i = 0; i < this.allControls.length; i++) {
        this.vehicle.ranges[i].baseCharge = +this.allControls.at(i).value;
      }
      this.alertService.startLoadingMessage('Saving...');
      this.vehicleDistanceService.updateVehicleDistance(this.vehicleId, this.vehicle.ranges).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showSuccessMessage('Settings Updated...');
      });
    } else {
      this.alertService.showErrorMessage('Please fill the required fields');
    }
  }
}
