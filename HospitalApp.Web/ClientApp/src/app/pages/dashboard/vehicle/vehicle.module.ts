import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VehicleManagementComponent} from './components/vehicle-management/vehicle-management.component';
import {VehicleInfoDialogComponent} from './components/vehicle-info-dialog/vehicle-info-dialog.component';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {PipesModule} from '../../../pipes/pipes.module';
import {VehicleRoutingModule} from './vehicle-routing.module';
import { VehicleDetailComponent } from './components/vehicle-detail/vehicle-detail.component';
import { VehicleServiceInfoDialogComponent } from './components/vehicle-service-info-dialog/vehicle-service-info-dialog.component';

@NgModule({
  declarations: [VehicleManagementComponent, VehicleInfoDialogComponent, VehicleDetailComponent, VehicleServiceInfoDialogComponent],
  entryComponents: [VehicleInfoDialogComponent, VehicleServiceInfoDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule,
    PipesModule,
    VehicleRoutingModule
  ]
})
export class VehicleModule {
}
