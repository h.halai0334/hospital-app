import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SymptomTypeManagementComponent} from './components/symptom-type-management/symptom-type-management.component';



const routes: Routes = [
  {path: '', component: SymptomTypeManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SymptomTypeRoutingModule { }
