import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {ImageFile} from '../../../../../constants/image-file';
import {CustomValidator} from '../../../../../customValidator/custom-validator';
import {SymptomType} from '../../../../../models/symptom-type';
import {SymptomTypeService} from '../../services/symptom-type.service';

@Component({
  selector: 'app-symptom-type-info-dialog',
  templateUrl: './symptom-type-info-dialog.component.html',
  styleUrls: ['./symptom-type-info-dialog.component.scss']
})
export class SymptomTypeInfoDialogComponent implements OnInit {

  cancelEvent: () => void;
  saveEvent: (symptomType: SymptomType) => void;
  isEdit = false;

  symptomType: SymptomType = new SymptomType();
  nameControl: FormControl;
  symptomTypeGroup: FormGroup;

  constructor(private symptomTypeService: SymptomTypeService,
              private cd: ChangeDetectorRef,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  get isLoading() {
    return this.alertService.isLoadingInProgress;
  }

  populateForm() {
    this.nameControl.setValue(this.symptomType.name);
  }

  initializeForm() {
    this.nameControl = new FormControl('', [Validators.required]);
    this.symptomTypeGroup = new FormGroup({
      name: this.nameControl,
    });
  }

  setSymptomType(row: SymptomType = null) {
    this.initializeForm();
    if (row == null) {
      row = new SymptomType();
    } else {
      this.isEdit = true;
      this.symptomTypeGroup.removeControl('password');
    }
    Object.assign(this.symptomType, row);
    this.populateForm();
  }

  update() {
    if (this.symptomTypeGroup.valid) {
      this.alertService.startLoadingMessage('Updating...');
      this.symptomTypeService.updateSymptomType(this.symptomType).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const data = result as SymptomType;
        this.saveEvent(data);
      });
    }
  }

  save() {
    if (this.symptomTypeGroup.invalid) {
      this.alertService.showMessage('Error', 'Please fill the name before submitting', MessageSeverity.error);
      return;
    }
    this.symptomType.name = this.nameControl.value;
    if (this.isEdit) {
      this.update();
      return;
    }
    this.alertService.startLoadingMessage('Saving...');
    this.symptomTypeService.createSymptomType(this.symptomType).subscribe(result => {
      this.alertService.stopLoadingMessage();
      const data = result as SymptomType;
      this.saveEvent(data);
    });

  }


  close() {
    if (this.cancelEvent) {
      this.cancelEvent();
    }
  }

}
