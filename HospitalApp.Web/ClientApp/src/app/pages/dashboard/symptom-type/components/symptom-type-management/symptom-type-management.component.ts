import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ViewImageDialogComponent} from '../../../../../shared/view-image-dialog/view-image-dialog.component';
import {SymptomTypeService} from '../../services/symptom-type.service';
import {SymptomType} from '../../../../../models/symptom-type';
import {SymptomTypeInfoDialogComponent} from '../symptom-type-info-dialog/symptom-type-info-dialog.component';
import {Router} from '@angular/router';
import {RouteDataStorageService} from '../../../../../services/route-data-storage.service';

@Component({
  selector: 'app-symptom-type-management',
  templateUrl: './symptom-type-management.component.html',
  styleUrls: ['./symptom-type-management.component.scss']
})
export class SymptomTypeManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Name', 'fullName', true),
      new TableHeader('Symptoms', 'fullName', false),
      new TableHeader('Created', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService,
              private routeDataStorageService: RouteDataStorageService,
              private symptomTypeService: SymptomTypeService,
              private router: Router,
              private modalService: NgbModal) {

  }

  get rows(): SymptomType[] {
    return this.symptomTypeService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.symptomTypeService.cachePage;
      this.totalPage = this.symptomTypeService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.symptomTypeService.getSymptomTypes(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.symptomTypeService.cacheRows = result.data as SymptomType[];
      this.symptomTypeService.cachePage = this.page;
      this.totalPage = result.count;
      this.symptomTypeService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  editSymptomType(row: SymptomType) {
    const modal = this.modalService.open(SymptomTypeInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as SymptomTypeInfoDialogComponent;
    instance.setSymptomType(row);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (symptomType: SymptomType) => {
      modal.close();
      row.name = symptomType.name;
      this.alertService.showMessage('Success', row.name + ' was updated Successfully', MessageSeverity.success);
    };
  }


  deleteSymptomType(row: SymptomType) {
    this.alertService.showDialog('Do you want to delete ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage('Deleting...');
      this.alertService.closeDialog();
      this.symptomTypeService.deleteSymptomType(row).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The service was deleted', MessageSeverity.success);
        this.getData();
      });
    });
  }

  create() {
    const modal = this.modalService.open(SymptomTypeInfoDialogComponent, {
      centered: true,
      backdrop: 'static',
    });
    const instance = modal.componentInstance as SymptomTypeInfoDialogComponent;
    instance.setSymptomType(null);
    instance.cancelEvent = () => {
      modal.close();
    };
    instance.saveEvent = (symptomType: SymptomType) => {
      modal.close();
      this.alertService.showMessage('Success', symptomType.name + ' was created Successfully', MessageSeverity.success);
      this.getData();
    };
  }

  viewSymptomType(row: SymptomType) {
    this.router.navigateByUrl('/dashboard/symptoms/' + row.id);
  }
}
