import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SymptomTypeManagementComponent } from './components/symptom-type-management/symptom-type-management.component';
import { SymptomTypeInfoDialogComponent } from './components/symptom-type-info-dialog/symptom-type-info-dialog.component';
import {SymptomTypeRoutingModule} from './symptom-type-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [SymptomTypeManagementComponent, SymptomTypeInfoDialogComponent],
  imports: [
    CommonModule,
    SymptomTypeRoutingModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class SymptomTypeModule { }
