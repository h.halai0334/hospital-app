import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {SymptomType} from '../../../../models/symptom-type';

@Injectable({
  providedIn: 'root'
})
export class SymptomTypeService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/symptomType';
  private readonly _dropDownUrl: string = this._baseUrl + '/dropDown';
  cachePage: number;
  cacheTotalSize: number;

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get dropDownUrl() {
    return this.configurations.baseUrl + this._dropDownUrl;
  }

  public cacheRows: SymptomType[] = [];

  getSymptomTypes(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  getSymptomTypesDropDown(): Observable<SymptomType[]> {
    return this.http.get(this.dropDownUrl, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <SymptomType[]>response));
  }

  createSymptomType(symptomType: SymptomType) {
    return this.http.post(this.baseUrl, symptomType, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <SymptomType>response));
  }

  updateSymptomType(symptomType: SymptomType) {
    return this.http.put(this.baseUrl + '/' + symptomType.id, symptomType, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <SymptomType>response));
  }

  deleteSymptomType(symptomType: SymptomType) {
    return this.http.delete(this.baseUrl + '/' + symptomType.id, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <SymptomType>response));
  }

}
