import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {AuthGuard} from '../../gaurds/auth-gaurd.guard';
import {RoleNames} from '../../constants/role-names';

const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  children: [
    // {
    //   path: 'home',
    //   loadChildren: './home/home.module#HomeModule',
    //   canActivateChild: [AuthGuard],
    //   data: {
    //     role: [RoleNames.Administrator]
    //   }
    // },
    // {
    //   path: 'home',
    //   loadChildren: './home/home.module#HomeModule',
    //   canActivateChild: [AuthGuard],
    //   data: {
    //     role: [RoleNames.Administrator]
    //   }
    // },
    {
      path: 'doctors',
      loadChildren: () => import('./doctor-page/doctor-page.module').then(p => p.DoctorPageModule),
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'symptomTypes',
      loadChildren: () => import('./symptom-type/symptom-type.module').then(p => p.SymptomTypeModule),
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'symptoms',
      loadChildren: () => import('./symptom/symptom.module').then(p => p.SymptomModule),
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'diseases',
      loadChildren: () => import('./disease/disease.module').then(p => p.DiseaseModule),
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'disease-symptoms',
      loadChildren: () => import('./disease-symptom/disease-symptom.module').then(p => p.DiseaseSymptomModule),
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'settings',
      loadChildren: './setting/setting.module#SettingModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule',
      canActivateChild: [AuthGuard],
    },
    {
      path: '',
      redirectTo: 'home',
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
