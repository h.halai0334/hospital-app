import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {FormControl} from '@angular/forms';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {Customer} from '../../../../../models/customer';
import {CustomerService} from '../../services/customer.service';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Full Name', 'fullName', true),
      new TableHeader('Email', 'email', true),
      new TableHeader('Phone Number', 'phoneNumber', true),
      new TableHeader('Status', 'status', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private customerService: CustomerService) {

  }

  get rows(): Customer[] {
    return this.customerService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.customerService.cachePage;
      this.totalPage = this.customerService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.customerService.getCustomers(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.customerService.cacheRows = result.data as Customer[];
      this.customerService.cachePage = this.page;
      this.totalPage = result.count;
      this.customerService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  viewDetail(row: Customer) {

  }

  blockCustomer(row: Customer) {
    this.alertService.showDialog('Do you want to block ' + row.fullName + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage();
      this.alertService.closeDialog();
      this.customerService.blockCustomer(row.id).subscribe(result => {
        row.isEnabled = false;
        this.alertService.closeDialog();
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The user is blocked successfully', MessageSeverity.success);
      });
    });
  }

  unBlockCustomer(row: Customer) {
    this.alertService.showDialog('Do you want to unblock ' + row.fullName + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage();
      this.customerService.unBlockCustomer(row.id).subscribe(result => {
        row.isEnabled = true;
        this.alertService.stopLoadingMessage();
        this.alertService.closeDialog();
        this.alertService.showMessage('Success', 'The user is unblocked successfully', MessageSeverity.success);
      });
    });
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

}
