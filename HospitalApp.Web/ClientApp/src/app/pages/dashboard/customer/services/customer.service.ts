import {Injectable} from '@angular/core';
import {ResponseModel} from '../../../../models/response.model';
import {Customer} from '../../../../models/customer';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends EndpointFactoryService {

  private readonly _baseUrl: string = '/api/v1/customer';
  private readonly _blockUrl: string = this._baseUrl + '/block/';
  private readonly _unBlockUrl: string = this._baseUrl + '/unblock/';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get blockUrl() {
    return this.configurations.baseUrl + this._blockUrl;
  }

  get unblockUrl() {
    return this.configurations.baseUrl + this._unBlockUrl;
  }

  public cacheRows: Customer[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getCustomers(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  blockCustomer(userId: string): Observable<ResponseModel> {
    return this.http.put(this.blockUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  unBlockCustomer(userId: string): Observable<ResponseModel> {
    return this.http.put(this.unblockUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
