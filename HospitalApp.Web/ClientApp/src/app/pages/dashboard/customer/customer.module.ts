import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerManagementComponent } from './components/customer-management/customer-management.component';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CustomerRoutingModule} from './customer-routing.module';

@NgModule({
  declarations: [CustomerManagementComponent],
  imports: [
    CommonModule,
    SharedModule,
    PipesModule,
    NgbModule,
    CustomerRoutingModule
  ]
})
export class CustomerModule { }
