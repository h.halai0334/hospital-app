import {Injectable} from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Company} from '../../../../models/company';
import {Observable, of} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {map} from 'rxjs/operators';
import {ConfigurationService} from '../../../../services/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/company';
  private readonly _blockUrl: string = this._baseUrl + '/block/';
  private readonly _unBlockUrl: string = this._baseUrl + '/unblock/';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get blockUrl() {
    return this.configurations.baseUrl + this._blockUrl;
  }

  get unblockUrl() {
    return this.configurations.baseUrl + this._unBlockUrl;
  }

  public cacheRows: Company[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getCompanies(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<ResponseModel> {
    if (ConfigurationService.isUseFakeApi) {
      const r = new ResponseModel();
      const company = new Company();
      r.data = [company.sampleData()];
      r.count = 1;
      return of(r);
    }
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  createCompany(company: Company) {
    return this.http.post(this.baseUrl, company, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  updateCompany(company: Company) {
    return this.http.post(this.baseUrl + '/' + company.id, company, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  blockCompany(userId: string): Observable<ResponseModel> {
    return this.http.put(this.blockUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  unBlockCompany(userId: string): Observable<ResponseModel> {
    return this.http.put(this.unblockUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
