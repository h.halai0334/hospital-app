import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CompanyManagementComponent} from './components/company-management/company-management.component';
import {CompanyCreateComponent} from './components/company-create/company-create.component';

const routes: Routes = [
  {path: 'view', component: CompanyManagementComponent, },
  {path: 'create', component: CompanyCreateComponent, },
  {path: '', redirectTo: 'view'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompanyRoutingModule {
}
