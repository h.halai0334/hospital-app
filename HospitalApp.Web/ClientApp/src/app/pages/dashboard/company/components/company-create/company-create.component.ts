import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CompanyService} from '../../services/company.service';
import {CustomValidator} from '../../../../../customValidator/custom-validator';
import {ServiceType} from '../../../../../models/service-type';
import {Company} from '../../../../../models/company';
import {Country} from '../../../../../models/country';
import {DataType} from '../../../../../enums/data-type.enum';
import {AlertService} from '../../../../../services/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CompanySuccessDialogComponent} from '../company-success-dialog/company-success-dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.scss']
})
export class CompanyCreateComponent implements OnInit {

  isShowPttDeviceError = false;
  currentStep = 0;
  maxStep = 2;
  isEdit = false;
  company = new Company();
  serviceTypes: ServiceType[] = [];
  countries: Country[] = [];
  imageControl: FormControl;
  nameControl: FormControl;
  emailControl: FormControl;
  phoneControl: FormControl;
  serviceControl: FormControl;
  infoFormGroup: FormGroup;

  addressControl: FormControl;
  flatControl: FormControl;
  unitControl: FormControl;
  floorControl: FormControl;
  remarksControl: FormControl;
  countryControl: FormControl;
  locationFromGroup: FormGroup;
  noOfLicenseControl: FormControl;
  pttDeviceControl: FormControl;
  startDateControl: FormControl;
  endDateControl: FormControl;
  licenseFormGroup: FormGroup;

  constructor(private  companyService: CompanyService, private router: Router, private cd: ChangeDetectorRef, private alertService: AlertService, private modal: NgbModal) {
  }

  ngOnInit() {
    this.initForms();
    this.addSampleData();
  }

  addSampleData() {
    this.serviceTypes = [
      {id: 1, name: 'Construction', createdDate: ''},
      {id: 2, name: 'Bank', createdDate: ''},
      {id: 3, name: 'AC Repair', createdDate: ''},
    ];
    this.countries = [
      {id: 1, name: 'Singapore', createdDate: ''},
      {id: 2, name: 'Pakistan', createdDate: ''},
    ];
  }

  initForms() {
    this.noOfLicenseControl = new FormControl('', [Validators.required, Validators.max(50)]);
    this.pttDeviceControl = new FormControl('', [Validators.required, Validators.minLength(15)]);
    this.startDateControl = new FormControl('', [Validators.required]);
    this.endDateControl = new FormControl('', [Validators.required, CustomValidator.GreaterThan(this.startDateControl, DataType.Date)]);
    this.licenseFormGroup = new FormGroup({
      noOfLicense: this.noOfLicenseControl,
      startDate: this.startDateControl,
      endDate: this.endDateControl,
    });
    this.imageControl = new FormControl('', [Validators.required]);
    this.nameControl = new FormControl('', [Validators.required, Validators.maxLength(50)]);
    this.emailControl = new FormControl('', [Validators.required, Validators.email, Validators.maxLength(50)]);
    this.phoneControl = new FormControl('', [Validators.required, CustomValidator.Phone()]);
    this.serviceControl = new FormControl('', [Validators.required]);
    this.infoFormGroup = new FormGroup({
      image: this.imageControl,
      name: this.nameControl,
      email: this.emailControl,
      phone: this.phoneControl,
      service: this.serviceControl,
    });

    this.addressControl = new FormControl('', [Validators.required]);
    this.countryControl = new FormControl('', [Validators.required]);
    this.flatControl = new FormControl('', [Validators.maxLength(12)]);
    this.unitControl = new FormControl('', [Validators.maxLength(12)]);
    this.floorControl = new FormControl('', [Validators.maxLength(12)]);
    this.remarksControl = new FormControl('', [Validators.maxLength(255)]);
    this.locationFromGroup = new FormGroup({
      address: this.addressControl,
      country: this.countryControl,
      flat: this.flatControl,
      unit: this.unitControl,
      floor: this.floorControl,
      remarks: this.remarksControl,
    });
  }

  changeImage() {
    document.getElementById('fileToUpload2').click();
  }

  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.company.image = e.currentTarget.result;
        this.imageControl.setValue(this.company.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  nextStep() {
    this.currentStep++;
    if (this.currentStep >= this.maxStep) {
      this.currentStep = this.maxStep;
    }
  }

  prevStep() {
    this.currentStep--;
    if (this.currentStep <= 0) {
      this.currentStep = 0;
    }
  }

  addImeiNo() {
    if (this.pttDeviceControl.valid) {
      const device = this.pttDeviceControl.value + '';
      this.company.deviceImeiNos.push(device);
      this.pttDeviceControl.setValue('');
    } else {
      this.isShowPttDeviceError = true;
      setTimeout(() => {
        this.isShowPttDeviceError = false;
      }, 3000);
    }
  }

  removeImeiNo(d: string) {
    const index = this.company.deviceImeiNos.findIndex(p => p === d);
    if (index > -1) {
      this.company.deviceImeiNos.splice(index, 1);
    }
  }

  create() {
    if (this.licenseFormGroup.valid && this.infoFormGroup.valid && this.locationFromGroup.valid) {
      if (!this.company.deviceImeiNos || this.company.deviceImeiNos.length === 0) {
        this.alertService.showWarningMessage('Please Enter at least one PTT Device');
        return;
      }
      this.company.name = this.nameControl.value;
      this.company.email = this.emailControl.value;
      this.company.address = this.addressControl.value;
      this.company.noOfLicenses = +this.noOfLicenseControl.value;
      this.company.serviceType = this.serviceControl.value;
      this.company.country = this.countryControl.value;
      this.company.flat = this.flatControl.value;
      this.company.floor = this.floorControl.value;
      this.company.unit = this.unitControl.value;
      this.company.remarks = this.remarksControl.value;
      this.company.startDate = this.startDateControl.value;
      this.company.endDate = this.endDateControl.value;
      this.alertService.startLoadingMessage('Saving...');
      this.companyService.createCompany(this.company).subscribe(result => {
          this.alertService.stopLoadingMessage();
          this.openSuccessDialog();
      });
    }
  }

  openSuccessDialog() {
    const dialog = this.modal.open(CompanySuccessDialogComponent, {
      backdrop: 'static',
      centered: true,
    });
    const instance = dialog.componentInstance as CompanySuccessDialogComponent;
    instance.viewCompanyEvent = () => {
      dialog.close();
      this.router.navigateByUrl('dashboard/companies/view');
    };
    instance.createAnotherCompanyEvent = () => {
      dialog.close();
      this.company = new Company();
      this.currentStep = 0;
      this.locationFromGroup.reset();
      this.infoFormGroup.reset();
      this.licenseFormGroup.reset();
    };
  }
}
