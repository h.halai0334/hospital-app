import {Component, OnInit} from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {CompanyService} from '../../services/company.service';
import {Company} from '../../../../../models/company';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-company-management',
  templateUrl: './company-management.component.html',
  styleUrls: ['./company-management.component.scss']
})
export class CompanyManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Name', 'name', true),
      new TableHeader('Email', 'email', true),
      new TableHeader('Phone Number', 'phoneNumber', true),
      new TableHeader('Licenses *', 'licenses', true, 'Used / Available'),
      new TableHeader('Status', 'status', true),
      new TableHeader('From', 'from', true),
      new TableHeader('To', 'to', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private companyService: CompanyService, private modalService: NgbModal, private router: Router) {

  }

  get rows(): Company[] {
    return this.companyService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    if (!this.rows || this.rows.length === 0) {
      this.getData();
    } else {
      this.page = this.companyService.cachePage;
      this.totalPage = this.companyService.cacheTotalSize;
    }
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.companyService.getCompanies(this.page, this.pageSize, this.search, this.sort, this.direction).subscribe(result => {
      this.companyService.cacheRows = result.data as Company[];
      this.companyService.cachePage = this.page;
      this.totalPage = result.count;
      this.companyService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  viewDetail(row: Company) {

  }

  blockCompany(row: Company) {
    this.alertService.showDialog('Do you want to block ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage();
      this.alertService.closeDialog();
      this.companyService.blockCompany(row.id).subscribe(result => {
        row.isEnabled = false;
        this.alertService.closeDialog();
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'The user is blocked successfully', MessageSeverity.success);
      });
    });
  }

  unBlockCompany(row: Company) {
    this.alertService.showDialog('Do you want to unblock ' + row.name + '?', DialogType.confirm, () => {
      this.alertService.startLoadingMessage();
      this.companyService.unBlockCompany(row.id).subscribe(result => {
        row.isEnabled = true;
        this.alertService.stopLoadingMessage();
        this.alertService.closeDialog();
        this.alertService.showMessage('Success', 'The user is unblocked successfully', MessageSeverity.success);
      });
    });
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }

  create() {
    this.alertService.startLoadingMessage('Navigating Please wait..');
    this.router.navigateByUrl('/dashboard/companies/create').then(() => {
      this.alertService.stopLoadingMessage();
    }).catch((e) => {
      this.alertService.stopLoadingMessage();
      this.alertService.showErrorMessage('Error while navigating');
      console.log(e);
    });
    // const modal = this.modalService.open(CompanyInfoDialogComponent, {
    //   centered: true,
    //   backdrop: 'static',
    // });
    // const instance = modal.componentInstance as CompanyInfoDialogComponent;
    // instance.setCompany(null);
    // instance.cancelEvent = () => {
    //   modal.close();
    // };
    // instance.saveEvent = (company: Company) => {
    //   modal.close();
    //   this.alertService.showMessage('Success', company.name + ' was created Successfully', MessageSeverity.success);
    //   this.getData();
    // };
  }

  editDetail(row: Company) {
    this.router.navigateByUrl('dashboard/companies/create?id=' + row.id);
  }
}
