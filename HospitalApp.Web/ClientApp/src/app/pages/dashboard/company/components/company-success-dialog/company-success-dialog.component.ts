import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-success-dialog',
  templateUrl: './company-success-dialog.component.html',
  styleUrls: ['./company-success-dialog.component.scss']
})
export class CompanySuccessDialogComponent implements OnInit {


  createAnotherCompanyEvent: () => void;
  viewCompanyEvent: () => void;
  constructor() { }

  ngOnInit() {
  }

  createAnother() {
    if (this.createAnotherCompanyEvent) {
      this.createAnotherCompanyEvent();
    }
  }

  viewCompany() {
    if (this.viewCompanyEvent) {
      this.viewCompanyEvent();
    }
  }

}
