import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyManagementComponent } from './components/company-management/company-management.component';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CompanyRoutingModule} from './company-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CompanyCreateComponent } from './components/company-create/company-create.component';
import { CompanySuccessDialogComponent } from './components/company-success-dialog/company-success-dialog.component';

@NgModule({
  declarations: [CompanyManagementComponent, CompanyCreateComponent, CompanySuccessDialogComponent],
  entryComponents: [CompanySuccessDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    PipesModule,
    NgbModule,
    FormsModule,
    CompanyRoutingModule,
    ReactiveFormsModule
  ]
})
export class CompanyModule { }
