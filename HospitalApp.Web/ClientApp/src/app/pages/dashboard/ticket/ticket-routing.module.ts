import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TicketManagementComponent} from './components/ticket-management/ticket-management.component';
import {ResolvedTicketManagementComponent} from './components/resolved-ticket-management/resolved-ticket-management.component';
import {TicketDetailComponent} from './components/ticket-detail/ticket-detail.component';

const routes: Routes = [
  {path: 'pending', component: TicketManagementComponent},
  {path: 'resolved', component: ResolvedTicketManagementComponent},
  {path: 'detail/:id', component: TicketDetailComponent},
  {path: '', redirectTo: 'pending'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketRoutingModule { }
