import { Injectable } from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Ticket} from '../../../../models/ticket';
import {Observable, of} from 'rxjs';
import {ResponseModel} from '../../../../models/response.model';
import {ConfigurationService} from '../../../../services/configuration.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TicketService extends EndpointFactoryService {
  private readonly _baseUrl: string = '/api/v1/ticket';
  private readonly _processUrl: string = this._baseUrl + '/process/';
  private readonly _resolvedUrl: string = this._baseUrl + '/resolve/';

  get baseUrl() {
    return this.configurations.baseUrl + this._baseUrl;
  }

  get processUrl() {
    return this.configurations.baseUrl + this._processUrl;
  }

  get resolveUrl() {
    return this.configurations.baseUrl + this._resolvedUrl;
  }

  public cacheRows: Ticket[] = [];
  cachePage: number;
  cacheTotalSize: number;

  getTickets(page = 1, pageSize = 10, search = '', orderBy = '', direction = '', status = 0): Observable<ResponseModel> {
    if (ConfigurationService.isUseFakeApi) {
      const r = new ResponseModel();
      const ticket = new Ticket();
      const t = ticket.sampleData();
      t.status = status;
      r.data = [t];
      r.count = 1;
      return of(r);
    }
    return this.getPaginateRequest(this.baseUrl, page, pageSize, search, orderBy, direction);
  }

  createTicket(ticket: Ticket) {
    return this.http.post(this.baseUrl, ticket, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  updateTicket(ticket: Ticket) {
    return this.http.post(this.baseUrl + '/' + ticket.id, ticket, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  processTicket(userId: string): Observable<ResponseModel> {
    return this.http.put(this.processUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }

  resolveTicket(userId: string): Observable<ResponseModel> {
    return this.http.put(this.processUrl + userId, null, {
      headers: this.getRequestHeaders().headers,
    }).pipe(map((response) => <ResponseModel>response));
  }
}
