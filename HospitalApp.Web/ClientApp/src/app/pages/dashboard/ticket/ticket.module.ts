import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketManagementComponent } from './components/ticket-management/ticket-management.component';
import {NgbPaginationModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedModule} from '../../../shared/shared.module';
import {PipesModule} from '../../../pipes/pipes.module';
import { ResolvedTicketManagementComponent } from './components/resolved-ticket-management/resolved-ticket-management.component';
import {TicketRoutingModule} from './ticket-routing.module';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';

@NgModule({
  declarations: [TicketManagementComponent, ResolvedTicketManagementComponent, TicketDetailComponent],
  imports: [
    CommonModule,
    TicketRoutingModule,
    NgbPaginationModule,
    NgbTooltipModule,
    SharedModule,
    PipesModule
  ]
})
export class TicketModule { }
