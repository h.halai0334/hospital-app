import { Component, OnInit } from '@angular/core';
import {TableHeader} from '../../../../../models/table-header';
import {AlertService} from '../../../../../services/alert.service';
import {TicketService} from '../../services/ticket.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Ticket} from '../../../../../models/ticket';

@Component({
  selector: 'app-resolved-ticket-management',
  templateUrl: './resolved-ticket-management.component.html',
  styleUrls: ['./resolved-ticket-management.component.scss']
})
export class ResolvedTicketManagementComponent implements OnInit {

  headers: TableHeader[] =
    [
      new TableHeader('Status', 'status', true),
      new TableHeader('Ticket No', 'ticketNo', true),
      new TableHeader('Name', 'Name', true),
      new TableHeader('Phone Number', 'phoneNumber', true),
      new TableHeader('Email', 'email', true),
      new TableHeader('Subject', 'subject', true),
      new TableHeader('Resolved Date', 'createdDate', true),
      new TableHeader('Action', 'action', false),
    ];
  prevHead: TableHeader = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 1;
  pageSize = 10;
  totalPage = 0;

  constructor(private alertService: AlertService, private ticketService: TicketService, private modalService: NgbModal, private router: Router) {

  }

  get rows(): Ticket[] {
    return this.ticketService.cacheRows;
  }


  ngOnInit() {
    this.initialize();
    this.initializeDataTable();
  }

  initialize() {
    this.getData();
  }

  getData() {
    this.alertService.startLoadingMessage();
    this.ticketService.getTickets(this.page, this.pageSize, this.search, this.sort, this.direction, 2).subscribe(result => {
      this.ticketService.cacheRows = result.data as Ticket[];
      this.ticketService.cachePage = this.page;
      this.totalPage = result.count;
      this.ticketService.cacheTotalSize = this.totalPage;
      this.alertService.stopLoadingMessage();
    });
  }


  get isLoading(): boolean {
    return this.alertService.isLoadingInProgress;
  }


  initializeDataTable() {

  }

  onSearchChanged(search: string) {
    this.search = search;
    this.getData();
  }

  viewDetail(row: Ticket) {
    this.router.navigateByUrl('dashboard/tickets/detail/' + row.id);
  }

  sortData(head: TableHeader) {
    if (!head.sortable) {
      return;
    }
    if (this.sort === head.sort) {
      this.prevHead = head;
      if (this.direction === 'desc') {
        this.direction = 'asc';
        this.prevHead.sortingDirection = true;
      } else {
        this.direction = 'desc';
        this.prevHead.sortingDirection = false;
      }
    } else {
      if (this.prevHead) {
        this.prevHead.revertSorting();
        this.prevHead = null;
      }
      this.sort = head.sort;
      this.direction = 'desc';
      this.prevHead = head;
      this.prevHead.sortingDirection = false;
    }
    this.getData();
  }

  refresh() {
    this.search = '';
    this.page = 1;
    this.totalPage = 1;
    this.getData();
  }

  changePage(page: number) {
    this.page = page;
    this.getData();
  }


}
