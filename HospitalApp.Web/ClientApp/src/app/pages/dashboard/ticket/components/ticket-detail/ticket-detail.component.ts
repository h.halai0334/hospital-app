import {Component, OnInit} from '@angular/core';
import {Ticket} from '../../../../../models/ticket';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit {

  ticket: Ticket;

  constructor() {
  }

  ngOnInit() {
    this.addSampleData();
  }

  addSampleData() {
    this.ticket = new Ticket().sampleData();
  }
}
