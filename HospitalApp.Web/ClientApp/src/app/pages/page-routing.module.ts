import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'login',
      loadChildren: './login/login.module#LoginModule',
    },
    {
      path: 'logout',
      loadChildren: './logout/logout.module#LogoutModule',
    },
    {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule',
    },
    {
      path: 'set-password',
      loadChildren: './set-password/set-password.module#SetPasswordModule',
    },
    {
      path: '',
      redirectTo: 'login',
    },
    {
      path: '**',
      loadChildren: './not-found/not-found.module#NotFoundModule',
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule { }
