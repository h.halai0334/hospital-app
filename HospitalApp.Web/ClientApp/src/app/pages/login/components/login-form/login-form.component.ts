import { Component, OnInit } from '@angular/core';
import {UserLogin} from '../../../../models/user-login.model';
import {AlertService, MessageSeverity} from '../../../../services/alert.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RoleNames} from '../../../../constants/role-names';
import {AuthService} from '../../../../services/auth.service';
import {Router} from '@angular/router';
import {LoginService} from '../../services/login.service';
import {CustomValidator} from '../../../../customValidator/custom-validator';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {


  currentDate = new Date();
  loginFormGroup: FormGroup;
  userNameControl: FormControl;
  passwordControl: FormControl;
  constructor(private authService: AuthService,
              public alertService: AlertService,
              private loginService: LoginService,
              private router: Router) {
  }



  ngOnInit() {
    this.initForm();
    this.authService.reevaluateLoginStatus();
    if (this.authService.isLoggedIn) {
      this.authService.redirectLoggedInUser();
    }
  }


  initForm() {
    this.userNameControl = new FormControl('', [Validators.required, Validators.email]);
    this.passwordControl = new FormControl('', [Validators.required]);
    this.loginFormGroup = new FormGroup({
      userName: this.userNameControl,
      password: this.passwordControl,
    });
  }

  goToRegisterPage() {
    this.router.navigateByUrl('/merchant-registration');
  }

  formSubmit() {
    if (this.loginFormGroup.valid) {
      this.alertService.startLoadingMessage('Authenticating...');
      const userLogin = new UserLogin();
      userLogin.email = this.userNameControl.value;
      userLogin.password = this.passwordControl.value;
      this.loginService.login(userLogin).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const user = this.authService.processLogin(result);
        this.alertService.showMessage('Success', 'Welcome ' + user.fullName, MessageSeverity.success);
        this.authService.redirectLoggedInUser();

      });
    }
  }
}
