export enum PromoRangeType {
  /// <summary>
  /// Date Range = 1
  /// e.g 10 Jan - 12 Jan
  /// </summary>
  DateRange = 1,
  /// <summary>
  /// Day of Week = 2
  /// Money, Friday,etc
  /// </summary>
  DayOfWeek = 2,
  /// <summary>
  /// Time Range = 3
  /// 10 AM to 10 PM
  /// </summary>
  TimeRange = 3,
  /// <summary>
  /// Price Range - 4 Min Max
  /// </summary>
  PriceRange = 4,
  /// <summary>
  /// No Range - 5
  /// </summary>
  NoRange = 5,
}
