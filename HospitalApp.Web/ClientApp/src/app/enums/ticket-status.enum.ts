export enum TicketStatus {
  Pending,
  Processing,
  Resolved
}
