export class RoleNames {
  public static readonly Administrator = 'Administrator';
  public static readonly Staff = 'Staff';
  public static readonly Manager = 'Manager';
  public static readonly Company = 'Company';
  public static readonly Worker = 'Worker';
}
