export class ImageFile {
  public static readonly DefaultUserImage = '/assets/img/default-user.png';
  public static readonly DefaultImage = '/assets/img/default.jpg';
}
