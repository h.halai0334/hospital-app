import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SidebarComponent} from './sidebar/sidebar.component';
import {RouterModule} from '@angular/router';
import { SearchBoxComponent } from './search-box/search-box.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { NavbarComponent } from './navbar/navbar.component';
import {FormsModule} from '@angular/forms';
import { FormErrorComponent } from './form-error/form-error.component';
import { ViewImageDialogComponent } from './view-image-dialog/view-image-dialog.component';

@NgModule({
  declarations: [SidebarComponent, SearchBoxComponent, PaginatorComponent, NavbarComponent, FormErrorComponent, ViewImageDialogComponent],
    exports: [SidebarComponent, SearchBoxComponent, NavbarComponent, FormErrorComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ]
})
export class SharedModule { }
