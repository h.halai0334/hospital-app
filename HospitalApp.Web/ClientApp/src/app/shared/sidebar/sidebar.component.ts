import {Component, OnInit, AfterViewInit, AfterViewChecked, AfterContentInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {RoleNames} from '../../constants/role-names';

// Metadata
export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  collapse?: string;
  icontype: string;
  // icon: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}

// Menu Items

export const ADMINUPPERROUTES: RouteInfo[] = [
  {path: '/dashboard/profile', title: 'Profile', type: 'link', icontype: 'fas fa fa-user'},
  {path: '/logout', title: 'Logout', type: 'link', icontype: 'fas fa fa-sign-out-alt'},
];

export const ADMINROUTES: RouteInfo[] = [
  // {path: '/dashboard/home', title: 'Home', type: 'link', icontype: 'fas fa fa-home'},
  {path: '/dashboard/doctors', title: 'Doctors', type: 'link', icontype: 'fas fa fa-user-md'},
  {path: '/dashboard/symptomTypes', title: 'Symptom Types', type: 'link', icontype: 'fas fa fa-syringe'},
  {path: '/dashboard/diseases', title: 'Diseases', type: 'link', icontype: 'fas fa fa-biohazard'},
  // {path: '/dashboard/languages', title: 'Languages', type: 'link', icontype: 'fas fa fa-language'},
  // {
  //   path: '/dashboard/companies/', title: 'Companies', type: 'sub', icontype: 'fas fa fa-building', collapse: 'components', children: [
  //     {path: 'create', title: 'Create Company', ab: 'CC', type: 'link'},
  //     {path: 'view', title: 'View Companies', ab: 'VC', type: 'link'},
  //   ]
  // },
  // {
  //   path: '/dashboard/tickets/', title: 'Tickets', type: 'sub', icontype: 'fas fa fa-ticket-alt', collapse: 'tickets', children: [
  //     {path: 'resolved', title: 'Resolved', ab: 'R', type: 'link'},
  //     {path: 'pending', title: 'Pending', ab: 'P', type: 'link'},
  //   ]
  // },
  // {path: '/dashboard/settings', title: 'Settings', type: 'link', icontype: 'fas fa fa-cog'},
];
// export const ROUTES: RouteInfo[] = [{
//         path: '/dashboard',
//         title: 'Dashboard',
//         type: 'link',
//         icontype: 'nc-icon nc-bank'
//     }, {
//         path: '/components',
//         title: 'Components',
//         type: 'sub',
//         collapse: 'components',
//         icontype: 'nc-icon nc-layout-11',
//         children: [
//             {path: 'buttons', title: 'Buttons', ab: 'B'},
//             {path: 'grid', title: 'Grid System', ab: 'GS'},
//             {path: 'panels', title: 'Panels', ab: 'P'},
//             {path: 'sweet-alert', title: 'Sweet Alert', ab: 'SA'},
//             {path: 'notifications', title: 'Notifications', ab: 'N'},
//             {path: 'icons', title: 'Icons', ab: 'I'},
//             {path: 'typography', title: 'Typography', ab: 'T'}
//         ]
//     }, {
//         path: '/forms',
//         title: 'Forms',
//         type: 'sub',
//         collapse: 'forms',
//         icontype: 'nc-icon nc-ruler-pencil',
//         children: [
//             {path: 'regular', title: 'Regular Forms', ab: 'RF'},
//             {path: 'extended', title: 'Extended Forms', ab: 'EF'},
//             {path: 'validation', title: 'Validation Forms', ab: 'VF'},
//             {path: 'wizard', title: 'Wizard', ab: 'W'}
//         ]
//     }, {
//         path: '/tables',
//         title: 'Tables',
//         type: 'sub',
//         collapse: 'tables',
//         icontype: 'nc-icon nc-single-copy-04',
//         children: [
//             {path: 'regular', title: 'Regular Tables', ab: 'RT'},
//             {path: 'extended', title: 'Extended Tables', ab: 'ET'},
//             {path: 'datatables.net', title: 'Datatables.net', ab: 'DT'}
//         ]
//     }, {
//         path: '/maps',
//         title: 'Maps',
//         type: 'sub',
//         collapse: 'maps',
//         icontype: 'nc-icon nc-pin-3',
//         children: [
//             {path: 'google', title: 'Google Maps', ab: 'GM'},
//             {path: 'fullscreen', title: 'Full Screen Map', ab: 'FSM'},
//             {path: 'vector', title: 'Vector Map', ab: 'VM'}
//         ]
//     }, {
//         path: '/widgets',
//         title: 'Widgets',
//         type: 'link',
//         icontype: 'nc-icon nc-box'
//
//     }, {
//         path: '/charts',
//         title: 'Charts',
//         type: 'link',
//         icontype: 'nc-icon nc-chart-bar-32'
//
//     }, {
//         path: '/calendar',
//         title: 'Calendar',
//         type: 'link',
//         icontype: 'nc-icon nc-calendar-60'
//     }, {
//         path: '/pages',
//         title: 'pages',
//         collapse: 'pages',
//         type: 'sub',
//         icontype: 'nc-icon nc-book-bookmark',
//         children: [
//             {path: 'timeline', title: 'Timeline Page', ab: 'T'},
//             {path: 'user', title: 'User Page', ab: 'UP'},
//             {path: 'login', title: 'Login Page', ab: 'LP'},
//             {path: 'register', title: 'Register Page', ab: 'RP'},
//             {path: 'lock', title: 'Lock Screen Page', ab: 'LSP'}
//         ]
//     }
// ];

declare const $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {

  public static Routes: RouteInfo[];
  currentRoutes: RouteInfo[];
  upperMenuRoutes: RouteInfo[];

  constructor(private authService: AuthService) {

  }

  public menuItems: RouteInfo[];

  isNotMobileMenu() {
    if (window.outerWidth > 991) {
      return false;
    }
    return true;
  }

  ngOnInit() {
    if (this.authService.isRole(RoleNames.Administrator)) {
      this.currentRoutes = ADMINROUTES.filter(p => true);
      this.upperMenuRoutes = ADMINUPPERROUTES.filter(p => true);
    }
    SidebarComponent.Routes = this.currentRoutes.filter(p => true);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  getProfileImage(): string {
    return this.authService.currentUser.image;
  }

  getProfileName(): string {
    return this.authService.currentUser.fullName;
  }

  getProfileRole(): string[] {
    return this.authService.currentUser.roles;
  }


}
