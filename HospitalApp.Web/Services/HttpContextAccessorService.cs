using HospitalApp.Persistence.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Web.Services
{
    public class HttpContextAccessorService : IHttpContextAccessorService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextAccessorService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserId()
        {
            if (_httpContextAccessor.HttpContext != null)
            {
                return _httpContextAccessor.HttpContext.User
                    .FindFirst(p => p.Type == new IdentityOptions().ClaimsIdentity.UserIdClaimType)?.Value?.Trim();
            }
            return "";
        }
    }
}