using System;

namespace HospitalApp.Common.Extensions
{
    public static class DateExtension
    {
        public static string ToShortDate(this DateTime dateTime)
        {
            return dateTime.ToShortDateString();
        }
        public static string ToGeneralShortDate(this DateTime dateTime)
        {
            return dateTime.ToString("u");
        }

        public static string ToDayAndDate(this DateTime dateTime)
        {
            return dateTime.ToString("ddd dd-MMM");
        }
        public static string ToGeneralShortDate(this DateTime? dateTime)
        {
            return dateTime.GetValueOrDefault().ToString("g");
        }

        public static string ToUtc(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToGeneralShortDate();
        }
        
    }
}