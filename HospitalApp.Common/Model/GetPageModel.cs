﻿namespace HospitalApp.Common.Model
{
    public class GetPageModel
    {
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }

        public virtual void SetDefaultValue()
        {
            if (string.IsNullOrEmpty(Search))
            {
                Search = "";
            }

            OrderByFilter();
        }

        public virtual void OrderByFilter()
        {
            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }
        }
    }
}