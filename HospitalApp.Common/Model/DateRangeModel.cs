using System;

namespace HospitalApp.Common.Model
{
    public class DateRangeModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}