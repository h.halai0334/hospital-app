namespace HospitalApp.Common.Model
{
    public class DayOfWeekModel
    {
        public string[] Days { get; set; }
    }
}