using System;

namespace HospitalApp.Common.Model
{
    public class TimeRangeModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    public class PriceRangeModel
    {
        public double Min { get; set; }
        public double Max { get; set; }
    }
}