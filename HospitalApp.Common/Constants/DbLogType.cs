namespace HospitalApp.Common.Constants
{
    public enum DbLogType
    {
        /// <summary>
        /// Create Row = 1
        /// </summary>
        Create = 1,
        /// <summary>
        /// Update Row = 2
        /// </summary>
        Update = 2,
        /// <summary>
        /// Delete Row = 3
        /// </summary>
        Delete = 3
    }

    public enum PaymentType
    {
        /// <summary>
        /// Pay with Points
        /// </summary>
        Points = 1,
        /// <summary>
        /// Pay with Stripe Card
        /// </summary>
        Card = 2,
    }
}