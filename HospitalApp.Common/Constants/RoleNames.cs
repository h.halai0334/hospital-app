namespace HospitalApp.Common.Constants
{
    public class RoleNames
    {
        public const string Admin = "Administrator";
        public const string Doctor = "Doctor";
        public const string CareProvider = "Care Provider";
        public const string DiagnosticProvider = "Diagnostic Provider";
    }
}