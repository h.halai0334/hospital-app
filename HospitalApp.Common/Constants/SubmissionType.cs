namespace HospitalApp.Common.Constants
{
    public enum SubmissionType
    {
        All = 1,
        Text,
        Video,
        Image,
        Documents
    }
}