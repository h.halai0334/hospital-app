namespace HospitalApp.Common.Constants
{

    public enum SettingDataType
    {
        Integer,
        Float,
        Double,
        String,
        Json
    }
}