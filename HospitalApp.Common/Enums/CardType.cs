namespace HospitalApp.Common.Enums
{
    public enum CardType
    {
        Visa = 1,
        MasterCard,
        AmericanExpress,
        Jcb,
        Discover,
        Diners
    }
}