using System.Collections.Generic;

namespace HospitalApp.Common.Response
{
    public class ResponseBadRequestViewModel
    {
        public string Message { get; set; }
        public List<string> Errors { get; set; }
    }

    public class UnAuthorizedBadRequestViewModel
    {
        public string Message { get; set; }
        public bool IsExpired { get; set; }
    }

    public class OperationCancelledRequestViewModel
    {
        public string Message { get; set; }
    }
}