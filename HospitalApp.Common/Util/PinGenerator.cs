using System;
using System.Linq;

namespace HospitalApp.Common.Util
{
    public class PinGenerator
    {
        public static string CreatePin()
        {
            return new Random(DateTime.UtcNow.Millisecond).Next(1000, 9999) + "";
        }
    }

    public class StringGenerator
    {
        const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string RandomString(int length)
        {
            var random = new Random((int)DateTime.UtcNow.Ticks);
            return new string(Enumerable.Repeat(Chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GuidString()
        {
            var guid = Guid.NewGuid().ToString().Replace("-", "");
            return guid;
        }
    }
}