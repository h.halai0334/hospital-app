﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.DiseaseSymptoms.Model;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Shared;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.DiseaseSymptoms.Queries.GetDiseaseSymptomByDiseaseId
{

    public class GetDiseaseSymptomByDiseaseIdRequestModel : GetPagedRequest<GetDiseaseSymptomByDiseaseIdResponseModel>
    {
        public int DiseaseId { get; set; }
        public bool IsGetParent { get; set; }
    }

    public class GetDiseaseSymptomByDiseaseIdRequestModelValidator : PageRequestValidator<GetDiseaseSymptomByDiseaseIdRequestModel>
    {
        public GetDiseaseSymptomByDiseaseIdRequestModelValidator()
        {
            RuleFor(p => p.DiseaseId).Required();
        }
    }

    public class GetDiseaseSymptomByDiseaseIdRequestHandler : IRequestHandler<GetDiseaseSymptomByDiseaseIdRequestModel, GetDiseaseSymptomByDiseaseIdResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetDiseaseSymptomByDiseaseIdRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetDiseaseSymptomByDiseaseIdResponseModel> Handle(GetDiseaseSymptomByDiseaseIdRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            Disease disease = null;
            if (request.IsGetParent)
            {
                disease = await _context.Diseases.GetByAsync(p => p.Id == request.DiseaseId, cancellationToken: cancellationToken);
                if (disease == null)
                {
                    throw new NotFoundException(nameof(Disease),request.DiseaseId);
                }
            }
            Expression<Func<DiseaseSymptom, bool>> query = p =>
                p.DiseaseId == request.DiseaseId &&
                (p.SymptomType.Name.Contains(request.Search) || p.Symptom.Name.Contains(request.Search));
            var list = await _context.DiseaseSymptoms.GetManyReadOnly(query, request)
                .Select(p => new SharedDiseaseSymptomResponse()
                {
                    Disease = p.Disease.Name,
                    Symptom = p.Symptom.Name,
                    DiseaseId = p.DiseaseId,
                    SymptomId = p.SymptomId,
                    SymptomType = p.SymptomType.Name,
                    SymptomTypeId = p.SymptomTypeId,
                    CreatedDate = p.CreatedDate.ToGeneralShortDate()
                }).ToListAsync(cancellationToken);
            var count = await _context.DiseaseSymptoms.CountAsync(query, cancellationToken: cancellationToken);
            return new GetDiseaseSymptomByDiseaseIdResponseModel()
            {
                Data = list,
                Count = count,
                Parent = disease
            };
        }
    }

    public class GetDiseaseSymptomByDiseaseIdResponseModel
    {
        public List<SharedDiseaseSymptomResponse> Data { get; set; }
        public int Count { get; set; }
        public Disease Parent { get; set; }
    }
}