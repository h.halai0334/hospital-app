﻿namespace HospitalApp.Application.DiseaseSymptoms.Model
{
    public class SharedDiseaseSymptomResponse
    {
        public int DiseaseId { get; set; }
        public int SymptomId { get; set; }
        public int SymptomTypeId { get; set; }
        public string Disease { get; set; }
        public string Symptom { get; set; }
        public string SymptomType { get; set; }
        public string CreatedDate { get; set; }
    }
}