﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.DiseaseSymptoms.Commands.DeleteDiseaseSymptom
{

    public class DeleteDiseaseSymptomRequestModel : IRequest<DeleteDiseaseSymptomResponseModel>
    {
        public int DiseaseId { get; set; }
        public int SymptomId { get; set; }
        public int SymptomTypeId { get; set; }
    }

    public class DeleteDiseaseSymptomRequestModelValidator : AbstractValidator<DeleteDiseaseSymptomRequestModel>
    {
        public DeleteDiseaseSymptomRequestModelValidator()
        {
            RuleFor(p => p.DiseaseId).Required();
            RuleFor(p => p.SymptomId).Required();
            RuleFor(p => p.SymptomTypeId).Required();
        }
    }

    public class DeleteDiseaseSymptomRequestHandler : IRequestHandler<DeleteDiseaseSymptomRequestModel, DeleteDiseaseSymptomResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteDiseaseSymptomRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DeleteDiseaseSymptomResponseModel> Handle(DeleteDiseaseSymptomRequestModel request, CancellationToken cancellationToken)
        {
            var diseaseSymptom = await _context.DiseaseSymptoms.GetByAsync(p =>
                p.DiseaseId == request.DiseaseId && p.SymptomId == request.SymptomId &&
                p.SymptomTypeId == request.SymptomTypeId, cancellationToken: cancellationToken);
            if (diseaseSymptom == null)
            {
                throw new NotFoundException(nameof(Symptom),request.SymptomId);
            }
            _context.Remove(diseaseSymptom);
            await _context.SaveChangesAsync(cancellationToken);
            return new DeleteDiseaseSymptomResponseModel();
        }
    }

    public class DeleteDiseaseSymptomResponseModel
    {
    }
}