﻿using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.DiseaseSymptoms.Model;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace HospitalApp.Application.DiseaseSymptoms.Commands.CreateDiseaseSymptom
{

    public class CreateDiseaseSymptomRequestModel : IRequest<CreateDiseaseSymptomResponseModel>
    {
        public int DiseaseId { get; set; }
        public int SymptomId { get; set; }
        public int SymptomTypeId { get; set; }
    }

    public class CreateDiseaseSymptomRequestModelValidator : AbstractValidator<CreateDiseaseSymptomRequestModel>
    {
        public CreateDiseaseSymptomRequestModelValidator()
        {
            RuleFor(p => p.DiseaseId).Required();
            RuleFor(p => p.SymptomTypeId).Required();
            RuleFor(p => p.SymptomId).Required();
        }
    }

    public class CreateDiseaseSymptomRequestHandler : IRequestHandler<CreateDiseaseSymptomRequestModel, CreateDiseaseSymptomResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateDiseaseSymptomRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CreateDiseaseSymptomResponseModel> Handle(CreateDiseaseSymptomRequestModel request, CancellationToken cancellationToken)
        {
            var isExist = await _context.DiseaseSymptoms.AnyAsync(p =>
                p.DiseaseId == request.DiseaseId && p.SymptomId == request.SymptomTypeId &&
                p.SymptomTypeId == request.SymptomTypeId, cancellationToken: cancellationToken);
            if (isExist)
            {
                throw new AlreadyExistsException(nameof(Symptom));
            }
            var disease = await _context.Diseases.GetByAsync(p => p.Id == request.DiseaseId, cancellationToken: cancellationToken);
            if (disease == null)
            {
                throw new NotFoundException(nameof(Disease),request.DiseaseId);
            }

            var symptom = await _context.Symptoms.GetByAsync(
                p => p.Id == request.SymptomId && p.SymptomTypeId == request.SymptomTypeId,
                p => p.Include(pr => pr.SymptomType), cancellationToken: cancellationToken);
            if (symptom == null)
            {
                throw new NotFoundException(nameof(Symptom),request.SymptomId);
            }
            var diseaseSymptom = new DiseaseSymptom()
            {
                DiseaseId = request.DiseaseId,
                SymptomId = request.SymptomId,
                SymptomTypeId = request.SymptomTypeId
            };
            _context.DiseaseSymptoms.Add(diseaseSymptom);
            await _context.SaveChangesAsync(cancellationToken);
            return new CreateDiseaseSymptomResponseModel()
            {
                Disease = disease.Name,
                Symptom = symptom.Name,
                DiseaseId = disease.Id,
                SymptomId = symptom.Id,
                SymptomType = symptom.SymptomType.Name,
                SymptomTypeId = symptom.SymptomTypeId,
                CreatedDate = diseaseSymptom.CreatedDate.ToGeneralShortDate()
            };
        }
    }

    public class CreateDiseaseSymptomResponseModel : SharedDiseaseSymptomResponse
    {
    }
}