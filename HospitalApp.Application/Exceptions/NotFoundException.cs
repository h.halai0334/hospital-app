using System;

namespace HospitalApp.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
            
        }
        public NotFoundException(string name, object key)
            : base($"\"{name}\" was not found ({key})")
        {
        }
    }
}