using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.AppSettings.Model;
using HospitalApp.Common.Response;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.AppSettings.Commands.SaveSetting
{

    public class SaveSettingRequestModel : IRequest<ResponseViewModel>
    {
        public List<SettingModel> Settings { get; set; }
    }

    public class SaveSettingRequestModelValidator : AbstractValidator<SaveSettingRequestModel>
    {
        public SaveSettingRequestModelValidator()
        {
            RuleForEach(p => p.Settings).SetValidator(new SettingModelValidator());
        }
    }

    public class SaveSettingRequestHandler : IRequestHandler<SaveSettingRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public SaveSettingRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(SaveSettingRequestModel request, CancellationToken cancellationToken)
        {
            var keys = request.Settings.Select(p => p.Key).ToList();
            var list = await _context.AppSettings.Where(p => keys.Contains(p.Key))
                .ToListAsync(cancellationToken);
            foreach (var appSetting in list)
            {
                var newSetting = request.Settings.FirstOrDefault(p => p.Key == appSetting.Key);
                if (newSetting != null)
                {
                    appSetting.Value = newSetting.Value;
                }
            }
            _context.UpdateRange(list);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel(request.Settings);
        }
    }
}