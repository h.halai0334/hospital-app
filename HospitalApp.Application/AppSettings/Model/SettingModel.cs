using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.AppSettings.Model
{
    public class SettingModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class SettingModelValidator : AbstractValidator<SettingModel>
    {
        public SettingModelValidator()
        {
            RuleFor(p => p.Key).Required();
            RuleFor(p => p.Value).Required();
        }
    }
}