using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.AppSettings.Model;
using HospitalApp.Application.Extensions;
using HospitalApp.Common.Response;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.AppSettings.Queries.GetSettings
{
    public class GetSettingsRequestModel : IRequest<ResponseViewModel>
    {
        public string[] Keys { get; set; }
    }

    public class GetSettingsRequestModelValidator : AbstractValidator<GetSettingsRequestModel>
    {
        public GetSettingsRequestModelValidator()
        {
            RuleFor(p => p.Keys).Required();
        }
    }

    public class GetSettingsRequestHandler : IRequestHandler<GetSettingsRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetSettingsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetSettingsRequestModel request,
            CancellationToken cancellationToken)
        {
            var list = await _context.AppSettings.GetAllReadOnly(p => request.Keys.Contains(p.Key)).Select(p =>
                new SettingModel()
                {
                    Key = p.Key,
                    Value = p.Value
                }).ToListAsync(cancellationToken);
            return new ResponseViewModel(list);
        }
    }
}