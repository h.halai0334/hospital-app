﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Doctors.Model;
using HospitalApp.Application.Shared;
using HospitalApp.Common.Constants;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Doctors.Queries.GetDoctors
{

    public class GetDoctorsRequestModel : GetPagedRequest<GetDoctorsResponseModel>
    {

    }

    public class GetDoctorsRequestModelValidator : PageRequestValidator<GetDoctorsRequestModel>
    {
        public GetDoctorsRequestModelValidator()
        {

        }
    }

    public class GetDoctorsRequestHandler : IRequestHandler<GetDoctorsRequestModel, GetDoctorsResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetDoctorsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetDoctorsResponseModel> Handle(GetDoctorsRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            Expression<Func<User, bool>> query = p =>
                (p.FullName.Contains(request.Search) || p.Email.Contains(request.Search))
                && p.UserRoles.Any(pr => pr.Role.Name == RoleNames.Doctor);
            var list = await _context.Users.GetManyReadOnly(query, request)
                .Select(p => new SharedDoctorResponse()
                {
                    Email = p.Email,
                    IsEnabled = p.IsEnabled,
                    Id = p.Id,
                    Image = p.Image,
                    Name = p.FullName,
                    CreatedDate = p.CreatedDate.ToGeneralShortDate()
                }).ToListAsync(cancellationToken);
            var count = await _context.Users.CountAsync(query, cancellationToken: cancellationToken);
            return new GetDoctorsResponseModel()
            {
                Count = count,
                Data = list
            };
        }
    }

    public class GetDoctorsResponseModel
    {
        public List<SharedDoctorResponse> Data { get; set; }
        public int Count { get; set; }
    }
}