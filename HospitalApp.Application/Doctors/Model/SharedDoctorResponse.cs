﻿namespace HospitalApp.Application.Doctors.Model
{
    public class SharedDoctorResponse
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public bool IsEnabled { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public string CreatedDate { get; set; }
    }
}