﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Doctors.Model;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Constants;
using HospitalApp.Common.Extensions;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Doctors.Commands.UpdateDoctor
{

    public class UpdateDoctorRequestModel : IRequest<UpdateDoctorResponseModel>
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

    }

    public class UpdateDoctorRequestModelValidator : AbstractValidator<UpdateDoctorRequestModel>
    {
        public UpdateDoctorRequestModelValidator()
        {
            RuleFor(p => p.Email).EmailAddress();
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.Image).Required();
            RuleFor(p => p.Id).Required();

        }
    }

    public class UpdateDoctorRequestHandler : IRequestHandler<UpdateDoctorRequestModel, UpdateDoctorResponseModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public UpdateDoctorRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<UpdateDoctorResponseModel> Handle(UpdateDoctorRequestModel request, CancellationToken cancellationToken)
        {
            request.Email = request.Email.ToLower();
            var doctor = await _context.Users.FirstOrDefaultAsync(p =>
                p.Id == request.Id && p.UserRoles.Any(pr => pr.Role.Name == RoleNames.Doctor), cancellationToken: cancellationToken);
            if (doctor == null)
            {
                throw new NotFoundException(RoleNames.Doctor,request.Id);
            }

            if (doctor.Email != request.Email)
            {
                var isAlreadyExist = await _context.Users.AnyAsync(p => p.Email == request.Email, cancellationToken: cancellationToken);
                if (isAlreadyExist)
                {
                    throw new AlreadyExistsException("User","Email",request.Email);
                }

                doctor.Email = request.Email;
                doctor.UserName = request.Email;
                doctor.NormalizedEmail = request.Email.ToUpper();
                doctor.NormalizedUserName = request.Email.ToUpper();
            }
            doctor.FullName = request.Name;
            if (doctor.Image != request.Image)
            {
                var previousImage = doctor.Image;
                doctor.Image = _imageService.SaveImage(request.Image);
                if (previousImage != Constant.DefaultImageUrl)
                {
                    _imageService.DeleteImage(previousImage);
                }
            }
            _context.Update(doctor);
            await _context.SaveChangesAsync(cancellationToken);
            return new UpdateDoctorResponseModel()
            {
                Id = doctor.Id,
                Email = request.Email,
                Image = doctor.Image,
                IsEnabled = doctor.IsEnabled,
                Name = doctor.FullName,
                CreatedDate = doctor.CreatedDate.ToGeneralShortDate()
            };
        }
    }

    public class UpdateDoctorResponseModel : SharedDoctorResponse
    {
            
    }
}