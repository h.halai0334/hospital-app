﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Common.Constants;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Doctors.Commands.BlockDoctor
{

    public class BlockDoctorRequestModel : IRequest<BlockDoctorResponseModel>
    {
        public string Id { get; set; }
    }

    public class BlockDoctorRequestModelValidator : AbstractValidator<BlockDoctorRequestModel>
    {
        public BlockDoctorRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class BlockDoctorRequestHandler : IRequestHandler<BlockDoctorRequestModel, BlockDoctorResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public BlockDoctorRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<BlockDoctorResponseModel> Handle(BlockDoctorRequestModel request, CancellationToken cancellationToken)
        {
            var doctor = await _context.Users.GetByAsync(p =>
                p.Id == request.Id && p.UserRoles.Any(pr => pr.Role.Name == RoleNames.Doctor),
                cancellationToken: cancellationToken);
            if (doctor == null)
            {
                throw new NotFoundException(RoleNames.Doctor,request.Id);
            }

            if (doctor.IsEnabled)
            {
                doctor.IsEnabled = false;
                _context.Update(doctor);
                await _context.SaveChangesAsync(cancellationToken);
            }
            return new BlockDoctorResponseModel();
        }
    }

    public class BlockDoctorResponseModel
    {
    }
}