﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Doctors.Model;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Constants;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Doctors.Commands.CreateDoctor
{

    public class CreateDoctorRequestModel : IRequest<CreateDoctorResponseModel>
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
    }

    public class CreateDoctorRequestModelValidator : AbstractValidator<CreateDoctorRequestModel>
    {
        public CreateDoctorRequestModelValidator()
        {
            RuleFor(p => p.Email).EmailAddress();
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.Image).Required();
            RuleFor(p => p.Password).Password();
        }
    }

    public class CreateDoctorRequestHandler : IRequestHandler<CreateDoctorRequestModel, CreateDoctorResponseModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public CreateDoctorRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<CreateDoctorResponseModel> Handle(CreateDoctorRequestModel request, CancellationToken cancellationToken)
        {
            request.Email = request.Email.ToLower();
            var isAlreadyExist = await _context.Users.AnyAsync(p => p.Email == request.Email, cancellationToken: cancellationToken);
            if (isAlreadyExist)
            {
                throw new AlreadyExistsException("User","Email",request.Email);
            }
            var doctorRole = await _context.Roles.FirstOrDefaultAsync(p => p.Name == RoleNames.Doctor, cancellationToken: cancellationToken);
            if (doctorRole == null)
            {
                throw new NotFoundException("Role",RoleNames.Doctor);
            }


            var doctor = new User()
            {
                Email = request.Email,
                NormalizedEmail = request.Email.ToUpper(),
                FullName = request.Name,
                EmailConfirmed = true,
                IsEnabled = true,
                NormalizedUserName = request.Email.ToUpper(),
                UserName = request.Email,
                Image = Constant.DefaultImageUrl,
                PhoneNumber = "",
                UserRoles = new List<UserRole>()
                {
                    new UserRole()
                    {
                        RoleId = doctorRole.Id,
                    }
                },
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };
            if (request.Image != Constant.DefaultImageUrl)
            {
                doctor.Image = _imageService.SaveImage(request.Image);
            }
            var passwordHasher = new PasswordHasher<User>();
            doctor.PasswordHash = passwordHasher.HashPassword(doctor, request.Password);
            _context.Users.Add(doctor);
            await _context.SaveChangesAsync(cancellationToken);
            return new CreateDoctorResponseModel()
            {
                Id = doctor.Id,
                Email = request.Email,
                Image = doctor.Image,
                IsEnabled = doctor.IsEnabled,
                Name = doctor.FullName,
                CreatedDate = doctor.CreatedDate.ToGeneralShortDate()
            };
        }
    }

    public class CreateDoctorResponseModel : SharedDoctorResponse
    {
    }
}