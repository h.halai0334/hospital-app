﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Common.Constants;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Doctors.Commands.UnBlockDoctor
{

    public class UnBlockDoctorRequestModel : IRequest<UnBlockDoctorResponseModel>
    {
        public string Id { get; set; }
    }

    public class UnBlockDoctorRequestModelValidator : AbstractValidator<UnBlockDoctorRequestModel>
    {
        public UnBlockDoctorRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class UnBlockDoctorRequestHandler : IRequestHandler<UnBlockDoctorRequestModel, UnBlockDoctorResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public UnBlockDoctorRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UnBlockDoctorResponseModel> Handle(UnBlockDoctorRequestModel request, CancellationToken cancellationToken)
        {
            var doctor = await _context.Users.GetByAsync(p =>
                    p.Id == request.Id && p.UserRoles.Any(pr => pr.Role.Name == RoleNames.Doctor),
                cancellationToken: cancellationToken);
            if (doctor == null)
            {
                throw new NotFoundException(RoleNames.Doctor,request.Id);
            }

            if (!doctor.IsEnabled)
            {
                doctor.IsEnabled = true;
                _context.Update(doctor);
                await _context.SaveChangesAsync(cancellationToken);
            }
            return new UnBlockDoctorResponseModel();
        }
    }

    public class UnBlockDoctorResponseModel
    {
    }
}