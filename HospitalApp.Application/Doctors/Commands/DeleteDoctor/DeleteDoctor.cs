﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Constants;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Doctors.Commands.DeleteDoctor
{

    public class DeleteDoctorRequestModel : IRequest<DeleteDoctorResponseModel>
    {
        public string Id { get; set; }
    }

    public class DeleteDoctorRequestModelValidator : AbstractValidator<DeleteDoctorRequestModel>
    {
        public DeleteDoctorRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class DeleteDoctorRequestHandler : IRequestHandler<DeleteDoctorRequestModel, DeleteDoctorResponseModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public DeleteDoctorRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<DeleteDoctorResponseModel> Handle(DeleteDoctorRequestModel request, CancellationToken cancellationToken)
        {
            var doctor = await _context.Users.FirstOrDefaultAsync(p =>
                p.Id == request.Id 
                && p.UserRoles.Any(pr => pr.Role.Name == RoleNames.Doctor)
                , cancellationToken: cancellationToken);
            if (doctor == null)
            {
                throw new NotFoundException(RoleNames.Doctor,request.Id);
            }
            _context.Remove(doctor);
            await _context.SaveChangesAsync(cancellationToken);
            if (doctor.Image != Constant.DefaultImageUrl)
            {
                _imageService.DeleteImage(doctor.Image);
            }
            return new DeleteDoctorResponseModel();
        }
    }

    public class DeleteDoctorResponseModel
    {
    }
}