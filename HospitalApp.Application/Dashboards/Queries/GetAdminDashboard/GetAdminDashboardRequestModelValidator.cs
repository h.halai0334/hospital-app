using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Dashboards.Queries.GetAdminDashboard
{
    public class GetAdminDashboardRequestModelValidator : AbstractValidator<GetAdminDashboardRequestModel>
    {
        public GetAdminDashboardRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}