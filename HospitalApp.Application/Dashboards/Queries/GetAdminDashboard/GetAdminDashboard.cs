using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Common.Response;
using HospitalApp.Persistence.Context;
using MediatR;

namespace HospitalApp.Application.Dashboards.Queries.GetAdminDashboard
{
    public class GetAdminDashboardRequestHandler : IRequestHandler<GetAdminDashboardRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetAdminDashboardRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetAdminDashboardRequestModel request,
            CancellationToken cancellationToken)
        {
            return new ResponseViewModel();
            // var customerId = _context.Roles.GetBy(p => p.Name == RoleNames.Customer)?.Id;
            // var totalCustomers = await _context.Users.CountAsync(p => p.UserRoles.Any(pr => pr.RoleId == customerId),
            //     cancellationToken: cancellationToken);
            // List<string> weeks = new List<string>();
            // for (int i = 0; i < 7; i++)
            // {
            //     weeks.Add(DateTime.Now.AddDays(-i).ToDayAndDate());
            // }
            //
            // var dailySales = await _context.UserTransactions.Where(p =>
            //         p.CreatedDate >= DateTime.Today.AddDays(-7) &&
            //         p.CreatedDate <= DateTime.Today.AddDays(1).AddMinutes(-1))
            //     .GroupBy(p => p.CreatedDate.Date)
            //     .Select(p => new {Date = p.Key.ToDayAndDate(), Count = p.Sum(pr => pr.Amount)})
            //     .ToListAsync(cancellationToken);
            //
            // var dailyRegistration = await _context.Users.Where(p =>
            //         p.CreatedDate >= DateTime.Today.AddDays(-7) &&
            //         p.CreatedDate <= DateTime.Today.AddDays(1).AddMinutes(-1))
            //     .GroupBy(p => p.CreatedDate.Date)
            //     .Select(p => new {Date = p.Key.ToDayAndDate(), Count = p.Count()})
            //     .ToListAsync(cancellationToken);
            // for (int i = 0; i < weeks.Count; i++)
            // {
            //     var week = weeks[i];
            //     var item = dailySales.FirstOrDefault(p => p.Date == week);
            //     if (item == null)
            //     {
            //         dailySales.Insert(i, new {Date = week, Count = 0d});
            //     }
            //
            //     var regItem = dailyRegistration.FirstOrDefault(p => p.Date == week);
            //     if (regItem == null)
            //     {
            //         dailyRegistration.Insert(i, new {Date = week, Count = 0});
            //     }
            // }

            // var totalCashIns = await _context.CashInTracks.SumAsync(p => p.Amount, cancellationToken);
            // var totalCashOuts = await _context.CashOutTracks.SumAsync(p => p.Amount, cancellationToken);
            // List<BillInfoResponseModel> recentBills = _context.Bills.GetManyReadOnly(p => p.Status == RequestStatus.None.ToInt(), "CreatedDate",
            //     1, 5, true,
            //     p => p.Include(pr => pr.User)
            //         .Include(pr => pr.Transaction)
            //         .Include(pr => pr.Company))
            //     .Select(p => new BillInfoResponseModel
            //     {
            //         Customer = p.User.FullName,
            //         Amount = p.Transaction.Amount,
            //         Status = p.Status,
            //         Remarks = p.Remarks,
            //         BillId = p.Id,
            //         BillNo = p.BillNo,
            //         CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
            //         ReferenceNo = p.ReferenceNo,
            //         Company = p.Company.Name
            //     }).ToList();
            // List<TransactionWebInfoViewModel> recentTransaction = _context.Transactions.GetMany(
            //        p => p.LoadType > 0,"CreatedDate",
            //        1, 5, true, p => p.Include(pr => pr.ReceiverUser)
            //             .Include(pr => pr.SenderUser))
            //     .Select(p => new TransactionWebInfoViewModel
            //     {
            //         Amount = p.Amount,
            //         TransactionId = p.Id,
            //         LoadType = p.LoadType,
            //         ReceiverName = p.ReceiverUser == null ? "" : p.ReceiverUser.FullName + "",
            //         SenderName = p.SenderUser == null ? "" : p.SenderUser.FullName + "",
            //         Date = converter.ConvertToDateTime(p.CreatedDate)
            //     }).ToList();
            // List<AirTimeTopUpInfoResponseModel> recentTopUpRequests = _context.AirTopUpRequests.GetMany(
            //         p =>
            //             p.Status == RequestStatus.None.ToInt(),"CreatedDate",
            //         1, 5, true,
            //         p => p.Include(pr => pr.User).Include(pr => pr.Company))
            //     .Select(p => new AirTimeTopUpInfoResponseModel
            //     {
            //         Company = p.Company.Name,
            //         Name = p.User.FullName,
            //         Status = p.Status,
            //         MobileNo = p.MobileNo,
            //         RequestId = p.Id,
            //         CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
            //         Amount = p.Amount,
            //         Remarks = p.Remarks
            //     }).ToList();
            // List<WithdrawInfoResponseModel> recentWithdrawRequest = _context.WithdrawRequests.GetMany(
            //         p => p.Status == RequestStatus.None.ToInt(), "CreatedDate",
            //         1, 5, true,
            //         p => p.Include(pr => pr.User).Include(pr => pr.Bank))
            //     .Select(p => new WithdrawInfoResponseModel
            //     {
            //         Bank = p.Bank.Name,
            //         Name = p.User.FullName,
            //         Status = p.Status,
            //         AccountNo = p.AccountNo,
            //         RequestId = p.Id,
            //         CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
            //         Amount = p.Amount
            //     }).ToList();
            // var model = new AdminDashboardViewModel()
            // {
            //     TotalCustomer = totalCustomers,
            //     TotalRequests = totalRequest,
            //     Labels = weeks,
            //     TotalMoney = totalMoney,
            //     TotalTransactions = totalTransaction,
            //     DailyRegistration = dailyRegistration.Select(p => p.Count).ToList(),
            //     DailyTransactions = dailySales.Select(p => p.Count).ToList()
            // };
            // return new ResponseViewModel().CreateOk(model);
        }

        class AdminDashboardViewModel
        {
            public double TotalCustomer { get; set; }
            public double TotalMoney { get; set; }
            public double TotalTransactions { get; set; }
            public double TotalRequests { get; set; }

            // public double TotalCashIn { get; set; }
            // public double TotalCashOut { get; set; }
            public List<string> Labels { get; set; }
            public List<double> DailyTransactions { get; set; }

            public List<int> DailyRegistration { get; set; }
            // public List<TransactionWebInfoViewModel> RecentTransactions { get; set; }
            // public List<WithdrawInfoResponseModel> RecentWithdrawRequests { get; set; }
            // public List<AirTimeTopUpInfoResponseModel> RecentAirTimeTopUpRequests { get; set; }
            // public List<BillInfoResponseModel> RecentBills { get; set; }   
            //
        }

        private class TransactionWebInfoViewModel
        {
            public string TransactionId { get; set; }
            public int LoadType { get; set; }
            public string SenderName { get; set; }
            public string ReceiverName { get; set; }
            public double Amount { get; set; }
            public string Date { get; set; }
        }
    }
}