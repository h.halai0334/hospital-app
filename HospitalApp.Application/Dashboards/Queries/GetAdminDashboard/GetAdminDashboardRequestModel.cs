using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Dashboards.Queries.GetAdminDashboard
{

    public class GetAdminDashboardRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}