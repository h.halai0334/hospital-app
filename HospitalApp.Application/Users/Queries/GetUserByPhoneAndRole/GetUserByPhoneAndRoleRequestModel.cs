using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Queries.GetUserByPhoneAndRole
{

    public class GetUserByPhoneAndRoleRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
    }
}