using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Users.Queries.GetUserByPhoneAndRole
{

    public class GetUserByPhoneAndRoleRequestHandler : IRequestHandler<GetUserByPhoneAndRoleRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetUserByPhoneAndRoleRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetUserByPhoneAndRoleRequestModel request, CancellationToken cancellationToken)
        {
            var role = _context.Roles.GetBy(p => p.NormalizedName == request.Role.ToUpper());
            if (role == null)
            {
                throw new NotFoundException(nameof(Role),request.Role);
            }

            var user = await _context.Users.GetByAsync(p =>
                p.PhoneNumber == request.PhoneNumber && p.UserRoles.Any(pr => pr.RoleId == role.Id), cancellationToken: cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(role.Name,request.PhoneNumber);
            }
            return new ResponseViewModel().CreateOk(new UserResponseViewModel()
            {
                Image = user.Image,
                Name = user.FullName,
                Role = role.Name,
                PhoneNumber = user.PhoneNumber,
                Id = user.Id
            });
        }

        class UserResponseViewModel
        {
            public string Id { get; set; }
            public string Image { get; set; }
            public string Role { get; set; }
            public string PhoneNumber { get; set; }
            public string Name { get; set; }
        }
    }

}