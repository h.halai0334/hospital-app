using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Queries.GetUserByPhoneAndRole
{
    public class GetUserByPhoneAndRoleRequestModelValidator : AbstractValidator<GetUserByPhoneAndRoleRequestModel>
    {
        public GetUserByPhoneAndRoleRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Role).Required();
        }
    }
}