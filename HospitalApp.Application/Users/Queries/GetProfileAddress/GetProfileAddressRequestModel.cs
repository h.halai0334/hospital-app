using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Queries.GetProfileAddress
{

    public class GetProfileAddressRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}