using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Queries.GetProfileAddress
{
    public class GetProfileAddressRequestModelValidator : AbstractValidator<GetProfileAddressRequestModel>
    {
        public GetProfileAddressRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}