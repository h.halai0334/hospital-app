using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Users.Queries.GetProfileAddress
{

    public class GetProfileAddressRequestHandler : IRequestHandler<GetProfileAddressRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetProfileAddressRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetProfileAddressRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User),request.UserId);
            }
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(new AddressInfo()
            {
                Address = user.Address,
            }));
        }
        
        class AddressInfo
        {
            public string Address { get; set; }
        }
    }

}