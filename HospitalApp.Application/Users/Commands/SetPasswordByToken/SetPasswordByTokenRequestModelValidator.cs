using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.SetPasswordByToken
{
    public class SetPasswordByTokenRequestModelValidator : AbstractValidator<SetPasswordByTokenRequestModel>
    {
        public SetPasswordByTokenRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin(4);
            RuleFor(p => p.Token).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}