using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.UpdateProfile
{

    public class UpdateProfileRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
    }
}