using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.UpdateProfile
{
    public class UpdateProfileRequestModelValidator : AbstractValidator<UpdateProfileRequestModel>
    {
        public UpdateProfileRequestModelValidator()
        {
            RuleFor(p => p.FullName).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}