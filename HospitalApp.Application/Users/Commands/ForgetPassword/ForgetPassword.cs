using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Users.Commands.ForgetPassword
{

    public class ForgetPasswordRequestHandler : IRequestHandler<ForgetPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly ISmsService _smsService;
        public ForgetPasswordRequestHandler(ApplicationDbContext context, ISmsService smsService)
        {
            _context = context;
            _smsService = smsService;
        }

        public async Task<ResponseViewModel> Handle(ForgetPasswordRequestModel request,
            CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.PhoneNumber == request.MobileNumber);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.MobileNumber);
            }

            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}