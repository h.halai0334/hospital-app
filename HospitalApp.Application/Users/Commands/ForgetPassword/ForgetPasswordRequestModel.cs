using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.ForgetPassword
{

    public class ForgetPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string MobileNumber { get; set; }
    }
}