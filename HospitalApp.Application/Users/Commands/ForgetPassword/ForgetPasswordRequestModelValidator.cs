using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.ForgetPassword
{
    public class ForgetPasswordRequestModelValidator : AbstractValidator<ForgetPasswordRequestModel>
    {
        public ForgetPasswordRequestModelValidator()
        {
            RuleFor(p => p.MobileNumber).Phone();
        }
    }
}