using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.UpdateProfileAddress
{

    public class UpdateProfileAddressRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Address { get; set; }

    }
}