using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.ResetPassword
{

    public class ResetPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string MobileNumber { get; set; }
        public string Otp { get; set; }
    }
}