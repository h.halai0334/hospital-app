using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Application.Users.Commands.ResetPassword
{

    public class ResetPasswordRequestHandler : IRequestHandler<ResetPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public ResetPasswordRequestHandler(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<ResponseViewModel> Handle(ResetPasswordRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.PhoneNumber == request.MobileNumber);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.MobileNumber);
            }

            var token = _userManager.GeneratePasswordResetTokenAsync(user);
            _context.Update(user.PasswordHash);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOkCustom(new List<KeyValuePair<string, object>>() {
                new KeyValuePair<string, object>(nameof(token),token),
                new KeyValuePair<string, object>(nameof(user.Id),user.Id),
            });;
        }
    }
}