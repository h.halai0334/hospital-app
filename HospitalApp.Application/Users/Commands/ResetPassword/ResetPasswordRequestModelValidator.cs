using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.ResetPassword
{
    public class ResetPasswordRequestModelValidator : AbstractValidator<ResetPasswordRequestModel>
    {
        public ResetPasswordRequestModelValidator()
        {
            RuleFor(p => p.Otp).Pin();
            RuleFor(p => p.MobileNumber).Phone();
        }
    }
}