using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.UpdateImage
{

    public class UpdateImageRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Image { get; set; }
    }
}