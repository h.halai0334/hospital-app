using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.UpdateImage
{
    public class UpdateImageRequestModelValidator : AbstractValidator<UpdateImageRequestModel>
    {
        public UpdateImageRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Image).Required();
        }
    }
}