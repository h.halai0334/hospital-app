using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.UpdateFcmId
{
    public class UpdateFcmIdRequestModelValidator : AbstractValidator<UpdateFcmIdRequestModel>
    {
        public UpdateFcmIdRequestModelValidator()
        {
            RuleFor(p => p.FcmId).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}