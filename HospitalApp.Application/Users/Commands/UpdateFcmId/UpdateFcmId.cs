using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Users.Commands.UpdateFcmId
{

    public class UpdateFcmIdRequestHandler : IRequestHandler<UpdateFcmIdRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateFcmIdRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateFcmIdRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new  NotFoundException(nameof(User), request.UserId);
            }

            user.FcmId = request.FcmId;
            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}