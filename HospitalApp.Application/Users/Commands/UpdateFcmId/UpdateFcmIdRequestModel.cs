using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.UpdateFcmId
{

    public class UpdateFcmIdRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string FcmId { get; set; }

    }
}