using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation.Results;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Application.Users.Commands.ChangePassword
{

    public class ChangePasswordRequestHandler : IRequestHandler<ChangePasswordRequestModel, ResponseViewModel>
    {
        private readonly UserManager<User> _userManager;

        public ChangePasswordRequestHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        public async Task<ResponseViewModel> Handle(ChangePasswordRequestModel request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserId);
            }
            var result = await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
            if (!result.Succeeded)
            {
                throw new ValidationException(result.Errors.Select(p => new ValidationFailure("",p.Description)).ToList());
            }
            return new ResponseViewModel();
        }
    }

}