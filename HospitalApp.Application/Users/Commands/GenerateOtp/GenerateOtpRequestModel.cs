using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Users.Commands.GenerateOtp
{

    public class GenerateOtpRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}