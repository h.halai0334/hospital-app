using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Exceptions;
using HospitalApp.Common.Response;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Users.Commands.GenerateOtp
{

    public class GenerateOtpRequestHandler : IRequestHandler<GenerateOtpRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public GenerateOtpRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(GenerateOtpRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException("User",request.UserId);
            }

            // user.Otp = PinGenerator.CreatePin();
            // user.OtpExpiry = DateTime.UtcNow.AddMinutes(5);
            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            // await _mediator.Send(new SendSmsRequestModel
            // {
            //     Message = "Your All Money OTP: " + user.Otp,
            //     PhoneNumber = user.PhoneNumber
            // },cancellationToken);
            return new ResponseViewModel();
        }
    }

}