using FluentValidation;
using HospitalApp.Application.Extensions;

namespace HospitalApp.Application.Users.Commands.GenerateOtp
{
    public class GenerateOtpRequestModelValidator : AbstractValidator<GenerateOtpRequestModel>
    {
        public GenerateOtpRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}