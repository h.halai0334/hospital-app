﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Symptoms.Commands.DeleteSymptom
{
    public class DeleteSymptomRequestModel : IRequest<DeleteSymptomResponseModel>
    {
        public int Id { get; set; }
        public int SymptomTypeId { get; set; }
    }

    public class DeleteSymptomRequestModelValidator : AbstractValidator<DeleteSymptomRequestModel>
    {
        public DeleteSymptomRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
            RuleFor(p => p.SymptomTypeId).Required();
        }
    }

    public class DeleteSymptomRequestHandler : IRequestHandler<DeleteSymptomRequestModel, DeleteSymptomResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteSymptomRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DeleteSymptomResponseModel> Handle(DeleteSymptomRequestModel request,
            CancellationToken cancellationToken)
        {
            var symptom =
                await _context.Symptoms.GetByAsync(p => p.Id == request.Id
                                                        && !p.DiseaseSymptomTypes.Any()
                                                        && p.SymptomTypeId == request.SymptomTypeId,
                    cancellationToken: cancellationToken);
            if (symptom == null)
            {
                throw new CannotDeleteException(nameof(Symptom));
            }
            _context.Remove(symptom);
            await _context.SaveChangesAsync(cancellationToken);
            return new DeleteSymptomResponseModel();
        }
    }

    public class DeleteSymptomResponseModel
    {
    }
}