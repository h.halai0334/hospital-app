﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Symptoms.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Symptoms.Commands.UpdateSymptom
{

    public class UpdateSymptomRequestModel : IRequest<UpdateSymptomResponseModel>
    {
        public int Id { get; set; }
        public int SymptomTypeId { get; set; }
        public string Name { get; set; }
    }

    public class UpdateSymptomRequestModelValidator : AbstractValidator<UpdateSymptomRequestModel>
    {
        public UpdateSymptomRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
            RuleFor(p => p.SymptomTypeId).Required();
            RuleFor(p => p.Name).Required();
        }
    }

    public class UpdateSymptomRequestHandler : IRequestHandler<UpdateSymptomRequestModel, UpdateSymptomResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateSymptomRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UpdateSymptomResponseModel> Handle(UpdateSymptomRequestModel request, CancellationToken cancellationToken)
        {
            var symptom =
                await _context.Symptoms.GetByAsync(p => p.Id == request.Id
                                                        && p.SymptomTypeId == request.SymptomTypeId,
                    cancellationToken: cancellationToken);
            if (symptom == null)
            {
                throw new NotFoundException(nameof(Symptom),request.Id);
            }

            symptom.Name = request.Name;
            _context.Update(symptom);
            await _context.SaveChangesAsync(cancellationToken);
            return new UpdateSymptomResponseModel()
            {
                Id = symptom.Id,
                Name = symptom.Name,
                CreatedDate = symptom.CreatedDate.ToGeneralShortDate(),
                SymptomTypeId = symptom.SymptomTypeId
            };
            
        }
    }

    public class UpdateSymptomResponseModel : SharedSymptomResponse
    {
    }
}