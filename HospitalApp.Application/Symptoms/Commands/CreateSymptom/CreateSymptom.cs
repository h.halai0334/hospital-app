﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Symptoms.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using MediatR;

namespace HospitalApp.Application.Symptoms.Commands.CreateSymptom
{

    public class CreateSymptomRequestModel : IRequest<CreateSymptomResponseModel>
    {
        public int SymptomTypeId { get; set; }
        public string Name { get; set; }
        
    }

    public class CreateSymptomRequestModelValidator : AbstractValidator<CreateSymptomRequestModel>
    {
        public CreateSymptomRequestModelValidator()
        {
            RuleFor(p => p.SymptomTypeId).Required();
            RuleFor(p => p.Name).Required();
        }
    }

    public class CreateSymptomRequestHandler : IRequestHandler<CreateSymptomRequestModel, CreateSymptomResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateSymptomRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CreateSymptomResponseModel> Handle(CreateSymptomRequestModel request, CancellationToken cancellationToken)
        {
            var symptom = new Symptom()
            {
                Name = request.Name,
                SymptomTypeId = request.SymptomTypeId
            };
            _context.Symptoms.Add(symptom);
            await _context.SaveChangesAsync(cancellationToken);
            return new CreateSymptomResponseModel()
            {
                Id = symptom.Id,
                Name = symptom.Name,
                CreatedDate = symptom.CreatedDate.ToGeneralShortDate(),
                SymptomTypeId = symptom.SymptomTypeId
            };
        }
    }

    public class CreateSymptomResponseModel : SharedSymptomResponse
    {
        
    }
}