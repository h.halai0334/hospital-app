﻿namespace HospitalApp.Application.Symptoms.Model
{
    public class SharedSymptomResponse
    {
        public int SymptomTypeId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedDate { get; set; }
    }
}