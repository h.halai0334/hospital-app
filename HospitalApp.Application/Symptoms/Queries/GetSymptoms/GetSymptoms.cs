﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Shared;
using HospitalApp.Application.Symptoms.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Symptoms.Queries.GetSymptoms
{

    public class GetSymptomsRequestModel : GetPagedRequest<GetSymptomsResponseModel>
    {
        public int SymptomTypeId { get; set; }
        public bool IsGetParent { get; set; }
    }

    public class GetSymptomsRequestModelValidator : PageRequestValidator<GetSymptomsRequestModel>
    {
        public GetSymptomsRequestModelValidator()
        {
            RuleFor(p => p.SymptomTypeId).Required();
        }
    }

    public class GetSymptomsRequestHandler : IRequestHandler<GetSymptomsRequestModel, GetSymptomsResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetSymptomsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetSymptomsResponseModel> Handle(GetSymptomsRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            SymptomType type = null;
            if (request.IsGetParent)
            {
                type = _context.SymptomTypes.FirstOrDefault(p => p.Id == request.SymptomTypeId);
                if (type == null)
                {
                    throw new NotFoundException(nameof(SymptomType),request.SymptomTypeId);
                }
            }
            Expression<Func<Symptom, bool>> query = p => p.SymptomTypeId == request.SymptomTypeId && p.Name.Contains(request.Search);
            var list = await _context.Symptoms.GetManyReadOnly(query, request)
                .Select(p => new SharedSymptomResponse()
                {
                    Id = p.Id,
                    Name = p.Name,
                    CreatedDate = p.CreatedDate.ToGeneralShortDate(),
                    SymptomTypeId = p.SymptomTypeId
                }).ToListAsync(cancellationToken);
            var count = await _context.Symptoms.CountAsync(query, cancellationToken: cancellationToken);
            return new GetSymptomsResponseModel()
            {
                Data = list,
                Count = count,
                Parent = type,
            };
        }
    }

    public class GetSymptomsResponseModel
    {
        public List<SharedSymptomResponse> Data { get; set; }
        public int Count { get; set; }
        public SymptomType Parent { get; set; }
    }
}