﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Extensions;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Symptoms.Queries.GetSymptomsDropDown
{

    public class GetSymptomsDropDownRequestModel : IRequest<List<GetSymptomsDropDownResponseModel>>
    {
        public int SymptomTypeId { get; set; }
    }

    public class GetSymptomsDropDownRequestModelValidator : AbstractValidator<GetSymptomsDropDownRequestModel>
    {
        public GetSymptomsDropDownRequestModelValidator()
        {
            RuleFor(p => p.SymptomTypeId).Required();
        }
    }

    public class GetSymptomsDropDownRequestHandler : IRequestHandler<GetSymptomsDropDownRequestModel, List<GetSymptomsDropDownResponseModel>>
    {
        private readonly ApplicationDbContext _context;

        public GetSymptomsDropDownRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<GetSymptomsDropDownResponseModel>> Handle(GetSymptomsDropDownRequestModel request, CancellationToken cancellationToken)
        {
            return await _context.Symptoms.Where(p => p.SymptomTypeId == request.SymptomTypeId)
                .Select(p => new GetSymptomsDropDownResponseModel()
                {
                    Id = p.Id,
                    Name = p.Name,
                }).ToListAsync(cancellationToken);
        }
    }

    public class GetSymptomsDropDownResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}