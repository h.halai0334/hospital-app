using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Sms.Commands.SendSms
{

    public class SendSmsRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}