using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Sms.Commands.SendSms
{

    public class SendSmsRequestHandler : IRequestHandler<SendSmsRequestModel, ResponseViewModel>
    {

        private readonly ISmsService _service;

        public SendSmsRequestHandler(ISmsService service)
        {
            _service = service;
        }

        public async Task<ResponseViewModel> Handle(SendSmsRequestModel request, CancellationToken cancellationToken)
        {
            await _service.SendMessage(request.PhoneNumber, request.Message);
            return new ResponseViewModel();
        }
    }

}