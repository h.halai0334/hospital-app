﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Diseases.Commands.DeleteDisease
{

    public class DeleteDiseaseRequestModel : IRequest<DeleteDiseaseResponseModel>
    {
        public int Id { get; set; }
    }

    public class DeleteDiseaseRequestModelValidator : AbstractValidator<DeleteDiseaseRequestModel>
    {
        public DeleteDiseaseRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class DeleteDiseaseRequestHandler : IRequestHandler<DeleteDiseaseRequestModel, DeleteDiseaseResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteDiseaseRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DeleteDiseaseResponseModel> Handle(DeleteDiseaseRequestModel request, CancellationToken cancellationToken)
        {
            var disease = await _context.Diseases.GetByAsync(p => p.Id == request.Id && !p.DiseaseSymptomTypes.Any(), cancellationToken: cancellationToken);
            if (disease == null)
            {
                throw new CannotDeleteException(nameof(Disease));
            }
            _context.Remove(disease);
            await _context.SaveChangesAsync(cancellationToken);
            return new DeleteDiseaseResponseModel();
        }
    }

    public class DeleteDiseaseResponseModel
    {
    }
}