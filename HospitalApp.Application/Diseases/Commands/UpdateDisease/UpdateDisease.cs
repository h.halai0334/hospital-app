﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Diseases.Model;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Symptoms.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.Diseases.Commands.UpdateDisease
{

    public class UpdateDiseaseRequestModel : IRequest<UpdateDiseaseResponseModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class UpdateDiseaseRequestModelValidator : AbstractValidator<UpdateDiseaseRequestModel>
    {
        public UpdateDiseaseRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
            RuleFor(p => p.Name).Required();
        }
    }

    public class UpdateDiseaseRequestHandler : IRequestHandler<UpdateDiseaseRequestModel, UpdateDiseaseResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateDiseaseRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UpdateDiseaseResponseModel> Handle(UpdateDiseaseRequestModel request, CancellationToken cancellationToken)
        {
            var disease = await _context.Diseases.GetByAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (disease == null)
            {
                throw new NotFoundException(nameof(Disease),request.Id);
            }

            disease.Name = request.Name;
            _context.Update(disease);
            await _context.SaveChangesAsync(cancellationToken);
            return new UpdateDiseaseResponseModel()
            {
                Id = disease.Id,
                Name = disease.Name,
                CreatedDate = disease.CreatedDate.ToGeneralShortDate(),
                SymptomCount = 0
            };
        }
    }

    public class UpdateDiseaseResponseModel : SharedDiseaseResponse
    {
    }
}