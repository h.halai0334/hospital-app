﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Diseases.Model;
using HospitalApp.Application.Extensions;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using MediatR;

namespace HospitalApp.Application.Diseases.Commands.CreateDisease
{

    public class CreateDiseaseRequestModel : IRequest<CreateDiseaseResponseModel>
    {
        public string Name { get; set; }
    }

    public class CreateDiseaseRequestModelValidator : AbstractValidator<CreateDiseaseRequestModel>
    {
        public CreateDiseaseRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
        }
    }

    public class CreateDiseaseRequestHandler : IRequestHandler<CreateDiseaseRequestModel, CreateDiseaseResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateDiseaseRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CreateDiseaseResponseModel> Handle(CreateDiseaseRequestModel request, CancellationToken cancellationToken)
        {
            var disease = new Disease()
            {
                Name = request.Name
            };
            _context.Diseases.Add(disease);
            await _context.SaveChangesAsync(cancellationToken);
            return new CreateDiseaseResponseModel()
            {
                Id = disease.Id,
                Name = disease.Name,
                CreatedDate = disease.CreatedDate.ToGeneralShortDate(),
                SymptomCount = 0
            };
        }
    }

    public class CreateDiseaseResponseModel : SharedDiseaseResponse
    {
    }
}