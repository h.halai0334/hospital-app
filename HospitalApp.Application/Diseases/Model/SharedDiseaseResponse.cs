﻿namespace HospitalApp.Application.Diseases.Model
{
    public class SharedDiseaseResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SymptomCount { get; set; }
        public string CreatedDate { get; set; }
    }
}