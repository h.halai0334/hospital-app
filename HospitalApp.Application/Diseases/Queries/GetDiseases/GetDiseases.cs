﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Diseases.Model;
using HospitalApp.Application.Shared;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.Diseases.Queries.GetDiseases
{

    public class GetDiseasesRequestModel : GetPagedRequest<GetDiseasesResponseModel>
    {
    }

    public class GetDiseasesRequestModelValidator : PageRequestValidator<GetDiseasesRequestModel>
    {
        public GetDiseasesRequestModelValidator()
        {

        }
    }

    public class GetDiseasesRequestHandler : IRequestHandler<GetDiseasesRequestModel, GetDiseasesResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetDiseasesRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetDiseasesResponseModel> Handle(GetDiseasesRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            Expression<Func<Disease, bool>> query = p => p.Name.Contains(request.Search);
            var list = await _context.Diseases.GetManyReadOnly(query, request)
                .Select(p => new SharedDiseaseResponse()
                {
                    Id = p.Id,
                    Name = p.Name,
                    CreatedDate = p.CreatedDate.ToGeneralShortDate(),
                    SymptomCount = p.DiseaseSymptomTypes.Count()
                }).ToListAsync(cancellationToken);
            var count = await _context.Diseases.CountAsync(query, cancellationToken);
            return new GetDiseasesResponseModel()
            {
                Data = list,
                Count = count
            };
        }
    }

    public class GetDiseasesResponseModel
    {
        public List<SharedDiseaseResponse> Data { get; set; }
        public int Count { get; set; }
    }
}