﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Persistence.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.SymptomTypes.Queries.GetSymptomTypesDropDown
{

    public class GetSymptomTypesDropDownRequestModel : IRequest<List<GetSymptomTypesDropDownResponseModel>>
    {

    }

    public class GetSymptomTypesDropDownRequestModelValidator : AbstractValidator<GetSymptomTypesDropDownRequestModel>
    {
        public GetSymptomTypesDropDownRequestModelValidator()
        {

        }
    }

    public class GetSymptomTypesDropDownRequestHandler : IRequestHandler<GetSymptomTypesDropDownRequestModel, List<GetSymptomTypesDropDownResponseModel>>
    {
        private readonly ApplicationDbContext _context;

        public GetSymptomTypesDropDownRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<GetSymptomTypesDropDownResponseModel>> Handle(GetSymptomTypesDropDownRequestModel request, CancellationToken cancellationToken)
        {
            return await _context.SymptomTypes.Select(p => new GetSymptomTypesDropDownResponseModel()
            {
                Id = p.Id,
                Name = p.Name,
            }).ToListAsync(cancellationToken);
        }
    }

    public class GetSymptomTypesDropDownResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}