﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.Shared;
using HospitalApp.Application.SymptomTypes.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace HospitalApp.Application.SymptomTypes.Queries.GetSymptomTypes
{

    public class GetSymptomTypesRequestModel : GetPagedRequest<GetSymptomTypesResponseModel>
    {

    }

    public class GetSymptomTypesRequestModelValidator : PageRequestValidator<GetSymptomTypesRequestModel>
    {
        public GetSymptomTypesRequestModelValidator()
        {
        }
    }

    public class GetSymptomTypesRequestHandler : IRequestHandler<GetSymptomTypesRequestModel, GetSymptomTypesResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetSymptomTypesRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetSymptomTypesResponseModel> Handle(GetSymptomTypesRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            Expression<Func<SymptomType, bool>> query = p => p.Name.Contains(request.Search);
            var list = await _context.SymptomTypes.GetManyReadOnly(query, request)
                .Select(p => new SharedSymptomTypeResponse()
                {
                    Id = p.Id,
                    Name = p.Name,
                    CreatedDate = p.CreatedDate.ToGeneralShortDate(),
                    SymptomCount = p.Symptoms.Count()
                }).ToListAsync(cancellationToken);
            var count = await _context.SymptomTypes.CountAsync(query, cancellationToken: cancellationToken);
            return new GetSymptomTypesResponseModel()
            {
                Data = list,
                Count = count
            };
        }
    }

    public class GetSymptomTypesResponseModel
    {
        public List<SharedSymptomTypeResponse> Data { get; set; }
        public int Count { get; set; }
    }
}