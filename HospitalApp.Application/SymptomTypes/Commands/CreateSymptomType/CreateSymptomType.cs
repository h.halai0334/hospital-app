﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.SymptomTypes.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using MediatR;

namespace HospitalApp.Application.SymptomTypes.Commands.CreateSymptomType
{

    public class CreateSymptomTypeRequestModel : IRequest<CreateSymptomTypeResponseModel>
    {
        public string Name { get; set; }
    }

    public class CreateSymptomTypeRequestModelValidator : AbstractValidator<CreateSymptomTypeRequestModel>
    {
        public CreateSymptomTypeRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
        }
    }

    public class CreateSymptomTypeRequestHandler : IRequestHandler<CreateSymptomTypeRequestModel, CreateSymptomTypeResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateSymptomTypeRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CreateSymptomTypeResponseModel> Handle(CreateSymptomTypeRequestModel request, CancellationToken cancellationToken)
        {
            var symptomType = new SymptomType()
            {
                Name = request.Name
            };
            _context.SymptomTypes.Add(symptomType);
            await _context.SaveChangesAsync(cancellationToken);
            return new CreateSymptomTypeResponseModel()
            {
                Id = symptomType.Id,
                Name = symptomType.Name,
                CreatedDate = symptomType.CreatedDate.ToGeneralShortDate(),
                SymptomCount = 0
            };
        }
    }

    public class CreateSymptomTypeResponseModel : SharedSymptomTypeResponse
    {
        
    }
}