﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Application.SymptomTypes.Model;
using HospitalApp.Common.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.SymptomTypes.Commands.UpdateSymptomType
{

    public class UpdateSymptomTypeRequestModel : IRequest<UpdateSymptomTypeResponseModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class UpdateSymptomTypeRequestModelValidator : AbstractValidator<UpdateSymptomTypeRequestModel>
    {
        public UpdateSymptomTypeRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class UpdateSymptomTypeRequestHandler : IRequestHandler<UpdateSymptomTypeRequestModel, UpdateSymptomTypeResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateSymptomTypeRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UpdateSymptomTypeResponseModel> Handle(UpdateSymptomTypeRequestModel request, CancellationToken cancellationToken)
        {
            var symptomType = await _context.SymptomTypes.GetByAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (symptomType == null)
            {
                throw new NotFoundException(nameof(SymptomType),request.Id);
            }

            symptomType.Name = request.Name;
            _context.Update(symptomType);
            await _context.SaveChangesAsync(cancellationToken);
            return new UpdateSymptomTypeResponseModel()
            {
                Id = symptomType.Id,
                Name = symptomType.Name,
                CreatedDate = symptomType.CreatedDate.ToGeneralShortDate(),
                SymptomCount = 0
            };
        }
    }

    public class UpdateSymptomTypeResponseModel : SharedSymptomTypeResponse
    {
    }
}