﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HospitalApp.Application.Exceptions;
using HospitalApp.Application.Extensions;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using HospitalApp.Persistence.Extension;
using MediatR;

namespace HospitalApp.Application.SymptomTypes.Commands.DeleteSymptomType
{

    public class DeleteSymptomTypeRequestModel : IRequest<DeleteSymptomTypeResponseModel>
    {
        public int Id { get; set; }
    }

    public class DeleteSymptomTypeRequestModelValidator : AbstractValidator<DeleteSymptomTypeRequestModel>
    {
        public DeleteSymptomTypeRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class DeleteSymptomTypeRequestHandler : IRequestHandler<DeleteSymptomTypeRequestModel, DeleteSymptomTypeResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteSymptomTypeRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DeleteSymptomTypeResponseModel> Handle(DeleteSymptomTypeRequestModel request, CancellationToken cancellationToken)
        {
            var type = await _context.SymptomTypes.GetByAsync(p => p.Id == request.Id && !p.Symptoms.Any(), cancellationToken: cancellationToken);
            if (type == null)
            {
                throw new CannotDeleteException(nameof(SymptomType));
            }

            _context.Remove(type);
            await _context.SaveChangesAsync(cancellationToken);
            return new DeleteSymptomTypeResponseModel();
        }
    }

    public class DeleteSymptomTypeResponseModel
    {
    }
}