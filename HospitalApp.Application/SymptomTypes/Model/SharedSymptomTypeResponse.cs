﻿namespace HospitalApp.Application.SymptomTypes.Model
{
    public class SharedSymptomTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedDate { get; set; }
        public int SymptomCount { get; set; }
    }
}