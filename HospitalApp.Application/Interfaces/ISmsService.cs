using System.Threading.Tasks;

namespace HospitalApp.Application.Interfaces
{
    public interface ISmsService
    {
        Task<bool> SendMessage(string phoneNumber, string message);
    }
}