namespace HospitalApp.Application.Interfaces
{
    public interface IUrlShortenService
    {
        string UrlShorten(string longUrl);
    }
}