using System.Threading.Tasks;

namespace HospitalApp.Application.Interfaces
{
    public interface INotificationService
    {
        Task<bool> SendNotification(string fcmId, string title, string body);
    }
}