using System.Threading.Tasks;

namespace HospitalApp.Application.Interfaces
{
    public interface IAlertService
    {
        Task<bool> SendAlertToAdmin(string message);
    }
}