using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HospitalApp.Application.Interfaces;
using HospitalApp.Common.Response;
using HospitalApp.Domain.Entities;
using HospitalApp.Persistence.Context;
using MediatR;

namespace HospitalApp.Application.Notifications.Commands.SendNotification
{

    public class SendNotificationRequestHandler : IRequestHandler<SendNotificationRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly INotificationService _notificationService;
        public SendNotificationRequestHandler(ApplicationDbContext context, INotificationService notificationService)
        {
            _context = context;
            _notificationService = notificationService;
        }

        public async Task<ResponseViewModel> Handle(SendNotificationRequestModel request, CancellationToken cancellationToken)
        {
            var fcmId = _context.Users.Where(p => p.Id == request.UserId).Select(p => p.FcmId).FirstOrDefault();
            if (fcmId != null)
            {
                var notification = new Notification()
                {
                    UserId = request.UserId,
                    Message = request.Message,
                };
                _context.Notifications.Add(notification);
                if (await _notificationService.SendNotification(fcmId, "Info", request.Message))
                {
                    _context.SaveChanges();
                }
            }
            return new ResponseViewModel();
        }
    }

}