using HospitalApp.Common.Response;
using MediatR;

namespace HospitalApp.Application.Notifications.Commands.SendNotification
{

    public class SendNotificationRequestModel : IRequest<ResponseViewModel>
    {
        public string TransactionId { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
    }
}