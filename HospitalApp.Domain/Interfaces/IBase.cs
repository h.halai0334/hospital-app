﻿﻿﻿using System;

  namespace HospitalApp.Domain.Interfaces
{
    public interface IBase
    {
        /// <summary>
        /// The Date it was created
        /// </summary>
        DateTime CreatedDate { get; set; }
        /// <summary>
        /// The Date it was Updated
        /// </summary>
        DateTime UpdatedDate { get; set; }
        /// <summary>
        /// The UserId of the User that created this entity
        /// </summary>
        string CreatedBy { get; set; }
        /// <summary>
        /// The UserId of the User that updated this entity
        /// </summary>
        string UpdatedBy { get; set; }
    }
}