namespace HospitalApp.Domain.Entities
{
    public class Notification : Base
    {
        public string Message { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}