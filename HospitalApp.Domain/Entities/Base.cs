using System;
using HospitalApp.Domain.Interfaces;

namespace HospitalApp.Domain.Entities
{
    public class Base : IBase
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}