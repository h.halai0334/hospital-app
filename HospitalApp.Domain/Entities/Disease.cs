﻿using System.Collections.Generic;

namespace HospitalApp.Domain.Entities
{
    public class Disease : Base
    {
        public string Name { get; set; }
        public IEnumerable<DiseaseSymptom> DiseaseSymptomTypes { get; set; }
    }
}