using System;
using System.Collections.Generic;
using HospitalApp.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Domain.Entities
{
    public class Role : IdentityRole<string>, IBase
    {
        public Role()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<RoleClaim> RoleClaims { get; set; }

    }
}