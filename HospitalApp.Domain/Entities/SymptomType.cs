﻿using System.Collections.Generic;

namespace HospitalApp.Domain.Entities
{
    public class SymptomType : Base
    {
        public string Name { get; set; }
        public IEnumerable<Symptom> Symptoms { get; set; }
        public IEnumerable<DiseaseSymptom> DiseaseSymptomTypes { get; set; }
    }
}