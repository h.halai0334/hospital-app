using System;
using HospitalApp.Domain.Interfaces;

namespace HospitalApp.Domain.Entities
{
    public class AppSetting : IBase
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}