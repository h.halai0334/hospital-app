﻿using System;
using HospitalApp.Domain.Interfaces;

namespace HospitalApp.Domain.Entities
{
    public class DiseaseSymptom : IBase
    {
        public int DiseaseId { get; set; }
        public Disease Disease { get; set; }    
        public int SymptomId { get; set; }
        public Symptom Symptom { get; set; }
        public int SymptomTypeId { get; set; }
        public SymptomType SymptomType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}