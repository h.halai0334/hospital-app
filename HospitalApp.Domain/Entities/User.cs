using System;
using System.Collections.Generic;
using HospitalApp.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Domain.Entities
{
    public class User : IdentityUser, IBase
    {
        public User()
        {
            Id = Guid.NewGuid().ToString("N");
        }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FcmId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<UserClaim> UserClaims { get; set; }
        /// <summary>
        /// Is User Allowed to Login or Not
        /// </summary>
        public bool IsEnabled { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
    }
}