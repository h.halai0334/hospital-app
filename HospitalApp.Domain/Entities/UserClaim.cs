using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Domain.Entities
{
    public class UserClaim : IdentityUserClaim<string>
    {
        public User User { get; set; }    
    }
}