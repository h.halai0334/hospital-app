﻿using System.Collections.Generic;

namespace HospitalApp.Domain.Entities
{
    public class Symptom : Base
    {
        public int SymptomTypeId { get; set; }
        public SymptomType SymptomType { get; set; }
        public string Name { get; set; }
        public IEnumerable<DiseaseSymptom> DiseaseSymptomTypes { get; set; }
    }
}