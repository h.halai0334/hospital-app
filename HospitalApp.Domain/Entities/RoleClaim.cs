using Microsoft.AspNetCore.Identity;

namespace HospitalApp.Domain.Entities
{
    public class RoleClaim : IdentityRoleClaim<string>
    {
        public Role Role { get; set; }        
    }
}