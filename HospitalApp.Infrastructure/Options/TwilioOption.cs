namespace HospitalApp.Infrastructure.Options
{
    public class TwilioOption
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Phone { get; set; }
    }
}