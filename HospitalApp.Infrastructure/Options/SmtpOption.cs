namespace HospitalApp.Infrastructure.Options
{
    public class SmtpOption
    {
        public string Host { get; set; }
        public string Email { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
    }
}