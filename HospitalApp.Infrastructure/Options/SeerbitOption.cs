using System;

namespace HospitalApp.Infrastructure.Options
{
    public class SeerbitOption
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AccessToken { get; set; }
        public DateTime? Expiry { get; set; }
    }
}