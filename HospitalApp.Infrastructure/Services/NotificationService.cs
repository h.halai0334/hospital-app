using System;
using System.Threading.Tasks;
using CorePush.Google;
using HospitalApp.Application.Interfaces;
using HospitalApp.Infrastructure.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HospitalApp.Infrastructure.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IOptions<FcmOption> _options;
        private readonly ILogger<NotificationService> _logger;
        public NotificationService(IOptions<FcmOption> options, ILogger<NotificationService> logger)
        {
            _options = options;
            _logger = logger;
        }

        public async Task<bool> SendNotification(string fcmId, string title, string body)
        {
            try
            {
                if (string.IsNullOrEmpty(fcmId))
                {
                    return false;
                }
                using (FcmSender fcm = new FcmSender(_options.Value.ServerKey, _options.Value.SenderKey))
                {
                    await fcm.SendAsync(fcmId, new
                    {
                        notification = new
                        {
                            title,
                            body
                        }
                    });
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Notification Error", e);
            }
            return true;

        }
    }
}