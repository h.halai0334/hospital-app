using System.Net;
using System.Net.Mail;
using HospitalApp.Application.Interfaces;
using HospitalApp.Infrastructure.Options;
using Microsoft.Extensions.Options;

namespace HospitalApp.Infrastructure.Services
{
    public class EmailService : IEmailService
    {
        private readonly IOptions<SmtpOption> _options;

        public EmailService(IOptions<SmtpOption> options)
        {
            _options = options;
        }

        public bool SendEmail(string email, string subject, string body)
        {
            var client = new SmtpClient(_options.Value.Host, _options.Value.Port)
            {
                Credentials = new NetworkCredential(_options.Value.Email, _options.Value.Password),
                EnableSsl = true
            };
            client.Send(_options.Value.Email, email, subject, body);
            return true;

        }
    }
}