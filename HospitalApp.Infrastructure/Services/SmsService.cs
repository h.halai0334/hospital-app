using System;
using System.Threading.Tasks;
using HospitalApp.Application.Interfaces;
using HospitalApp.Infrastructure.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace HospitalApp.Infrastructure.Services
{
    public class SmsService : ISmsService
    {

        private readonly IOptions<TwilioOption> _options;
        private readonly ILogger<SmsService> _logger;
        public SmsService(IOptions<TwilioOption> options, ILogger<SmsService> logger)
        {
            _options = options;
            _logger = logger;
        }

        public async Task<bool> SendMessage(string phoneNumber, string message)
        {

            try
            {
                TwilioClient.Init(_options.Value.AccountSid, _options.Value.AuthToken);

                var response = await MessageResource.CreateAsync(
                    body: message,
                    @from: new PhoneNumber(_options.Value.Phone),
                    to: new PhoneNumber(phoneNumber)
                );
                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    throw new Exception(response.ErrorMessage);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"SMS Provider Error");
                return true;
            }
        }
    }
}