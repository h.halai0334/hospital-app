using System;
using System.IO;
using HospitalApp.Application.Interfaces;

namespace HospitalApp.Infrastructure.Services
{
    public class ImageService : IImageService
    {
        public string SaveImage(string base64)
        {
            byte[] image = LoadImage(base64);
            string extension = ".jpg";
            string fileName = Guid.NewGuid() + extension;
            var seprator = Path.DirectorySeparatorChar;
            string directoryPath = Path.Combine(
                Directory.GetCurrentDirectory(), $"wwwroot{seprator}assets{seprator}img");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            var path = Path.Combine(directoryPath, fileName);
            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                stream.Write(image, 0, image.Length);
                return "/assets/img/" + fileName;
            }
        }

        public void DeleteImage(string url)
        {
            if (File.Exists("wwwroot" + url))
            {
                File.Delete("wwwroot" + url);
            }
        }

        byte[] LoadImage(string baseString)
        {
            baseString = baseString.Remove(0, baseString.IndexOf(',') + 1);
            return Convert.FromBase64String(baseString);
        }
    }
}