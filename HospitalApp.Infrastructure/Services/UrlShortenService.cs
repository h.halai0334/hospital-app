using System;
using System.IO;
using System.Net;
using HospitalApp.Application.Interfaces;
using Newtonsoft.Json;

namespace HospitalApp.Infrastructure.Services
{
    public class UrlShortenService : IUrlShortenService
    {
        public string UrlShorten(string longUrl)
        {
            GoogleUrlShortenerResponse model = null;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.rebrandly.com/v1/links");
            httpWebRequest.Headers.Add("apikey","f6a7654de60942adbba4987e3fb64cc0");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"destination\":\"" +longUrl + "\"}";
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var json = streamReader.ReadToEnd();
                    model = JsonConvert.DeserializeObject<GoogleUrlShortenerResponse>(json);
                    return model.shortUrl;
                }                
            }

            throw new Exception(httpResponse.StatusDescription);
        }
        class GoogleUrlShortenerResponse
        {
            public string shortUrl { get; set; }
        }
    }
}